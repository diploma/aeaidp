package com.agileai.hotweb.ws;

import java.util.ArrayList;
import java.util.List;

public class ServiceModel {
	public static final class AuthTypes{
		public static final String IpWhiteList = "IpWhiteList";
		public static final String UserNameToken = "UserNameToken";
		public static final String URLMatch = "URLMatch";
	}
	public static final class ServiceTypes {
		public static final String Rest = "Rest";
		public static final String Soap = "Soap";
	}
	
	private String id = null;
	private String implClass = null;
	private String address = null;
	private String serviceType = null;
	private List<AuthConfig> authConfigs = new ArrayList<AuthConfig>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImplClass() {
		return implClass;
	}
	public void setImplClass(String implClass) {
		this.implClass = implClass;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public List<AuthConfig> getAuthConfigs() {
		return authConfigs;
	}
	public void setAuthConfigs(List<AuthConfig> authConfigs) {
		this.authConfigs = authConfigs;
	}
}
