package com.agileai.hotweb.ws;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.apache.log4j.Logger;

import com.agileai.common.AppConfig;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.ClassLoaderFactory;
import com.agileai.hotweb.common.HttpRequestHelper;
import com.agileai.hotweb.common.ModuleManager;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.util.StringUtil;

public class BaseRestService {
	protected Logger log = Logger.getLogger(this.getClass());

	private ServiceModel serviceModel = null;
	
	protected HttpServletRequest getHttpServletRequest(){
		Message message = PhaseInterceptorChain.getCurrentMessage();  
		HttpServletRequest httpRequest = (HttpServletRequest)message.get(AbstractHTTPDestination.HTTP_REQUEST);  
		return httpRequest;
	}
	
	protected Profile getProfile(){
		HttpServletRequest request = getHttpServletRequest();
		HttpSession session = request.getSession();
		Profile profile = (Profile)session.getAttribute(Profile.PROFILE_KEY);
		return profile;
	}
	
	protected Object getUser(){
		Object user = null;
		Profile profile = getProfile();
		if (profile != null){
			user = profile.getUser();			
		}
		return user;
	}	
	
	protected DataSource getDataSource(){
		return BeanFactory.instance().getDataSource();
	}
	
	protected DataSource getDataSource(String dataSourceId){
		return BeanFactory.instance().getDataSource(dataSourceId);
	}
	
	public AppConfig getAppConfig(){
		AppConfig result = (AppConfig)BeanFactory.instance().getAppConfig();
		return result;
	}		
	
	protected Object lookupService(String serviceId){
		Object service = null;
		ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
		ModuleManager moduleManager = ModuleManager.getOnly(appClassLoader);
		String moduleName = moduleManager.getServiceModule(serviceId);
		if (StringUtil.isNotNullNotEmpty(moduleName)){
			ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
			ClassLoader moduleClassLoader = classLoaderFactory.createModuleClassLoader(moduleName);
			service = BeanFactory.instance(moduleClassLoader).getBean(serviceId);
		}else{
			service = BeanFactory.instance().getBean(serviceId);
		}
		return service;
	}
	
	protected <ServiceType> ServiceType lookupService(Class<ServiceType> serviceClass){
		String serviceId = BaseHandler.buildServiceId(serviceClass);
		Object service = this.lookupService(serviceId);
		return serviceClass.cast(service);
	}
	
	public ServiceModel getServiceModel() {
		return serviceModel;
	}
	
	public void setServiceModel(ServiceModel serviceModel) {
		this.serviceModel = serviceModel;
	}
	
	public String getRemoteHost(HttpServletRequest request){
		return HttpRequestHelper.getRemoteHost(request);
	}		
}