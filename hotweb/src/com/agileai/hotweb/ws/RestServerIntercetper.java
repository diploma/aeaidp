package com.agileai.hotweb.ws;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.soap.SOAPException;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.log4j.Logger;

import com.agileai.common.KeyValuePair;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.util.StringUtil;

public class RestServerIntercetper extends AbstractPhaseInterceptor<Message>{
	private static final Logger logger = Logger.getLogger(RestServerIntercetper.class);
	public static final String XML_USERID_EL = "userId";
	public static final String XML_PASSWORD_EL = "password";
	public static final String LocalHost = "localhost";
	
	private ServiceModel serviceModel = null;
	
	public ServiceModel getServiceModel() {
		return serviceModel;
	}

	public void setServiceModel(ServiceModel serviceModel) {
		this.serviceModel = serviceModel;
	}

	public RestServerIntercetper() {
		super(Phase.PRE_INVOKE);
	}
	
	public void handleMessage(Message message) throws Fault{
		HttpServletRequest request = (HttpServletRequest)message.get("HTTP.REQUEST");
		List<AuthConfig> authConfigs = serviceModel.getAuthConfigs();
		String reqAddress = request.getRemoteAddr();
		if ("0:0:0:0:0:0:0:1".equals(reqAddress) || "127.0.0.1".equals(reqAddress)){
			 reqAddress = LocalHost;
		}
		 
		for (int i=0;i < authConfigs.size();i++){
			AuthConfig authConfig = authConfigs.get(i);
			String authType = authConfig.getType();
			if (ServiceModel.AuthTypes.IpWhiteList.equals(authType)){
				 KeyValuePair keyValuePair = authConfig.getProperties().get("IpAddress");
				 if (keyValuePair != null){
					 String ipText = keyValuePair.getValue();
					 if (ipText.indexOf("*.*") > -1){
						 logger.debug(reqAddress +" request address auth successfully !");
					 }else{
						 List<String> validIpAddressList = parseValidValueList(ipText);
						 if (validIpAddressList.contains(reqAddress)){
							 logger.debug(reqAddress +" request address auth successfully !");
						 }else{
							SOAPException soapExc = new SOAPException(reqAddress + " is not valid request address !");
							throw new Fault(soapExc);
						 }
					 }
				 }
			}
			else if (ServiceModel.AuthTypes.URLMatch.equals(authType)){
				String reqURL = request.getRequestURI();
				if (LocalHost.equals(reqAddress)){
					logger.debug(reqURL +" request url auth successfully !");
					return;
				}
				KeyValuePair publicKeyValuePair = authConfig.getProperties().get("publicURL");
				boolean isMatched = false;
				if (publicKeyValuePair != null){
					String publicURLs = publicKeyValuePair.getValue();
					if (publicURLs.indexOf("*.*") > -1){
						logger.debug(reqURL +" request url auth successfully !");
					}
					else{
						 List<String> validURLList = parseValidValueList(publicURLs);
						 for (int j=0;j < validURLList.size();j++){
							 String validURL = validURLList.get(i);
							 if (reqURL.indexOf(validURL) > -1){
								 logger.debug(reqURL +" request url auth successfully !");
								 isMatched = true;
								 break;
							 }
						 }
					}
				}

				KeyValuePair protectedKeyValuePair = authConfig.getProperties().get("protectedURL");
				if (!isMatched && protectedKeyValuePair != null){
					String protectedURLs = protectedKeyValuePair.getValue();
					if (protectedURLs.indexOf("*.*") > -1 && this.getUser(request) != null){
						logger.debug(reqURL +" request url auth successfully !");
					}
					else{
						 List<String> validURLList = parseValidValueList(protectedURLs);
						 for (int j=0;j < validURLList.size();j++){
							 String validURL = validURLList.get(i);
							 if (reqURL.indexOf(validURL) > -1 && this.getUser(request) != null){
								 logger.debug(reqURL +" request url auth successfully !");
								 isMatched = true;
								 break;
							 }
						 }
					}
				}
				
				if (!isMatched){
					SOAPException soapExc = new SOAPException(reqURL + " is not valid request url !");
					throw new Fault(soapExc);
				}
				
			}
		}
	}	
	private List<String> parseValidValueList(String ipText){
		List<String> result = new ArrayList<String>();
		if (StringUtil.isNotNullNotEmpty(ipText)){
			String[] ipArray = ipText.split(",");
			for (int i=0;i < ipArray.length;i++){
				String tempIp = ipArray[i];
				if (StringUtil.isNotNullNotEmpty(tempIp)){
					if (!result.contains(tempIp.trim())){
						result.add(tempIp.trim());
					}
				}
			}
		}
		return result;
	}
	
	protected Profile getProfile(HttpServletRequest request){
		HttpSession session = request.getSession();
		Profile profile = (Profile)session.getAttribute(Profile.PROFILE_KEY);
		return profile;
	}
	
	protected Object getUser(HttpServletRequest request){
		Object user = null;
		Profile profile = getProfile(request);
		if (profile != null){
			user = profile.getUser();			
		}
		return user;
	}	
}