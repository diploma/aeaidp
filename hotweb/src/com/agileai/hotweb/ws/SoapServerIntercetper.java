package com.agileai.hotweb.ws;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.SOAPException;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.log4j.Logger;

import com.agileai.common.KeyValuePair;
import com.agileai.util.StringUtil;

public class SoapServerIntercetper extends AbstractPhaseInterceptor<SoapMessage>{
	private static final Logger logger = Logger.getLogger(SoapServerIntercetper.class);
	public static final String XML_USERID_EL = "userId";
	public static final String XML_PASSWORD_EL = "password";
	
	private ServiceModel serviceModel = null;
	
	public ServiceModel getServiceModel() {
		return serviceModel;
	}

	public void setServiceModel(ServiceModel serviceModel) {
		this.serviceModel = serviceModel;
	}

	public SoapServerIntercetper() {
		super(Phase.PRE_INVOKE);
	}
	
	public void handleMessage(SoapMessage message) throws Fault{
		HttpServletRequest request = (HttpServletRequest)message.get("HTTP.REQUEST");
		List<AuthConfig> authConfigs = serviceModel.getAuthConfigs();
		for (int i=0;i < authConfigs.size();i++){
			AuthConfig authConfig = authConfigs.get(i);
			String authType = authConfig.getType();
			if (ServiceModel.AuthTypes.IpWhiteList.equals(authType)){
				 String reqAddress = request.getRemoteAddr();
				 KeyValuePair keyValuePair = authConfig.getProperties().get("IpAddress");
				 if (keyValuePair != null){
					 String ipText = keyValuePair.getValue();
					 List<String> validIpAddressList = parseValidIpAddressList(ipText);
					 if (validIpAddressList.contains(reqAddress)){
						 logger.debug(reqAddress +" request address auth successfully !");
					 }else{
						SOAPException soapExc = new SOAPException(reqAddress + " is not valid request address !");
						throw new Fault(soapExc);
					 }					 
				 }
			}
		}
	}
	
	private List<String> parseValidIpAddressList(String ipText){
		List<String> result = new ArrayList<String>();
		if (StringUtil.isNotNullNotEmpty(ipText)){
			String[] ipArray = ipText.split(",");
			for (int i=0;i < ipArray.length;i++){
				String tempIp = ipArray[i];
				if (StringUtil.isNotNullNotEmpty(tempIp)){
					if (!result.contains(tempIp.trim())){
						result.add(tempIp.trim());
					}
				}
			}
		}
		return result;
	}
}