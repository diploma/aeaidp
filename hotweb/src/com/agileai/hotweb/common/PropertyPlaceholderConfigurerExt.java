package com.agileai.hotweb.common;

import java.io.File;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.agileai.hotweb.config.SharedConfig;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;

public class PropertyPlaceholderConfigurerExt extends PropertyPlaceholderConfigurer implements SharedConfig{
	private boolean useSharedConfig = false;
	protected Properties properties = null;

	public void setLocations(Resource... locations) {
		Resource[] newLocations = new Resource[locations.length];
		for (int i=0;i < locations.length;i++){
			Resource resource = locations[i];
			if (resource.getFilename().equals("shared.properties")){
				String catalinaBase = System.getProperty("catalina.base");
				String filePath = catalinaBase+File.separator+"conf"+File.separator+resource.getFilename(); 
				File file = new File(filePath);
				FileSystemResource fsResource = new FileSystemResource(file);
				newLocations[i] = fsResource;
				
				this.useSharedConfig = true;
			}else{
				newLocations[i] = resource;
			}
		}
		super.setLocations(newLocations);
	}	
	
    @Override 
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) 
                    throws BeansException { 
		String secretKey = props.getProperty("dataSource.securityKey");
        String password = props.getProperty("dataSource.password"); 
        if (password != null) { 
        	props.setProperty("dataSource.password", CryptionUtil.decryption(password, secretKey)); 
        } 
        this.properties = props;
        super.processProperties(beanFactory, props);
    }

	@Override
	public boolean isEnable() {
		return useSharedConfig;
	}

	@Override
	public String getConfig(String key) {
		return this.properties.getProperty(key);
	}

	@Override
	public String getConfig(String key, String defaultValue) {
		String result = getConfig(key);
		if (StringUtil.isNullOrEmpty(result)){
			result = defaultValue;
		}
		return result;
	}
}