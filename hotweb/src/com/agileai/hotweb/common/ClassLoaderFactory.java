package com.agileai.hotweb.common;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

public class ClassLoaderFactory {
	private static Map<ClassLoader,ClassLoaderFactory> instances = new HashMap<ClassLoader,ClassLoaderFactory>();
	private ClassLoader appClassLoader = null;
	private Map<String,URLClassLoader> moduleClassLoaderCache = new HashMap<String,URLClassLoader>();
	private static Map<ClassLoader,String> appClassLoaderPathPair = new HashMap<ClassLoader,String>();
	private URLClassLoader serviceClassLoader = null;
	
	private ClassLoaderFactory(ClassLoader appClassLoader){
		this.appClassLoader = appClassLoader;
	}	
	
	public static void initAppClassLoaderPathPair(ClassLoader appClassLoader,String appContextPath){
		if (!appClassLoaderPathPair.containsKey(appClassLoader)){
			appClassLoaderPathPair.put(appClassLoader, appContextPath);
		}
	}
	
	public static synchronized ClassLoaderFactory instance(ClassLoader appClassLoader){
		if (!instances.containsKey(appClassLoader)){
			ClassLoaderFactory temp = new ClassLoaderFactory(appClassLoader);
			instances.put(appClassLoader,temp);
		}
		return instances.get(appClassLoader);
	}
	
	public synchronized ClassLoader createModuleClassLoader(String moduleName) {
		if (!moduleClassLoaderCache.containsKey(moduleName)){
			try {
				URL moduleUrl = this.buildModuleURL(moduleName);
				URL[] urls = new URL[]{moduleUrl};
				ModuleClassLoader mfClassLoader = new ModuleClassLoader(urls,appClassLoader);
				moduleClassLoaderCache.put(moduleName, mfClassLoader);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return moduleClassLoaderCache.get(moduleName);
	}
	
	public synchronized ClassLoader createServiceClassLoader(){
		if (serviceClassLoader == null){
			try {
				URL serviceUrl = this.buildServiceURL();
				URL[] urls = new URL[]{serviceUrl};
				serviceClassLoader = new ServiceClassLoader(urls, appClassLoader);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return serviceClassLoader;
	}
	
	public synchronized ClassLoader rejectModuleClassLoader(String moduleName){
		ClassLoader result =  null;
		if (moduleClassLoaderCache.containsKey(moduleName)){
			result = moduleClassLoaderCache.get(moduleName);
			moduleClassLoaderCache.remove(moduleName);
		}
		return result;
	}
	
	public synchronized ClassLoader rejectServiceClassLoader(){
		serviceClassLoader = null;
		return null;
	}
	
	private URL buildModuleURL(String moduleName) throws MalformedURLException{
		StringBuffer result = new StringBuffer();
		String appContextPath = appClassLoaderPathPair.get(this.appClassLoader);
		result.append(appContextPath).append("WEB-INF/modules/").append(moduleName);
		File file = new File(result.toString());
		return file.toURI().toURL();
	}
	
	private URL buildServiceURL() throws MalformedURLException{
		StringBuffer result = new StringBuffer();
		String appContextPath = appClassLoaderPathPair.get(this.appClassLoader);
		result.append(appContextPath).append("WEB-INF/services/");
		File file = new File(result.toString());
		return file.toURI().toURL();
	}
}