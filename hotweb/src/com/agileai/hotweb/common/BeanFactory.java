package com.agileai.hotweb.common;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.agileai.common.AppConfig;
import com.agileai.hotweb.bizmoduler.core.BaseService;
import com.agileai.hotweb.config.SharedConfig;

public class BeanFactory {
	public final static String DfaultDatasourceId = "dataSource";
	public final static String ContextServiceConfigFile = "ServiceContext.xml";
	public final static String ModuleServiceConfigFile = "ServiceModule.xml";
	
	private static HashMap<ClassLoader,BeanFactory> beanFactoryCache = new HashMap<ClassLoader,BeanFactory>();
	
	private ApplicationContext context = null;
	private ClassLoader beanClassLoader = null;
	
	private BeanFactory(){}
	
	public synchronized static BeanFactory instance(){
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		BeanFactory instance = beanFactoryCache.get(classLoader);
		if (instance == null){
			try {
				instance = (BeanFactory)classLoader.loadClass("com.agileai.hotweb.common.BeanFactory").newInstance();
				beanFactoryCache.put(classLoader, instance);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return instance;
	}
	
	public synchronized static BeanFactory instance(ClassLoader classLoader){
		BeanFactory instance = beanFactoryCache.get(classLoader);
		if (instance == null){
			try {
				instance = (BeanFactory)classLoader.loadClass("com.agileai.hotweb.common.BeanFactory").newInstance();
				instance.beanClassLoader = classLoader;
				instance.initServiceModule();
				beanFactoryCache.put(classLoader, instance);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return instance;
	}
	
	public synchronized static void destroy(ClassLoader moduleClassLoader){
		if (beanFactoryCache.containsKey(moduleClassLoader)){
			beanFactoryCache.remove(moduleClassLoader);
		}
	}
	
	private void initServiceModule(){
		if (this.context == null){
			String[] configs = new String[]{ContextServiceConfigFile,ModuleServiceConfigFile};
//			String[] configs = new String[]{ModuleServiceConfigFile};
			this.context = new ClassPathXmlApplicationContext();
			((ClassPathXmlApplicationContext)this.context).setClassLoader(beanClassLoader);
			((ClassPathXmlApplicationContext)this.context).setConfigLocations(configs);
			((ClassPathXmlApplicationContext)this.context).refresh();
		}
	}

	public static void init(String serviceConfigLocation){
		BeanFactory instance = instance();
		if (instance.context == null){
			String[] configs = serviceConfigLocation.split(",");
			instance.context = new ClassPathXmlApplicationContext(configs);
		}
	}
	public Object getBean(String beanId){
		Object bean = this.context.getBean(beanId);
		if (bean instanceof BaseService && beanClassLoader != null){
			BaseService baseService = (BaseService)bean;
			baseService.setClassLoader(beanClassLoader);
		}
		return bean;
	}
	public DataSource getDataSource(){
		DataSource result = (DataSource)getBean(DfaultDatasourceId);
		return result;
	}
	public DataSource getDataSource(String datasoureId){
		DataSource result = (DataSource)getBean(datasoureId);
		return result;
	}
	public AppConfig getAppConfig(){
		AppConfig result = (AppConfig)getBean("appConfig");
		return result;
	}
	public SharedConfig getSharedConfig(){
		SharedConfig result = (SharedConfig)getBean("propertyConfigurer");
		return result;
	}
}
