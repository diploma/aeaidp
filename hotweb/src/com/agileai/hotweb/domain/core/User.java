package com.agileai.hotweb.domain.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class User implements Serializable{
	public static final String LocaleKey = "java.util.Locale";
	
	private static final long serialVersionUID = 5317642201310695295L;
	
	private String userId = null;
	private String userCode = null;
	private String userName = null;
	
	private List<Role> roleList = new ArrayList<Role>();
	private List<Group> groupList = new ArrayList<Group>();
	private HashMap<String,HashMap<String,Resource>> resoucesRefs = new HashMap<String,HashMap<String,Resource>>();

	private HashMap<String,Object> properties = new HashMap<String,Object>();
	private HashMap<String,Object> tempProperties = new HashMap<String,Object>();
	private HashMap<String,Object> extendProperties = new HashMap<String,Object>();

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public List<Group> getGroupList() {
		return groupList;
	}

	public HashMap<String, HashMap<String, Resource>> getResoucesRefs() {
		return resoucesRefs;
	}
	
	public boolean hasRole(String roleCode){
		boolean result = false;
		for (int i=0;i < roleList.size();i++){
			Role role = this.roleList.get(i);
			if (roleCode.equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}

	public boolean hasGroup(String groupCode){
		boolean result = false;
		for (int i=0;i < groupList.size();i++){
			Group group = this.groupList.get(i);
			if (groupCode.equals(group.getGroupCode())){
				result = true;
				break;
			}
		}	
		return result;
	}
	
	public boolean isAdmin(){
		return "admin".equals(this.userCode);
	}
	
	public boolean containResouce(String resourceType,String resourceId){
		boolean result = false;
		HashMap<String,Resource> resourceMap = this.resoucesRefs.get(resourceType);
		if (resourceMap != null){
			result = resourceMap.containsKey(resourceId);
		}
		return result;
	}

	public HashMap<String, Object> getTempProperties() {
		return tempProperties;
	}
	
	public HashMap<String, Object> getExtendProperties() {
		return extendProperties;
	}

	public HashMap<String, Object> getProperties() {
		return properties;
	}
}
