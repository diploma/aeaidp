package com.agileai.hotweb.domain.core;

import java.io.Serializable;
import java.util.HashMap;

public class Role implements Serializable{
	private static final long serialVersionUID = -4001981780985315610L;
	
	private String roleId = null;
	private String roleCode = null;
	private String roleName = null;
	private HashMap<String,Object> properties = new HashMap<String,Object>();
	private HashMap<String,Object> extendProperties = new HashMap<String,Object>();
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public HashMap<String, Object> getExtendProperties() {
		return extendProperties;
	}
	public HashMap<String, Object> getProperties() {
		return properties;
	}
	public void setProperties(HashMap<String, Object> properties) {
		this.properties = properties;
	}
	
}