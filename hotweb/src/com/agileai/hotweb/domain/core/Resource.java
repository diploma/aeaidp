package com.agileai.hotweb.domain.core;

import java.io.Serializable;
import java.util.HashMap;

public class Resource implements Serializable{
	private static final long serialVersionUID = -1702321170852343595L;
	
	public static interface Type{
		public static final String Navigater = "Navigater";
		public static final String Menu = "Menu";
		public static final String Portlet = "Portlet";
		
		public static final String Application = "Application";
		
		public static final String InfoColumn = "InfoColumn";
		public static final String Infomation = "Infomation";
		
		public static final String Handler = "Handler";
		public static final String Operation = "Operation";
	}
	
	private String resouceId = null;
	private String resouceType = null;
	private String resouceName = null;
	
	private HashMap<String,Object> properties = new HashMap<String,Object>();
	
	public String getResouceId() {
		return resouceId;
	}
	public void setResouceId(String resouceId) {
		this.resouceId = resouceId;
	}
	public String getResouceType() {
		return resouceType;
	}
	public void setResouceType(String resouceType) {
		this.resouceType = resouceType;
	}
	public String getResouceName() {
		return resouceName;
	}
	public void setResouceName(String resouceName) {
		this.resouceName = resouceName;
	}
	public HashMap<String, Object> getProperties() {
		return properties;
	}
	public void setProperties(HashMap<String, Object> properties) {
		this.properties = properties;
	}
}
