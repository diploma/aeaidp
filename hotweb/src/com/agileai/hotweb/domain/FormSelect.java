package com.agileai.hotweb.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataMap;

public class FormSelect extends DataMap implements Serializable{
	private static final long serialVersionUID = 8922428979279031207L;
	public static final String DEF_KEY_COLUMN_NAME = "id";
	public static final String DEF_VALUE_COLUMN_NAME = "value";
	public static final String DEF_DESCR_COLUMN_NAME = "descr";
	
	public static String BLACK_VALUE = "--请选择--";

	private boolean hasBlankValue = true;
	protected String blankText = BLACK_VALUE;
    protected String keyColumnName = "id";
    protected String valueColumnName = "value";
    protected String descrColumnName = "descr";
    protected String seletedValue = null;
    protected DataMap content = new DataMap();
    protected DataMap descriptions = new DataMap();
    protected List<String> excludeKeys = new ArrayList<String>();
    
    public FormSelect() {
        super();
    }
    public DataMap getContent() {
		return content;
	}
    
    public DataMap getDescriptions() {
		return descriptions;
	}
    
    public String getDescription(String key) {
		return descriptions.getString(key);
	}
    
	public String getText(String id){
    	String result = null;
    	try {
    		if (id != null && this.content.containsKey(id)){
    			result = String.valueOf(this.content.get(id));    			
    		}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
    }
	public void putValue(String id,String value){
    	DataMap row = new DataMap();
    	row.put(this.keyColumnName, id);
    	row.put(this.valueColumnName, value);
    	this.content.put(id,value);
    	this.put(String.valueOf(this.size()+1),row);
    }
	public FormSelect addValue(String id,String value){
    	this.putValue(id, value);
    	return this;
    }
    @SuppressWarnings({"rawtypes" })
	public void putValues(List list){
        try {
            if (list != null && list.size() >0 && this.seletedValue == null){
            	for (int i=0;i < list.size();i++){
            		HashMap row = (HashMap)list.get(i);
            		this.put(String.valueOf(this.size()+1),row);
            		this.content.put(String.valueOf(row.get(this.keyColumnName)),String.valueOf(row.get(this.valueColumnName)));
            		this.descriptions.put(String.valueOf(row.get(this.keyColumnName)),String.valueOf(row.get(this.descrColumnName)));
            	}
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public FormSelect addSelectedValue(String sv){
        this.seletedValue = sv;
        return this;
    }
    public void setSelectedValue(String sv){
        this.seletedValue = sv;
    }
    public String getSelectedValue(){
        return this.seletedValue;
    }
    public String getKeyColumnName(){
        return this.keyColumnName;
    }
    public void setKeyColumnName(String kc){
        this.keyColumnName = kc;
    }
    public String getValueColumnName(){
        return this.valueColumnName ;
    }
    public void setValueColumnName(String vc){
        this.valueColumnName = vc;
    }
	public String getSyntax(){
        StringBuffer syntax = new StringBuffer("");;
        try {
            int count = this.size();
            DataMap heRow = null;
            if (this.hasBlankValue){
            	syntax.append("<option value=''>").append(this.blankText).append("</option>");
            }
            for(int i = 1 ;i<= count;i++){
                heRow = (DataMap)this.get(String.valueOf(i));
                String id = String.valueOf(heRow.get(this.keyColumnName));
				if (this.excludeKeys.contains(id)){
					continue;
				}
                String value = String.valueOf(heRow.get(this.valueColumnName));
                String selected = null;
                if (id.equals(this.seletedValue))
                    selected = " selected='selected' ";
                else
                    selected = "";
                syntax.append("<option value='"+id+"' " + selected + ">"+value+"</option>");
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return syntax.toString();
    }
    public FormSelect addBlankValue(){
    	this.hasBlankValue = true;
    	return this;
    }
    public Object getValue(Object id){
        return this.get(id);
    }
    
    public String toString(){
        return this==null?null:this.getSyntax();
    }
    
	@SuppressWarnings({"rawtypes" })
	public String getScriptSyntax(String selectId){
		String result = null;
		try {
			StringBuffer responseText = new StringBuffer();
			responseText.append("var selectList = document.getElementById('"+selectId+"');");
			responseText.append("selectList.innerHTML='';");
			responseText.append("var tmp = '';");
			String selectedIndex = null;
			for (int i=1;i <= this.size();i++){
				HashMap heRow = (HashMap)this.get(String.valueOf(i));
				String id = String.valueOf(heRow.get(this.getKeyColumnName()));
				if (this.excludeKeys.contains(id)){
					continue;
				}
				String lable = String.valueOf(heRow.get(this.getValueColumnName()));
				responseText.append("tmp = new Option('"+lable+"','"+id+"');");
				if (id.equals(this.getSelectedValue())){
					selectedIndex = String.valueOf(i-1);
				}
				responseText.append("selectList[selectList.length]=tmp;");
			}
			if (selectedIndex != null){
				responseText.append("selectList.options["+selectedIndex+"].selected = true;");	
			}
			result = responseText.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	public boolean isHasBlankValue() {
		return hasBlankValue;
	}
	public String getBlankText() {
		return blankText;
	}
	public void setBlankText(String blankText) {
		this.blankText = blankText;
	}
	public void setHasBlankValue(boolean hasBlankValue) {
		this.hasBlankValue = hasBlankValue;
	}
	public FormSelect addHasBlankValue(boolean hasBlankValue) {
		this.hasBlankValue = hasBlankValue;
		return this;
	}
	public List<String> getExcludeKeys() {
		return excludeKeys;
	}
	public FormSelect clone(){
		FormSelect result = new FormSelect();
		result.setHasBlankValue(this.hasBlankValue);
		result.setBlankText(this.blankText);
		result.setKeyColumnName(this.keyColumnName);
		result.setSelectedValue(this.seletedValue);
		result.excludeKeys.addAll(this.excludeKeys);
		result.content.putAll(this.content);
		result.putAll(this);
		return result;
	}
}
