package com.agileai.hotweb.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.common.Constants;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.controller.core.BaseHandler.OperaType;
import com.agileai.hotweb.domain.core.BasePageBean;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class PageBean extends BasePageBean implements Serializable{
	private static final long serialVersionUID = -4501893001496781892L;

	public PageBean(){
		super();
	}
	@SuppressWarnings("rawtypes")
	private List rsList = new ArrayList();
	private int rsSize = -1;
	private String operaType = BaseHandler.OperaType.DETAIL;

	public String getOperaType() {
		return BaseHandler.OperaType.COPY.equals(this.operaType)?BaseHandler.OperaType.CREATE:operaType;
	}
	public void setOperaType(String operateType) {
		this.operaType = operateType;
	}
	@SuppressWarnings("rawtypes")
	public List getRsList() {
		return rsList;
	}
	@SuppressWarnings("rawtypes")
	public void setRsList(List rsList) {
		this.rsList = rsList;
	}
	public String inputDate(String key){
		String result = "";
		try {
			Object temp = getAttribute(key);
			result = processDate(temp,result);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			result = "";
		}
		return result;
	}
	
	public String inputTime(String key){
		String result = "";
		try {
			Object temp = getAttribute(key);
			result = processDateTime(temp,result);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			result = "";
		}
		return result;
	}
	
	public String inputDate(int i,String key){
		String result = "";
		try {
			Object temp = this.value(i, key);
			result = processDate(temp,result);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			result = "";
		}
		return result;
	}
	
	public boolean isOnCreateMode(){
		return BaseHandler.OperaType.CREATE.equals(this.getOperaType());
	}
	
	public boolean isOnEditMode(){
		return BaseHandler.OperaType.UPDATE.equals(this.getOperaType());
	}
	
	public boolean isOnViewMode(){
		return BaseHandler.OperaType.DETAIL.equals(this.getOperaType());
	}
	
	public String inputTime(int i,String key){
		String result = "";
		try {
			Object temp = this.value(i, key);
			result = processDateTime(temp,result);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			result = "";
		}
		return result;
	}
	
	public String labelTime(String key){
		String result = "&nbsp;";
		try {
			Object temp = getAttribute(key);
			result = processDateTime(temp,result);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			result = "";
		}
		return result;
	}
	
	public String labelDate(String key){
		String result = "&nbsp;";
		try {
			Object temp = getAttribute(key);
			result = processDate(temp,result);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			result = "";
		}
		return result;
	}
	
	private String processDateTime(Object temp,String devalueValue){
		String result = null;
		if (temp == null || String.valueOf(temp).trim().equals("")){
			result = devalueValue;
		}
		else{
			if (temp instanceof java.util.Date) {
				if (Constants.DateTimeFormat.TIME.equals("yyyy-mm-dd hh:mm")){
					result = DateUtil.getDateByType(11,(Date)temp);
				}
				else if (Constants.DateTimeFormat.TIME.equals("yyyy/mm/dd hh:mm")){
					result = DateUtil.getDateByType(6,(Date)temp);
				}
			}
			else if (temp instanceof java.sql.Timestamp) {
				Timestamp timestamp = (Timestamp)temp;
				if (Constants.DateTimeFormat.TIME.equals("yyyy-mm-dd hh:mm")){
					result = DateUtil.getDateByType(11,new Date(timestamp.getTime()));
				}
				else if (Constants.DateTimeFormat.TIME.equals("yyyy/mm/dd hh:mm")){
					result = DateUtil.getDateByType(6,new Date(timestamp.getTime()));
				}
			}				
			else if (temp instanceof String){
				result = (String)temp;
			}
			else{
				result = temp.toString();
			}			
		}
		return result;
	}
	private String processDate(Object temp,String defaultValue){
		String result = null;
		if (temp == null || String.valueOf(temp).trim().equals("")){
			result = defaultValue;
		}
		else{
			if (temp instanceof java.util.Date) {
				if (Constants.DateTimeFormat.DATE.equals("yyyy-mm-dd")){
					result = DateUtil.getDateByType(9,(Date)temp);
				}
				else if (Constants.DateTimeFormat.DATE.equals("yyyy/mm/dd")){
					result = DateUtil.getDateByType(1,(Date)temp);
				}
			}
			else if (temp instanceof java.sql.Timestamp) {
				Timestamp timestamp = (Timestamp)temp;
				if (Constants.DateTimeFormat.TIME.equals("yyyy-mm-dd")){
					result = DateUtil.getDateByType(9,new Date(timestamp.getTime()));
				}
				else if (Constants.DateTimeFormat.TIME.equals("yyyy/mm/dd")){
					result = DateUtil.getDateByType(1,new Date(timestamp.getTime()));
				}
			}			
			else if (temp instanceof String){
				result = (String)temp;
			}
			else{
				result = temp.toString();
			}
		}		
		return result;
	}
	
	private Object value(int i,String key){
		Object result = null;
		if (rsList != null && rsList.size() > i){
			DataRow row = (DataRow)this.rsList.get(i);
			result = row.get(key);
		}
		return result;
	}
	public Object getAttribute(int i,String key){
		Object result = null;
		if (rsList != null && rsList.size() > i){
			DataRow row = (DataRow)this.rsList.get(i);
			result = row.get(key);
		}
		return result;
	}
	public String labelDate(int i,String key){
		String result = "&nbsp";
		Object temp = this.value(i, key);
		result = this.processDate(temp, result);
		return result;
	}
	public String labelTime(int i,String key){
		String result = "&nbsp";
		Object temp = this.value(i, key);
		result = processDateTime(temp, result);
		return result;
	}
	public String labelValue(int i,String key){
		String result = null;
		Object temp = this.value(i, key);
		result = this.label(temp);
		return result;
	}
	public String inputValue(int i,String key){
		String result = null;
		Object temp = this.value(i, key);
		result = this.value(temp);
		return result;
	}
	public String selectValue(int i,String key,String formSelectKey){
		String result = null;
		String selectedValue = String.valueOf(this.value(i, key));
		FormSelect select = (FormSelect)getAttribute(formSelectKey);
		if (select != null){
			select.setSelectedValue(selectedValue);
			result = select.toString();
		}
		return result;
	}
	public String labelValue(String key){
		String result = null;
		try {
			Object temp = getAttribute(key);
			if (temp == null || temp.toString().trim().equals("")){
				result = "&nbsp;";
			}
			else{
				result = String.valueOf(temp);
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			result = "&nbsp;";
		}
		return result;
	}	
	public String inputValue(String key){
		String result = null;
		Object temp = getAttribute(key);
		String paramValue = String.valueOf(temp);
		result = this.value(paramValue);
		return result;
	}
	public String labelValue(int i,String key,int limitLength){
		String result = this.labelValue(i, key);
		if (!StringUtil.isNullOrEmpty(result) && result.length() > limitLength){
			result = result.substring(0,limitLength)+"…";
		}
		return result;
	}
	public String inputValue(int i,String key,int limitLength){
		String result = this.inputValue(i, key);
		if (!StringUtil.isNullOrEmpty(result) && result.length() > limitLength){
			result = result.substring(0,limitLength)+"…";
		}
		return result;
	}
	public String labelValue(String key,int limitLength){
		String result = this.labelValue(key);
		if (!StringUtil.isNullOrEmpty(result) && result.length() > limitLength){
			result = result.substring(0,limitLength)+"…";
		}
		return result;
	}
	public String inputValue(String key,int limitLength){
		String result = this.inputValue(key);
		if (!StringUtil.isNullOrEmpty(result) && result.length() > limitLength){
			result = result.substring(0,limitLength)+"…";
		}
		return result;
	}
	public String inputValue4DetailOrUpdate(String key,String defValue){
		String result = "";
		if (OperaType.DETAIL.equals(this.getOperaType()) || OperaType.UPDATE.equals(this.getOperaType())){
			result = inputValue(key);
		}
		if (StringUtil.isNullOrEmpty(result)){
			result = defValue;
		}
		return result;
	}	
	@SuppressWarnings("rawtypes")
	public void initListAttribute(String attriKey){
		List rsList = (List)getAttribute(attriKey);
		if (rsList != null){
			this.setRsList(rsList);			
		}else{
			this.setRsList(new ArrayList());
		}
	}
	public String selectValue(String elementName){
		String result = "";
		FormSelect select = (FormSelect)getAttribute(elementName);
		if (select != null){
			result = select.toString();
		}
		return result;
	}
	public String selectValue(String elementName,String selectedValue){
		String result = "";
		FormSelect select = (FormSelect)getAttribute(elementName);
		if (select != null){
			select.setSelectedValue(selectedValue);
			result = select.toString();
		}
		return result;
	}
	public RadioGroup selectRadio(String elementName){
		RadioGroup result = null;
		FormSelect select = (FormSelect)getAttribute(elementName);
		if (select != null){
			result = new RadioGroup(elementName,select);
		}
		return result;
	}
	public RadioGroup selectRadio(String elementName,String selectedValue){
		RadioGroup result = null;
		FormSelect select = (FormSelect)getAttribute(elementName);
		if (select != null){
			select.setSelectedValue(selectedValue);
			result = new RadioGroup(elementName,select);
		}
		return result;
	}
	public RadioGroup selectRadio(int i,String key,String formSelectKey){
		RadioGroup result = null;
		String selectedValue = String.valueOf(this.value(i, key));
		FormSelect select = (FormSelect)getAttribute(formSelectKey);
		if (select != null){
			select.setSelectedValue(selectedValue);
			result = new RadioGroup(key+"_"+i,select);
		}
		return result;
	}
	
	public CheckBoxGroup selectCheckBox(String elementName){
		CheckBoxGroup result = null;
		FormSelect select = (FormSelect)getAttribute(elementName);
		if (select != null){
			result = new CheckBoxGroup(elementName,select);
		}
		return result;
	}
	public CheckBoxGroup selectCheckBox(String elementName,String selectedValue){
		CheckBoxGroup result = null;
		FormSelect select = (FormSelect)getAttribute(elementName);
		if (select != null){
			select.setSelectedValue(selectedValue);
			result = new CheckBoxGroup(elementName,select);
		}
		return result;
	}
	
	public String selectedValue(String elementName){
		String result = "";
		FormSelect select = (FormSelect)getAttribute(elementName);
		if (select != null){
			result = select.getSelectedValue();
		}
		return result;
	}
	public String selectedText(String elementName){
		String result = "";
		FormSelect select = (FormSelect)getAttribute(elementName);
		if (select != null && select.getSelectedValue() != null){
			result = select.getText(select.getSelectedValue());
		}
		return result;
	}
	public String selectText(String elementName,String selectKey){
		FormSelect select = (FormSelect)getAttribute(elementName);
		String selectedText = select.getText(selectKey);
		return selectedText;
	}
	public int listSize(){
		if (rsList != null){
			this.rsSize = this.rsList.size();
		}else{
			this.rsSize = 0;
		}
		return this.rsSize;
	}
	public String checked(boolean expression){
		String result = "";
		if (expression)result = "checked";
		return result;
	}
	public String checked(String expression){
		String result = "";
		if ("1".equals(expression)  
				|| "y".equals(expression.toLowerCase())
				|| "yes".equals(expression.toLowerCase())
				|| "true".equals(expression.toLowerCase())){
			result = "checked";
		}
		return result;
	}
	public boolean isTrue(String expression){
		return "1".equals(expression)  
			|| "y".equals(expression.toLowerCase())
			|| "yes".equals(expression.toLowerCase())
			|| "true".equals(expression.toLowerCase());
	}
	public String disabled(boolean expression){
		String result = "";
		if (expression)result = "disabled";
		return result;
	}
	public String disabled(String expression){
		String result = "";
		if ("1".equals(expression)  
				|| "y".equals(expression.toLowerCase())
				|| "yes".equals(expression.toLowerCase())
				|| "true".equals(expression.toLowerCase())){
			result = "disabled";
		}
		return result;
	}
	public String readonly(String expression){
		String result = "";
		if ("1".equals(expression)  
				|| "y".equals(expression.toLowerCase())
				|| "yes".equals(expression.toLowerCase())
				|| "true".equals(expression.toLowerCase())){
			result = "readonly";
		}
		return result;
	}
	public Object nullToEmpty(Object obj){
		if (obj == null || "null".equals(obj)){
			return  "";
		}
		return obj;
	}
	public Object nullToNbsp(Object obj){
		if (obj == null || "null".equals(obj)){
			return "&nbsp;";
		}
		return obj;
	}
	public String readonly(boolean expression){
		String result = "";
		if (expression)result = "readonly";
		return result;
	}
}
