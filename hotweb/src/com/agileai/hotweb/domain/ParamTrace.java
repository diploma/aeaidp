package com.agileai.hotweb.domain;

import java.io.Serializable;

import com.agileai.domain.DataParam;

public class ParamTrace implements Serializable{
	private static final long serialVersionUID = 1392173498759090065L;
	
	private String handlerId = null;
	private DataParam pageParam = null;
	public ParamTrace(String handlerId,DataParam pageParam){
		this.handlerId = handlerId;
		this.pageParam = pageParam;
	}
	public String getHandlerId() {
		return handlerId;
	}
	public DataParam getPageParam() {
		return pageParam;
	}
}
