package com.agileai.hotweb.domain.system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FuncMenu implements Serializable{
	private static final long serialVersionUID = -8877000860467873190L;
	
	public static final String RootId = "00000000-0000-0000-00000000000000000";
	
	private String funcId = null;
	private String funcName = null;
	private String funcType = null;
	private String funcUrl = null;
	private String funcPid = null;
	private String funcIcon= null;
	
	private List<FuncMenu> children = new ArrayList<FuncMenu>();
	private List<FuncHandler> handlers = new ArrayList<FuncHandler>();

	public String getFuncId() {
		return funcId;
	}

	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}

	public String getFuncName() {
		return funcName;
	}

	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}

	public String getFuncType() {
		return funcType;
	}

	public void setFuncType(String funcType) {
		this.funcType = funcType;
	}

	public String getFuncPid() {
		return funcPid;
	}

	public void setFuncPid(String funcPid) {
		this.funcPid = funcPid;
	}

	public String getFuncUrl() {
		return funcUrl;
	}

	public void setFuncUrl(String funcUrl) {
		this.funcUrl = funcUrl;
	}

	public boolean isFolder(){
		return "funcmenu".equals(this.funcType);
	}
	
	public List<FuncHandler> getHandlers() {
		return handlers;
	}

	public List<FuncMenu> getChildren() {
		return children;
	}

	public String getFuncIcon() {
		return funcIcon;
	}

	public void setFuncIcon(String funcIcon) {
		this.funcIcon = funcIcon;
	}
}