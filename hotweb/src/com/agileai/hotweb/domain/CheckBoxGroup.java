package com.agileai.hotweb.domain;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataMap;
import com.agileai.util.StringUtil;

public class CheckBoxGroup {
	private String name = null;
	private String alias = null;
	private List<CheckBox> checkBoxGroup = new ArrayList<CheckBox>();
	private int spaceCount = 2;
	private List<CheckBoxEvent> events = new ArrayList<CheckBoxEvent>();
	
	public CheckBoxGroup(String name,FormSelect formSelect){
		this.name = name;
		this.initRadioGroup(formSelect);
	}
	private void initRadioGroup(FormSelect formSelect){
		int count = formSelect.size();
		String selectedValue = formSelect.getSelectedValue();
		List<String> selectedValues = new ArrayList<String>();
		if (selectedValue != null){
			String[] values = selectedValue.split(",");
			for (int i=0;i < values.length;i++){
				String value = values[i];
				selectedValues.add(value);
			}
		}
        for(int i = 1 ;i<= count;i++){
            DataMap heRow = (DataMap)formSelect.get(String.valueOf(i));
            String id = String.valueOf(heRow.get(formSelect.keyColumnName));
			if (formSelect.excludeKeys.contains(id)){
				continue;
			}
            String value = String.valueOf(heRow.get(formSelect.valueColumnName));
            CheckBox checkBox = new CheckBox();
            checkBox.setLabel(value);
            checkBox.setValue(id);
            if (selectedValues.contains(id))
            	checkBox.setChecked(true);
            else
            	checkBox.setChecked(false);
            checkBoxGroup.add(checkBox); 
        }
	}
	public CheckBoxGroup addEvent(String eventType,String scriptHandler){
		CheckBoxEvent checkBoxEvent = new CheckBoxEvent();
		checkBoxEvent.setEventType(eventType);
		checkBoxEvent.setScriptHandler(scriptHandler);
		this.events.add(checkBoxEvent);
		return this;
	}
	public CheckBoxGroup addSpaceCount(int spaceCount){
		this.spaceCount = spaceCount;
		return this;
	}
	public String toString(){
		String result = "";
		StringBuffer text = new StringBuffer();
		String checkBoxName = this.name;
		if (!StringUtil.isNullOrEmpty(alias)){
			checkBoxName = this.alias;
		}
		for (int i=0;i < checkBoxGroup.size();i++){
			CheckBox checkBox = checkBoxGroup.get(i);
			text.append("<input type=\"checkbox\" name=\"").append(checkBoxName).append("\"");
			for (int j=0;j < events.size();j++){
				CheckBoxEvent event = events.get(j);
				addEventHtml(text, event);
			}
			text.append(" id=\"").append(checkBoxName).append(i+"\"");
			text.append(" value=\"").append(checkBox.getValue()).append("\"");
			if (checkBox.isChecked()){
				text.append(" checked=\"checked\"");	
			}
			text.append(" />&nbsp;").append(checkBox.getLabel());
			if (i < checkBoxGroup.size()-1){
				for (int j=0;j < spaceCount;j++){
					text.append("&nbsp;");
				}
			}
		}
		result = text.toString();
		return result;
	}
	private void addEventHtml(StringBuffer text,CheckBoxEvent event){
		String eventType = event.getEventType();
		String scriptHandler = event.getScriptHandler();
		if (eventType.toLowerCase().startsWith("on")){
			text.append(" ").append(eventType).append("=\"").append(scriptHandler).append("\"");
		}else{
			text.append(" on").append(eventType).append("=\"").append(scriptHandler).append("\"");
		}
	}
	public CheckBoxGroup addAlias(String alias) {
		this.alias = alias;
		return this;
	}
}
class CheckBox{
	private String label = null;
	private String value = null;
	private boolean checked = false;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}
class CheckBoxEvent{
	private String eventType = "";
	private String scriptHandler = "";
	
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getScriptHandler() {
		return scriptHandler;
	}
	public void setScriptHandler(String scriptHandler) {
		this.scriptHandler = scriptHandler;
	}
}