package com.agileai.hotweb.controller.core;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.MapUtil;

public class StandardEditHandler extends BaseHandler{
	@SuppressWarnings("rawtypes")
	protected Class listHandlerClass = null; 
	protected boolean redirectListAfterSave = true;
	
	public StandardEditHandler(){
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);	
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);	
		}
		
		if (this.redirectListAfterSave){
			return new RedirectRenderer(getHandlerURL(listHandlerClass));			
		}else{
			param.put(OperaType.KEY,OperaType.UPDATE);
			return prepareDisplay(param);
		}
	}
	public ViewRenderer doCheckUniqueAction(DataParam param){
		String responseText = "";
		String operateType = param.get(OperaType.KEY);
		DataRow record = getService().getRecord(param);
		if (OperaType.CREATE.equals(operateType)){
			if (!MapUtil.isNullOrEmpty(record)){
				responseText = defDuplicateMsg;
			}			
		}else if (OperaType.UPDATE.equals(operateType)){
			String primaryKey = getService().getPrimaryKey();
			String primaryKeyValue = param.get(primaryKey);
			
			if (!MapUtil.isNullOrEmpty(record) && !primaryKeyValue.equals(record.stringValue(primaryKey))){
				responseText = defDuplicateMsg;
			}
		}
		return new AjaxRenderer(responseText);
	}
	
	public ViewRenderer doBackAction(DataParam param){
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	protected StandardService getService() {
		return (StandardService)this.lookupService(this.getServiceId());
	}
}
