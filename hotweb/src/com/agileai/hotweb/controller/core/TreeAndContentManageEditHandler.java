package com.agileai.hotweb.controller.core;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.MapUtil;

public abstract class TreeAndContentManageEditHandler extends BaseHandler{
	protected String tabId = "";
	protected String columnIdField = null;
	protected String contentIdField = null;
	
	public TreeAndContentManageEditHandler() {
        super();
    }
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getContentRecord(tabId,param);
			this.setAttributes(record);			
		}
		this.setOperaType(operaType);
		this.setAttribute(param, this.columnIdField);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	public ViewRenderer doSaveAction(DataParam param){
		String rspText = SUCCESS;
		TreeAndContentManage service = this.getService();
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			String colIdField = service.getTabIdAndColFieldMapping().get(this.tabId);
			String columnId = param.get(this.columnIdField);
			param.put(colIdField,columnId);
			getService().createtContentRecord(tabId,param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updatetContentRecord(tabId,param);	
		}
		return new AjaxRenderer(rspText);
	}
	
	public ViewRenderer doCheckUniqueAction(DataParam param){
		String responseText = "";
		String operateType = param.get(OperaType.KEY);
		DataRow record = getService().getContentRecord(tabId,param);
		if (OperaType.CREATE.equals(operateType)){
			if (!MapUtil.isNullOrEmpty(record)){
				responseText = defDuplicateMsg;
			}			
		}else if (OperaType.UPDATE.equals(operateType)){
			String contentId = param.get(contentIdField);
			if (!MapUtil.isNullOrEmpty(record) && !contentId.equals(record.stringValue(contentIdField))){
				responseText = defDuplicateMsg;
			}
		}
		return new AjaxRenderer(responseText);
	}
	
    abstract protected void processPageAttributes(DataParam param);

    protected TreeAndContentManage getService() {
        return (TreeAndContentManage) this.lookupService(this.getServiceId());
    }
}
