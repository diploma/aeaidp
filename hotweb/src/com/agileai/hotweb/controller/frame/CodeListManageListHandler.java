package com.agileai.hotweb.controller.frame;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class CodeListManageListHandler extends StandardListHandler{
	public static final class ManageType{
		public static final String NORMAL = "normal";
		public static final String BY_TYPE = "byType";
		public static final String BY_GROUP = "byGroup";
	}
	public CodeListManageListHandler(){
		super();
		this.editHandlerClazz = CodeListManageEditHandler.class;
		this.serviceId = "codeListService";
	}
	public ViewRenderer doChangeTypeGroupAction(DataParam param){
		String rspText = null;
		String typeGropu = param.get("TYPE_GROUP");
		DataParam fsParam = new DataParam();
		fsParam.put("TYPE_GROUP",typeGropu);
		FormSelect typeSelect = FormSelectFactory.create("codetype.findRecords", fsParam,"TYPE_ID","TYPE_NAME");
		rspText = typeSelect.getSyntax();
		return new AjaxRenderer(rspText);
	}
	public ViewRenderer doShowCodeTypeAction(DataParam param){
		StandardService service = (StandardService)this.lookupService("codeTypeService");
		String codeTypeId = param.get("CODE_TYPE_ID");
		DataParam dataParam = new DataParam();
		dataParam.put("TYPE_ID",codeTypeId);
		DataRow record = service.getRecord(dataParam);
		String typeGroup = record.stringValue("TYPE_GROUP");
		return new RedirectRenderer(getHandlerURL(CodeTypeManageListHandler.class)
				+"&TYPE_GROUP="+typeGroup);
	}
	protected void processPageAttributes(DataParam param) {
		String manageType = param.get("manageType");
		if (manageType.equals(ManageType.BY_GROUP)){
			DataParam dataParam = new DataParam();
			dataParam.put("TYPE_GROUP", param.get("TYPE_GROUP"));
			FormSelect typeIdFormSelect = FormSelectFactory.create("util.getCodeType", dataParam);
			typeIdFormSelect.setSelectedValue(param.get("TYPE_ID"));
			setAttribute("TYPE_ID", typeIdFormSelect);
			initMappingItem("TYPE_ID", typeIdFormSelect.getContent());			
		}
		else if (manageType.equals(ManageType.BY_TYPE)){
			FormSelect typeIdFormSelect = FormSelectFactory.create("util.getCodeType", new DataParam());
			initMappingItem("TYPE_ID", typeIdFormSelect.getContent());
			setAttribute("TYPE_ID", param.get("TYPE_ID"));
		}
		else{
			FormSelect typeGroupFormSelect = FormSelectFactory.create("CODE_TYPE_GROUP");
			typeGroupFormSelect.setSelectedValue(getAttributeValue("TYPE_GROUP"));
			this.setAttribute("TYPE_GROUP", typeGroupFormSelect);
			
			DataParam dataParam = new DataParam();
			dataParam.put("TYPE_GROUP", param.get("TYPE_GROUP"));
			dataParam.put("IS_UNITEADMIN", "Y");
			FormSelect typeIdFormSelect = FormSelectFactory.create("util.getCodeType", dataParam);
			typeIdFormSelect.setSelectedValue(param.get("TYPE_ID"));
			setAttribute("TYPE_ID", typeIdFormSelect);
			initMappingItem("TYPE_ID", typeIdFormSelect.getContent());
		}
		initMappingItem("CODE_FLAG",FormSelectFactory.create("SYS_VALID_TYPE").getContent());
	}
	@Override
	protected void initParameters(DataParam param) {
		this.initTitle(param);
	}
	public void initTitle(DataParam param){
		String manageType = param.get("manageType");
		if (StringUtil.isNullOrEmpty(manageType)){
			initParamItem(param, "manageType", ManageType.NORMAL);
		}else{
			initParamItem(param, "manageType", manageType);
			if (manageType.equals(ManageType.BY_TYPE)){
				String typeId = param.get("TYPE_ID");
				StandardService service = (StandardService)this.lookupService("codeTypeService");
				DataParam dataParam = new DataParam();
				dataParam.put("TYPE_ID",typeId);
				DataRow dataRow = service.getRecord(dataParam);
				String titile = dataRow.getString("TYPE_NAME");
				initParamItem(param,"titile",titile);		
			}else if (manageType.equals(ManageType.BY_GROUP)){
				String typeGroup = param.get("TYPE_GROUP");
				StandardService service = (StandardService)this.lookupService(this.serviceId);
				DataParam dataParam = new DataParam();
				dataParam.put("CODE_TYPE_ID",CodeTypeManageEditHandler.GROUP_TYPE_ID);
				dataParam.put("CODE_ID",typeGroup);
				DataRow dataRow = service.getRecord(dataParam);
				String titile = dataRow.getString("CODE_NAME");
				initParamItem(param,"titile",titile);	
			}
		}
	}
}

