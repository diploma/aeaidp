package com.agileai.hotweb.controller.frame;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class CodeTypeManageEditHandler extends StandardEditHandler{
	public static final String GROUP_TYPE_ID = "CODE_TYPE_GROUP";
	public CodeTypeManageEditHandler(){
		super();
		this.listHandlerClass = CodeTypeManageListHandler.class;
		this.serviceId = "codeTypeService";
	}
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			if (StringUtil.isNullOrEmpty(param.get("EXTEND_SQL"))){
				param.put("EXTEND_SQL","N");
			}
			getService().updateRecord(param);
			String codeType = param.get("TYPE_ID");
			FormSelectFactory.cleanCache(codeType);
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("EXTEND_SQL",FormSelectFactory.create("BOOL_DEFINE").addSelectedValue(getAttributeValue("EXTEND_SQL","N")));
		
		FormSelect typeGroupFormSelect = FormSelectFactory.create(GROUP_TYPE_ID);
		this.setAttribute("TYPE_GROUP_SELECT", typeGroupFormSelect);
		
		FormSelect characterLimitSelect = this.buildCodeCharLimitFormSelect();
		this.setAttribute("CHARACTER_LIMIT_SELECT", characterLimitSelect);
		
		String isUniteAdmin = getAttributeValue("IS_UNITEADMIN");
		if (StringUtil.isNullOrEmpty(isUniteAdmin)){
			isUniteAdmin = "Y";
		}
		FormSelect isUniteAdminSelect = FormSelectFactory.createSwitchFormSelect();
		this.setAttribute("IS_UNITEADMIN_SELECT", isUniteAdminSelect.addSelectedValue(isUniteAdmin));
		String isEditable = getAttributeValue("isEditable");
		if (StringUtil.isNullOrEmpty(isEditable)){
			isEditable = "Y";
		}
		FormSelect isEditableSelect = FormSelectFactory.createSwitchFormSelect();
		this.setAttribute("IS_EDITABLE_SELECT", isEditableSelect.addSelectedValue(isEditable));

		String isCached = getAttributeValue("IS_CACHED");
		if (StringUtil.isNullOrEmpty(isCached)){
			isCached = "Y";
		}
		FormSelect isCachedSelect = FormSelectFactory.createSwitchFormSelect();
		this.setAttribute("IS_CACHED", isCachedSelect.addSelectedValue(isCached));
		
		CodeListManageListHandler listHandler = new CodeListManageListHandler();
		listHandler.initTitle(param);
	}
	
	public FormSelect buildCodeCharLimitFormSelect(){
		FormSelect result = new FormSelect();
		result.putValue("C", "仅字符");
		result.putValue("N", "仅数字");
		result.putValue("B", "字符或数字");
		result.putValue("A", "字符、数字、下划线或点");
		result.putValue("X", "不做校验");
		return result;
	}
}
