package com.agileai.hotweb.i18n;

import java.util.HashMap;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;

public class ResourceBundle {
	HashMap<String,Properties> PropertiesCache = new HashMap<String, Properties>();
	private Locale locale = null;
	
	public static final String UserLocaleKey = "UserLocaleKey";
	public static final String DefaultLocale = "DefaultLocale";
	
	ResourceBundle(){
		
	}
	public Locale getLocale() {
		return locale;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public void setLocale(String language,String country) {
		this.locale = new Locale();
		this.locale.setCountry(country);
		this.locale.setLanguage(language);
	}
	
	public void setLocale(HttpServletRequest request){
		String localeValue = retrieveLocale(request);
		String[] values = localeValue.split("_");
		if (values != null && values.length == 2){
			String language = values[0];
			String country = values[1];
			this.setLocale(language, country);
		}
	}
	
	public String retrieveLocale(HttpServletRequest request){
		String currentLocale = null;
		User user = null;
		HttpSession session = request.getSession(false);
		Profile profile = (Profile)session.getAttribute(Profile.PROFILE_KEY);
		if (profile != null){
			 user = (User)profile.getUser();
		}
		if (user != null){
			Locale locale = (Locale)user.getExtendProperties().get(User.LocaleKey);
			if (locale != null){
				currentLocale = locale.getLanguage()+"_"+locale.getCountry();
			}else{
				currentLocale = this.parseFromCookie(request);
			}
		}else{
			currentLocale = this.parseFromCookie(request);
		}
		
		if (currentLocale == null){
			java.util.Locale locale = request.getLocale();
			if (locale != null){
				currentLocale = locale.getLanguage()+"_"+locale.getCountry();
			}else{
				String defalutLocales = request.getServletContext().getInitParameter(ResourceBundle.DefaultLocale);
				currentLocale = defalutLocales;
			}
		}
		return currentLocale;
	}
	
	private String parseFromCookie(HttpServletRequest request){
		String result = null;
  		Cookie[] cookies = request.getCookies();
  		if (cookies != null){
  			int len = cookies.length;
  			for(int i=0; i<len; i++) {
  				Cookie cookie = cookies[i];
  				String cookieName = cookie.getName();
  				if(cookieName.equals(ResourceBundle.UserLocaleKey)) {
  					result = cookie.getValue();
  					break;
  				}
  			}
  		}
		return result;
	}
	
	public String getString(String key){
		String result = "undefined";
		String language = this.locale.getLanguage();
		String country = this.locale.getCountry();
		String localeExtName = language+"_"+country;
		Properties properties = PropertiesCache.get(localeExtName);
		if (properties != null){
			result = properties.getProperty(key,"undefined");			
		}
		return result;
	}
}