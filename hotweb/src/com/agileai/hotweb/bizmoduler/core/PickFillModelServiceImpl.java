package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public class PickFillModelServiceImpl extends BaseService implements PickFillModelService {
	protected String queryPickFillRecordsSQL = "queryPickFillRecords";
	
	public List<DataRow> queryPickFillRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+queryPickFillRecordsSQL;
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
