package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public class TreeSelectServiceImpl extends BaseService implements TreeSelectService{
	protected String queryPickTreeRecordsSQL = "queryPickTreeRecords";
	public TreeSelectServiceImpl(){
		super();
	}
	public List<DataRow> queryPickTreeRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+queryPickTreeRecordsSQL;
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
