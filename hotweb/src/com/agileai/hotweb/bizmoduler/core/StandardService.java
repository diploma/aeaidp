package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface StandardService extends BaseInterface{
	void deletRecord(DataParam param);
	void createRecord(DataParam param);
	void createRecord(DataParam param,String primaryKey);
	void updateRecord(DataParam param);
	DataRow getRecord(DataParam param);
	List<DataRow> findRecords(DataParam param);
	
	String getPrimaryKey();
}
