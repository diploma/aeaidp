package com.agileai.hotweb.bizmoduler.frame;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.LogFactoryImpl;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.BaseService;

public class OnlineCounterServiceImpl extends BaseService implements OnlineCounterService {
	Log logger = LogFactoryImpl.getLog(OnlineCounterServiceImpl.class);
	private String ipAddress = null;

	public int queryOnlineCount(){
		int result = 0;
		String statementId = sqlNameSpace+"."+"getAllOnlineCount";
		DataParam param = new DataParam();
		DataRow row = this.daoHelper.getRecord(statementId, param);
		Object count = row.get("ONLINE_COUNT");
		if (count != null){
			result = row.getInt("ONLINE_COUNT");
		}
		return result;
	}
	private int queryOnlineCount(String ipAddress){
		int result = 0;
		String statementId = sqlNameSpace+"."+"getOnlineCountByIpAddress";
		DataParam param = new DataParam();
		param.put("IPADDRRESS",ipAddress);
		DataRow row = this.daoHelper.getRecord(statementId, param);
		Object count = row.get("ONLINE_COUNT");
		if (count != null){
			result = row.getInt("ONLINE_COUNT");
		}
		return result;
	} 
	public void addOnlineCount(){
		String statementId = sqlNameSpace+"."+"updateRecord";
		DataParam param = new DataParam();
		String ipAddress = getIpAddress();
		int onlineCount = queryOnlineCount(ipAddress);
		onlineCount++;
		param.put("IPADDRRESS",ipAddress);
		param.put("ONLINECOUNT",onlineCount);
		this.daoHelper.updateRecord(statementId, param);
	}

	public void minusOnlineCount(){
		String statementId = sqlNameSpace+"."+"updateRecord";
		DataParam param = new DataParam();
		String ipAddress = getIpAddress();
		int onlineCount = queryOnlineCount(ipAddress);
		onlineCount--;
		param.put("IPADDRRESS",ipAddress);
		param.put("ONLINECOUNT",onlineCount);
		this.daoHelper.updateRecord(statementId, param);
	}
	
	private String getIpAddress(){
		String result = null;
		if (ipAddress == null){
			try {
				InetAddress inet = InetAddress.getLocalHost(); 
//				String hostName = inet.getCanonicalHostName();
				String hostAddress = inet.getHostAddress();
				ipAddress = hostAddress;
			} catch (UnknownHostException e) {
				logger.error(e.getLocalizedMessage());
			}
		}
		result = ipAddress;
		return result;
	}

	public void initOnlineCount() {
		String ipAddress = getIpAddress();
		String statementId = sqlNameSpace+"."+"isExistCountByIpAddress";
		DataParam param = new DataParam();
		param.put("IPADDRRESS",ipAddress);
		DataRow row = this.daoHelper.getRecord(statementId, param);
		int existCount = row.getInt("EXIST_COUNT");
		if (existCount > 0){
			statementId = sqlNameSpace+"."+"updateRecord";
			param = new DataParam();
			int onlineCount = 0;
			param.put("IPADDRRESS",ipAddress);
			param.put("ONLINECOUNT",onlineCount);
			this.daoHelper.updateRecord(statementId, param);
		}else{
			statementId = sqlNameSpace+"."+"insertRecord";
			param = new DataParam();
			int onlineCount = 0;
			param.put("IPADDRRESS",ipAddress);
			param.put("ONLINECOUNT",onlineCount);
			this.daoHelper.updateRecord(statementId, param);
		}
	}
}
