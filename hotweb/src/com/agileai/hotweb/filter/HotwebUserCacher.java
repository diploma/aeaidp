package com.agileai.hotweb.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.spy.memcached.MemcachedClient;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.frame.FunctionManage;
import com.agileai.hotweb.bizmoduler.frame.SecurityAuthorizationConfig;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.domain.core.Group;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.hotweb.domain.core.Role;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.domain.system.FuncHandler;
import com.agileai.hotweb.domain.system.FuncMenu;
import com.agileai.hotweb.domain.system.Operation;

public class HotwebUserCacher {
	private static String UserIdPrefix = "HOTWEB-USER-";
	public static final String AutherHandlerCodes = "AutherHandlerCodes";
	private HashMap<String,User> UserCaches = new HashMap<String,User>();
	private static HashMap<String,HotwebUserCacher> Instances = new HashMap<String,HotwebUserCacher>();
	
	private String appName = null;
	private boolean useMemcache = false;
	private MemcachedClient memcachedClient = null;
	
	
	private SecurityAuthorizationConfig authorizationConfig = null;
	private FunctionManage functionTreeManage = null;
	
	private HotwebUserCacher(String appName){
		this.appName = appName;
		BeanFactory beanFactory = BeanFactory.instance();
		this.authorizationConfig = (SecurityAuthorizationConfig)beanFactory.getBean("securityAuthorizationConfigService");
		this.functionTreeManage = (FunctionManage)beanFactory.getBean("functionTreeManageService");
		AppConfig appConfig = beanFactory.getAppConfig();
		this.useMemcache = appConfig.getBoolConfig("GlobalConfig", "UseMemcache");
	}
	
	public static HotwebUserCacher getInstance(String appName){
		if (!Instances.containsKey(appName)){
			HotwebUserCacher userCacher = new HotwebUserCacher(appName);
			Instances.put(appName, userCacher);
		}
		return Instances.get(appName);
	}

	public synchronized User getUser(String userId){
		User result = null;
		if (!useMemcache){
			if (!UserCaches.containsKey(userId)){
				User user = new User();
				user.setUserCode(userId);
				this.initAuthedUser(user);
				UserCaches.put(userId, user);
			}
			User user = UserCaches.get(userId);
			result = cloneUser(user);
		}else{
			User user = (User)memcachedClient.get(UserIdPrefix+userId);
			if (user == null){
				user = new User();
				user.setUserCode(userId);
				this.initAuthedUser(user);
				memcachedClient.set(UserIdPrefix+userId,0, user);
			}
			result = cloneUser(user);
		}
		return result;
	}
	
	private User cloneUser(User user){
		User userTmp = new User();
		userTmp.setUserId(user.getUserId());
		userTmp.setUserCode(user.getUserCode());
		userTmp.setUserName(user.getUserName());
		
		userTmp.getRoleList().addAll(user.getRoleList());
		userTmp.getGroupList().addAll(user.getGroupList());
		userTmp.getResoucesRefs().putAll(user.getResoucesRefs());
		userTmp.getExtendProperties().putAll(user.getExtendProperties());
		userTmp.getProperties().putAll(user.getProperties());
		
		return userTmp;
	}
	
	public void initAuthedUser(User user){
		String loginName = user.getUserCode();
		DataRow userRow = authorizationConfig.retrieveUserRecord(loginName);
		
		String userId = userRow.stringValue("USER_ID");
		String userCode = userRow.stringValue("USER_CODE");
		String userName = userRow.stringValue("USER_NAME");
		user.setUserId(userId);
		user.setUserCode(userCode);
		user.setUserName(userName);
		user.getProperties().putAll(userRow);
		
		List<DataRow> roleRecords = authorizationConfig.retrieveRoleRecords(userId);
		for (int i=0;i < roleRecords.size();i++){
			DataRow row = roleRecords.get(i);
			String roleId = row.stringValue("ROLE_ID");
			String roleCode = row.stringValue("ROLE_CODE");
			String roleName = row.stringValue("ROLE_NAME");
			
			Role role = new Role();
			role.setRoleId(roleId);
			role.setRoleCode(roleCode);
			role.setRoleName(roleName);
			role.getProperties().putAll(row);
			user.getRoleList().add(role);
		}
		
		List<DataRow> groupRecords = authorizationConfig.retrieveGroupRecords(userId);
		for (int i=0;i < groupRecords.size();i++){
			DataRow row = groupRecords.get(i);
			String groupId = row.stringValue("GRP_ID");
			String groupCode = row.stringValue("GRP_CODE");
			String groupName = row.stringValue("GRP_NAME");
			Group group = new Group();
			group.setGroupId(groupId);
			group.setGroupCode(groupCode);
			group.setGroupName(groupName);
			group.getProperties().putAll(row);
			user.getGroupList().add(group);
		}
		
		List<String> menuItemIdList = authorizationConfig.retrieveMenuIdList(userId);
		List<String> handlerIdList = authorizationConfig.retrieveHandlerIdList(userId);
		List<String> operationIdList = authorizationConfig.retrieveOperationIdList(userId);
		
		if (menuItemIdList != null){
			if (menuItemIdList.contains(FuncMenu.RootId)){
				String functionId = FuncMenu.RootId;
				FuncMenu funcMenu = functionTreeManage.getFunction(functionId);
				HashMap<String,Resource> menuResources = getResourceRef(user, Resource.Type.Menu);
				
				Resource resource = new Resource();
				resource.setResouceType(Resource.Type.Menu);
				resource.setResouceId(funcMenu.getFuncId());
				resource.setResouceName(funcMenu.getFuncName());
				menuResources.put(funcMenu.getFuncId(), resource);
				
				List<FuncMenu> children = funcMenu.getChildren();
				this.processFuncMenu(user,children,menuItemIdList,handlerIdList,operationIdList);
			}
		}
	}
	
	private void processFuncMenu(User user,List<FuncMenu> funcMenuList
			,List<String> funcMenuIdList,List<String> handlerIdList,List<String> operationIdList){
		for (int i=0;i < funcMenuList.size();i++){
			FuncMenu funcMenu = funcMenuList.get(i);
			String curFuncId = funcMenu.getFuncId();
			
			if (funcMenuIdList.contains(curFuncId)){
				HashMap<String,Resource> menuResources = getResourceRef(user, Resource.Type.Menu);
				Resource menuResource = new Resource();
				menuResource.setResouceType(Resource.Type.Menu);
				String menuId = funcMenu.getFuncId();
				menuResource.setResouceId(menuId);
				menuResource.setResouceName(funcMenu.getFuncName());
				menuResources.put(menuId, menuResource);
			}else{
				loopFuncMenuList(funcMenu,funcMenuList, funcMenuIdList, user);
			}
			
			if (user.containResouce(Resource.Type.Menu, curFuncId)){
				if (funcMenu.isFolder()){
					List<FuncMenu> curFuncMenuList = funcMenu.getChildren();
					if (curFuncMenuList.size() > 0){
						this.processFuncMenu(user,curFuncMenuList,funcMenuIdList,handlerIdList,operationIdList);						
					}
				}else{
					List<FuncHandler> funcHandlers = funcMenu.getHandlers();
					if (funcHandlers.size() == 1){
						FuncHandler handler = funcHandlers.get(0);
						
						HashMap<String,Resource> handlerResources = getResourceRef(user, Resource.Type.Handler);
						Resource resource = new Resource();
						resource.setResouceType(Resource.Type.Handler);
						resource.setResouceId(handler.getHandlerId());
						resource.setResouceName(handler.getHandlerCode());
						handlerResources.put(handler.getHandlerId(), resource);
						
						initAutherHandlerCodes(user,handler.getHandlerCode());
						
						if (handler.getOperations().size() > 0){
							this.processOperation(user, handler.getOperations(),operationIdList);									
						}
					}else{
						if (funcHandlers.size() > 1){
							for (int j=0; j < funcHandlers.size();j++){
								FuncHandler handler = funcHandlers.get(j);
								String handlerId = handler.getHandlerId();
								if (handlerIdList.contains(handlerId)){
									HashMap<String,Resource> handlerResources = getResourceRef(user, Resource.Type.Handler);
									Resource resource = new Resource();
									resource.setResouceType(Resource.Type.Handler);
									resource.setResouceId(handler.getHandlerId());
									resource.setResouceName(handler.getHandlerCode());
									handlerResources.put(handler.getHandlerId(), resource);	
									
									initAutherHandlerCodes(user,handler.getHandlerCode());
								}else{
									this.loopFuncHandlerList(handler, funcHandlers, handlerIdList, user);
								}
								
								if (user.containResouce(Resource.Type.Handler, handlerId) && handler.getOperations().size() > 0){
									this.processOperation(user, handler.getOperations(),operationIdList);									
								}
							}
						}
					}
				}
			}
		}
	}
	private void processOperation(User user,List<Operation> operationList,List<String> operationIdList){
		for (int i=0;i < operationList.size();i++){
			Operation operation = operationList.get(i);
			String operationId = operation.getOperId();
			if (operationIdList.contains(operationId)){
				HashMap<String,Resource> operResources = getResourceRef(user, Resource.Type.Operation);
				Resource resource = new Resource();
				resource.setResouceType(Resource.Type.Operation);
				resource.setResouceId(operation.getOperId());
				resource.setResouceName(operation.getOperName());
				operResources.put(operation.getOperId(), resource);				
			}else{
				this.loopOperationList(operation, operationList, operationIdList, user);
			}
		}
	}
	private void loopOperationList(Operation operation,List<Operation> operationList,List<String> operationIdList,User user){
		boolean isMatched = false;
		for (int i=0;i < operationList.size();i++){
			Operation menuItem = operationList.get(i);
			String operationId = menuItem.getOperId();
			if (operationIdList.contains(operationId)){
				isMatched = true;
				break;
			}
		}
		if (!isMatched){
			HashMap<String,Resource> menuResources = getResourceRef(user, Resource.Type.Operation);
			Resource menuResource = new Resource();
			menuResource.setResouceType(Resource.Type.Operation);
			String operationId = operation.getOperId();
			menuResource.setResouceId(operationId);
			menuResource.setResouceName(operation.getOperName());
			menuResources.put(operationId, menuResource);
		}
	}

	private void loopFuncMenuList(FuncMenu curMenuItem,List<FuncMenu> funcMenuList,List<String> funcMenuIdList,User user){
		boolean isMatched = false;
		for (int i=0;i < funcMenuList.size();i++){
			FuncMenu menuItem = funcMenuList.get(i);
			String menuItemId = menuItem.getFuncId();
			if (funcMenuIdList.contains(menuItemId)){
				isMatched = true;
				break;
			}
		}
		if (!isMatched){
			HashMap<String,Resource> menuResources = getResourceRef(user, Resource.Type.Menu);
			Resource menuResource = new Resource();
			menuResource.setResouceType(Resource.Type.Menu);
			String menuId = curMenuItem.getFuncId();
			menuResource.setResouceId(menuId);
			menuResource.setResouceName(curMenuItem.getFuncName());
			menuResources.put(menuId, menuResource);
		}
	}
	
	private void loopFuncHandlerList(FuncHandler funcHandler,List<FuncHandler> funcHandlerList,List<String> handlerIdList,User user){
		boolean isMatched = false;
		for (int i=0;i < funcHandlerList.size();i++){
			FuncHandler curHandler = funcHandlerList.get(i);
			String funcHandlerId = curHandler.getHandlerId();
			if (handlerIdList.contains(funcHandlerId)){
				isMatched = true;
				break;
			}
		}
		if (!isMatched){
			HashMap<String,Resource> menuResources = getResourceRef(user, Resource.Type.Handler);
			Resource menuResource = new Resource();
			menuResource.setResouceType(Resource.Type.Handler);
			String funcHandlerId = funcHandler.getHandlerId();
			menuResource.setResouceId(funcHandlerId);
			menuResource.setResouceName(funcHandler.getHandlerCode());
			menuResources.put(funcHandlerId, menuResource);
			
			initAutherHandlerCodes(user,funcHandler.getHandlerCode());
		}
	}
	
	@SuppressWarnings("unchecked")
	private void initAutherHandlerCodes(User user,String handlerCode){
		HashMap<String,List<String>> autherHandlerCodes = null;
		if (!user.getExtendProperties().containsKey(AutherHandlerCodes)){
			List<String> temp = new ArrayList<String>();
			HashMap<String,List<String>> authCodeMap = new HashMap<String,List<String>>();
			authCodeMap.put(appName, temp);
			user.getExtendProperties().put(AutherHandlerCodes, authCodeMap);
		}
		
		autherHandlerCodes = (HashMap<String,List<String>>)user.getExtendProperties().get(AutherHandlerCodes);
		if (!autherHandlerCodes.containsKey(appName)){
			List<String> temp = new ArrayList<String>();
			autherHandlerCodes.put(appName, temp);
		}
		
		List<String> codeList = autherHandlerCodes.get(appName);
		codeList.add(handlerCode);
	}
	
	private HashMap<String,Resource> getResourceRef(User user,String resourceType){
		HashMap<String,Resource> result = null;
		if (!user.getResoucesRefs().containsKey(resourceType)){
			HashMap<String,Resource> temp = new HashMap<String,Resource>();
			user.getResoucesRefs().put(resourceType, temp);
		}
		result = user.getResoucesRefs().get(resourceType);
		return result;
	}	
	
	
	public synchronized void removeUser(String userCode){
		if (!useMemcache){
			if (UserCaches.containsKey(userCode)){
				UserCaches.remove(userCode);
			}			
		}else{
			memcachedClient.delete(userCode);
		}
	}
	
	public synchronized void truncateUsers(){
		if (!useMemcache){
			UserCaches.clear();
		}else{
			memcachedClient.flush();
		}	
	}
}
