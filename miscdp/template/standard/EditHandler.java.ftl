<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/standard",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro StandardFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
<#local detailArea = .node.DetailView.DetailEditArea>
<#local paramArea = .node.ListView.ParameterArea>
<#local listTableArea = .node.ListView.ListTableArea>
package ${Util.parsePkg(handler.@editHandlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.domain.*;
import com.agileai.util.*;
import com.agileai.hotweb.domain.*;
import com.agileai.hotweb.renders.*;

public class ${Util.parseClass(handler.@editHandlerClass)} extends StandardEditHandler{
	public ${Util.parseClass(handler.@editHandlerClass)}(){
		super();
		this.listHandlerClass = ${Util.parseClass(handler.@listHandlerClass)}.class;
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
	}

	<#if Util.isValid(resFileModel)>
	public ViewRenderer doSaveAction(DataParam param){
		String responseText = FAIL;
		try {
			String operateType = param.get(OperaType.KEY);
			if (OperaType.CREATE.equals(operateType)){
				getService().createRecord(param);	
			}
			else if (OperaType.UPDATE.equals(operateType)){
				getService().updateRecord(param);	
			}
			responseText = param.get("${resFileModel.bizPrimaryKeyField}");
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}	
	</#if>
	
	protected void processPageAttributes(DataParam param) {
		<#compress>
		<#if Util.isValid(detailArea)><#list detailArea["fa:FormObject"] as formObject><@SetAttribute formAtom=formObject/>
		</#list></#if>
		<@Form.PoupBoxJavaCode paramArea=.node.DetailView.DetailEditArea></@Form.PoupBoxJavaCode>
		</#compress>
	}
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>

<#macro SetAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getOperaAttributeValue("${atom["fa:Name"]}","${atom["fa:DefValue"].@value}")));</#if>
</#macro>