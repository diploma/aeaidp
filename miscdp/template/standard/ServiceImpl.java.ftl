<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/standard",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro StandardFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@ImplClassName)};

import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.domain.DataParam;
import ${service.@InterfaceName};

public class ${Util.parseClass(service.@ImplClassName)} extends StandardServiceImpl 
	implements ${Util.parseClass(service.@InterfaceName)} {
	
	public ${Util.parseClass(service.@ImplClassName)}(){
		super();
	}
	
	<#if Util.isValid(resFileModel)>
	public void deletRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteRecord";
		this.daoHelper.deleteRecords(statementId, param);
		
		statementId = "${resFileModel.sqlNameSpace}."+"deleteRecord";
		DataParam deleteParam = new DataParam();
		String primaryKey = this.getPrimaryKey();
		deleteParam.put("${resFileModel.bizIdField}",param.get(primaryKey));
		this.daoHelper.deleteRecords(statementId, deleteParam);
	}	
	</#if>	
}
</#macro>