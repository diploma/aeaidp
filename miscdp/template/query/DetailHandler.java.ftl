<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/query",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro QueryFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
<#local detailArea = .node.DetailView.DetailViewArea>
<#local paramArea = .node.ListView.ParameterArea>
<#local listTableArea = .node.ListView.ListTableArea>
package ${Util.parsePkg(handler.@detailHandlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.QueryModelDetailHandler;
import com.agileai.domain.*;
import com.agileai.hotweb.domain.*;

public class ${Util.parseClass(handler.@detailHandlerClass)} extends QueryModelDetailHandler{
	public ${Util.parseClass(handler.@detailHandlerClass)}(){
		super();
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
	}
	protected void processPageAttributes(DataParam param) {
		<#compress>
		<#if Util.isValid(detailArea)><#list detailArea["fa:FormObject"] as formObject>
		<@SetAttribute formAtom=formObject/>
		</#list></#if>
		</#compress>
		
	}
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>

<#macro SetAttribute formAtom>
<#local type=formAtom.*[0]?node_name?lower_case>
<#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getAttributeValue("${atom["fa:Name"]}")));</#if>
</#macro>