<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/query",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro QueryFuncModel>
<#local baseInfo = .node.BaseInfo>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${.node.BaseInfo.@detailTitle}</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<script language="javascript">
wrTitle('${.node.BaseInfo.@detailTitle}');
</script>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0">
<@Form.DetailEditArea paramArea=.node.DetailView.DetailViewArea></@Form.DetailEditArea>
</table>
<table width="100%" border="0">
  <tr>
    <td align="center"><input type="button" name="button" id="button" value="关闭窗口" class="formbutton" onClick="parent.PopupBox.closeCurrent()"></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value="" />
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
</#macro>

