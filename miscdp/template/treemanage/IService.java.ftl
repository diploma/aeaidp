<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treemanage",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeManageFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@InterfaceName)};

import com.agileai.hotweb.bizmoduler.core.TreeManage;

public interface ${Util.parseClass(service.@InterfaceName)} extends TreeManage{

}
</#macro>