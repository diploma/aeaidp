<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE sqlMap      
    PUBLIC "-//ibatis.apache.org//DTD SQL Map 2.0//EN"      
    "http://ibatis.apache.org/dtd/sql-map-2.dtd">
<sqlMap namespace="${table.namespace}">  
  <select id="queryTreeRecords" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${table.queryTreeRecordsSQL}
  </select>
  <select id="queryPickTreeRecords" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${table.queryPickTreeRecordsSQL}
  </select>
  <select id="queryTreeRecord" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${table.queryTreeRecordSQL}
  </select>
<#if table.manageTree>   
  <insert id="insertTreeRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.insertTreeRecordSQL}
  </insert>  
  <update id="updateTreeRecord" parameterClass="com.agileai.domain.DataParam">
	${table.updateTreeRecordSQL}
  </update>
  <delete id="deleteTreeRecord" parameterClass="String">
    ${table.deleteTreeRecordSQL}
  </delete> 
</#if>  
  <select id="queryChildTreeRecords" parameterClass="String" resultClass="com.agileai.domain.DataRow">
    ${table.queryChildTreeRecordsSQL}
  </select>
  <select id="queryMaxSortId" parameterClass="String" resultClass="com.agileai.domain.DataRow">
	${table.queryMaxSortIdSQL}
  </select>
  <select id="queryCurLevelRecords" parameterClass="String" resultClass="com.agileai.domain.DataRow">
	${table.queryCurLevelRecordsSQL}
  </select>  
<#list table.contentTableInfoList as contentInfo>
  <select id="find${contentInfo.contentTabId}Records" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${contentInfo.findContentRecordsSQL}
  </select>
  <select id="get${contentInfo.contentTabId}Record" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${contentInfo.getContentRecordSQL}
  </select>
  <insert id="insert${contentInfo.contentTabId}Record" parameterClass="com.agileai.domain.DataParam">
	${contentInfo.insertContentRecordSQL}
  </insert>
  <update id="update${contentInfo.contentTabId}Record" parameterClass="com.agileai.domain.DataParam">
    ${contentInfo.updateContentRecordSQL}
  </update>
  <delete id="delete${contentInfo.contentTabId}Record" parameterClass="com.agileai.domain.DataParam">
    ${contentInfo.deleteContentRecordSQL}
  </delete>
  <#if (contentInfo.existRelateTable)>
  <select id="query${contentInfo.contentTabId}Relation" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${contentInfo.queryContentRelationSQL}
  </select>
  <insert id="insert${contentInfo.contentTabId}Relation" parameterClass="com.agileai.domain.DataParam">
	${contentInfo.insertContentRelationSQL}
  </insert>
  <update id="update${contentInfo.contentTabId}Relation" parameterClass="com.agileai.domain.DataParam">
	${contentInfo.updateContentRelationSQL}
  </update>
  <delete id="delete${contentInfo.contentTabId}Relation" parameterClass="com.agileai.domain.DataParam">
	${contentInfo.deleteContentRelationSQL}
  </delete>
  </#if>
  </#list>  
</sqlMap>