package com.agileai.miscdp.util;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
/**
 * 对话框辅助类
 */
public class DialogUtil {
	public static enum MESSAGE_TYPE {
		INFO, ERROR, WARN
	}
	
	/**
	 * Show Dialog for message (info / error)
	 * @param title
	 * @param message
	 * @param message_type
	 */
	public static void showMessage(final Shell shell,final String title, final String message, final MESSAGE_TYPE message_type) {		
        Display.getDefault().asyncExec(new Runnable() {
           public void run() {
    		   Action action = new Action("Message") {
    				public void run() {
    					if (message_type == MESSAGE_TYPE.INFO) {
    						MessageDialog.openInformation(shell, title, message);
    					} else if (message_type == MESSAGE_TYPE.ERROR) {
    						MessageDialog.openError(shell, title, message);
    					} else if (message_type == MESSAGE_TYPE.WARN) {
    						MessageDialog.openWarning(shell, title, message);
    					}
    				}
    			};
    			action.run();
           }
        });
    }	
	
	public static void showInfoMessage(final String message) {
		final Shell shell = new Shell();
        Display.getDefault().asyncExec(new Runnable() {
           public void run() {
    		   Action action = new Action("Message") {
    				public void run() {
    					final String title = "信息提示";
    					MessageDialog.openInformation(shell, title, message);
    				}
    			};
    			action.run();
           }
        });
    }	
	
	public static Point getCenterInitialLocation(Shell shell) {
	       int x = shell.getParent().getLocation().x+(shell.getParent().getBounds().width-shell.getSize().x)/2;
	       int y = shell.getParent().getLocation().y+(shell.getParent().getBounds().height-shell.getSize().y)/2;
	       Point location = new Point(x,y);
	       return location;
	   }	
	/**
	 * Show Sync Dialog for message (info / error)
	 * @param title
	 * @param message
	 * @param message_type
	 */
	public static void showSyncMessage(final Shell shell,final String title, final String message, final MESSAGE_TYPE message_type) {		
        Display.getDefault().syncExec(new Runnable() {
           public void run() {
    		   Action action = new Action("Message") {
    				public void run() {
    					if (message_type == MESSAGE_TYPE.INFO) {
    						MessageDialog.openInformation(shell, title, message);
    					} else if (message_type == MESSAGE_TYPE.ERROR) {
    						MessageDialog.openError(shell, title, message);
    					} else if (message_type == MESSAGE_TYPE.WARN) {
    						MessageDialog.openWarning(shell, title, message);
    					}
    				}
    			};
    			action.run();
           }
        });
    }	
	
	/**
	 * Create a sync thread for Dialog
	 * @param dialog
	 */
	public static void openSyncDialog(final Dialog dialog) {
		Display.getDefault().syncExec(new Runnable() {
	        public void run() {
	        	Action action = new Action("Dialog") {
	        		public void run() {
	        			dialog.open();
	        		}
	        	};
	        	action.run();
			}    
	    });
	}
}
