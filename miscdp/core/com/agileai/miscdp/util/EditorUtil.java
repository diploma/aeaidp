﻿package com.agileai.miscdp.util;

import org.eclipse.core.resources.IFile;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
/**
 * 编辑器辅助类
 */
public class EditorUtil {
    /**
     * Close editor.
     */
	public static void closeEditor() {
		IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		activePage.closeEditor(activePage.getActiveEditor(), false);
	}
	
    /**
     * Open editor.
     */
	public static void openEditor(IFile file) {
		IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		try {
			IDE.openEditor(activePage, file, true);
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}
	
    /**
     * Close and re-open editor.
     */
	public static void reOpenEditor(IFile file) {
		closeEditor();
		openEditor(file);
	}
}
