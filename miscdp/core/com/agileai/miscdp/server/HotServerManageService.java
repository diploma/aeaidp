
package com.agileai.miscdp.server;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by Apache CXF 2.1
 * Wed Oct 26 12:30:22 CST 2016
 * Generated source version: 2.1
 * 
 */

@WebServiceClient(name = "HotServerManageService", targetNamespace = "http://service.hotserver.agileai.com/", wsdlLocation = "http://localhost:8080/HotServer/services/HotServerManage?wsdl")
public class HotServerManageService extends Service {

    public final static URL WSDL_LOCATION;
    public final static QName SERVICE = new QName("http://service.hotserver.agileai.com/", "HotServerManageService");
    public final static QName HotServerManagePort = new QName("http://service.hotserver.agileai.com/", "HotServerManagePort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/HotServer/services/HotServerManage?wsdl");
        } catch (MalformedURLException e) {
            System.err.println("Can not initialize the default wsdl from http://localhost:8080/HotServer/services/HotServerManage?wsdl");
            // e.printStackTrace();
        }
        WSDL_LOCATION = url;
    }

    public HotServerManageService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public HotServerManageService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public HotServerManageService() {
        super(WSDL_LOCATION, SERVICE);
    }

    /**
     * 
     * @return
     *     returns HotServerManage
     */
    @WebEndpoint(name = "HotServerManagePort")
    public HotServerManage getHotServerManagePort() {
        return super.getPort(HotServerManagePort, HotServerManage.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns HotServerManage
     */
    @WebEndpoint(name = "HotServerManagePort")
    public HotServerManage getHotServerManagePort(WebServiceFeature... features) {
        return super.getPort(HotServerManagePort, HotServerManage.class, features);
    }

}
