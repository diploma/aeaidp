package com.agileai.miscdp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;

import com.agileai.miscdp.hotweb.ConsoleHandler;
import com.agileai.miscdp.hotweb.domain.DeployResource;
import com.agileai.miscdp.hotweb.domain.DeployResource.FileTypes;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.server.DeployableZip;
import com.agileai.miscdp.util.FileUtil;
import com.agileai.miscdp.util.JavaModelUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ZipHelper;

public class DeployManager {
    private HashMap<String,String> moduleJspPathMap = null;
    private HashMap<String,String> moduleJavaPathMap = null;
    
    private List<DeployResource> selectedResourceList = null; 
	private List<String> selectedModuleNames = null;

    private List<String> deleteDirs = null;
	private List<String> folderPathList = null;
	private List<String> packagePathList = null;
    private List<String> moduleNames = null;
    
	private String tempDir = null;
	private File tempDirFile = null;
	private String tempDeployDir = null;
	
	private String serviceJavaPath = null;
	
	private String appName = null;
	private ProjectConfig projectConfig = null;
	
	public DeployManager(String appName){
	    this.moduleNames = new ArrayList<String>();
	    this.moduleJspPathMap = new HashMap<String,String>();
	    this.moduleJavaPathMap = new HashMap<String,String>();
	    this.selectedResourceList = new ArrayList<DeployResource>();
	    
	    this.selectedModuleNames = new ArrayList<String>();
	    
	    this.deleteDirs = new ArrayList<String>();
	    this.folderPathList = new ArrayList<String>();
	    this.packagePathList = new ArrayList<String>();
	    
	    this.appName = appName;
	    this.projectConfig = MiscdpUtil.getProjectConfig(appName);
	}
	
	
	private void initTempDirs(IJavaProject javaProject){
		String appAlias = projectConfig.getProjectAlias();
		IProject project = javaProject.getProject();
		File projectFile = project.getLocation().toFile().getAbsoluteFile();
		String parentPath = projectFile.getParentFile().getPath();
		this.tempDeployDir = parentPath + "/tempDeploy";
		File tempDeployFile = new File(tempDeployDir);
		if (!tempDeployFile.exists()){
			tempDeployFile.mkdir();
		}
		
		this.tempDir = parentPath + "/tempDir/"+appAlias;
		this.tempDirFile = new File(tempDir);
		if (tempDirFile.exists()){
			if (tempDirFile.listFiles().length > 0){
				try {
					FileUtil.deleteWholeDirectory(tempDirFile);
					tempDirFile.mkdirs();					
				} catch (Exception e) {
					MiscdpPlugin.getDefault().logError(e.getLocalizedMessage(),e);
				}
			}
		}else{
			tempDirFile.mkdirs();
		}
	}	
	
	private void copyWebRoot(File from, File to, boolean includeSubdirs,
			List<String> excludedDirNames) throws IOException {
		if (!to.exists()) {
			to.mkdirs();
		}
		if ((from == null) || (!from.isDirectory()) || (!to.isDirectory()))
			return;
		File[] fs = from.listFiles();
		if (fs == null)
			return;
		for (int i = 0; i < fs.length; i++) {
			String n = fs[i].getName();
			File c = new File(to, n);
			if ((fs[i].isDirectory()) && (!includeSubdirs))
				continue;
			if (fs[i].isDirectory()) {
				if (excludedDirNames.contains(fs[i].getName())) {
					continue;
				}
				c.mkdirs();
				copyDir(fs[i], c, includeSubdirs, excludedDirNames);
			} else {
				FileUtil.copyFileToDir(fs[i], to);
			}
		}
	}	
	
	private void copyDir(File from, File to, boolean includeSubdirs,
			List<String> excludedDirNames) throws IOException {
		if (!to.exists()) {
			to.mkdirs();
		}
		if ((from == null) || (!from.isDirectory()) || (!to.isDirectory()))
			return;
		File[] fs = from.listFiles();
		if (fs == null)
			return;
		for (int i = 0; i < fs.length; i++) {
			String n = fs[i].getName();
			File c = new File(to, n);
			if ((fs[i].isDirectory()) && (!includeSubdirs))
				continue;
			if (fs[i].isDirectory()) {
				if (excludedDirNames.contains(fs[i].getName())) {
					continue;
				}
				if ("module".equals(fs[i].getName()) && isInClassesDir(fs[i])){
					continue;
				}				
				c.mkdirs();
				copyDir(fs[i], c, includeSubdirs, excludedDirNames);
			} else {
				FileUtil.copyFileToDir(fs[i], to);
			}
		}
	}		
	
	private boolean isInClassesDir(File file){
		boolean result = false;
		String matchPath = "WEB-INF"+File.separator+"classes";
		if (file.getAbsolutePath().indexOf(matchPath) > -1){
			result = true;
		}
		return result;
	}
	
	public DeployableZip buildDeployZip4App(IJavaProject javaProject,boolean needReload) throws IOException{
		DeployableZip deployableZip = new DeployableZip();
		deployableZip.setType(DeployResource.FileTypes.Applicaiton);
		deployableZip.setNeedReload(needReload);
		
		ConsoleHandler.info("prepare zip resource ...");
		
		this.initTempDirs(javaProject);
		
		File sourceWeb = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot"));
		List<String> excludeFiles = new ArrayList<String>();
//		excludeFiles.add("module");
		excludeFiles.add(".svn");
		excludeFiles.add(".CVS");
		copyWebRoot(sourceWeb,tempDirFile,true,excludeFiles);
		
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
		String subDir = projectConfig.getMainPkg().replaceAll("\\.", "/");
		if (ProjectConfig.ProjectType.IntegrateWebProject.equals(projectConfig.getProjectType())){
			deployableZip.setIntegrateApp(true);
		}else{
			deployableZip.setIntegrateApp(false);
		}
		
		copyModules(appName, subDir, moduleNames, false);
		copyServices(appName, subDir);
		
		ConsoleHandler.info("do zip files ...");
		
		ZipHelper zipUtil = new ZipHelper();
		String tempZipFile = tempDeployDir + "/" + projectConfig.getProjectAlias() + ".zip";
		zipUtil.doZip(tempDirFile.getAbsolutePath(),tempZipFile);
		
		DataSource source = new FileDataSource(new File(tempZipFile));
		deployableZip.setDataHandler(new DataHandler(source));
		return deployableZip;
	}
	
	private void copyModules(String appName,String subDir,List<String> moduleNames,boolean copyJspFolder) throws IOException{
		File sqlmapDesDir = new File(tempDir+"/WEB-INF/classes/sqlmap");
		if (!sqlmapDesDir.exists()){
			sqlmapDesDir.mkdirs();			
		}
		for (int i=0;i < moduleNames.size();i++){
			String moduleName = moduleNames.get(i);
			File tempClassesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName);
			tempClassesDir.mkdirs();
			
			File sourceModuleClassesFile = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot/WEB-INF/classes/"+subDir+"/module/" + moduleName));
			if (sourceModuleClassesFile.exists() && sourceModuleClassesFile.listFiles().length > 0){
				FileUtil.copyDir(sourceModuleClassesFile,tempClassesDir,true,new ArrayList<String>());	
			}
			
			if (copyJspFolder){
				File sourceModuleJspFile = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot/jsp/" + moduleName));
				if (sourceModuleJspFile.exists() && sourceModuleJspFile.listFiles().length > 0){
					File tempJspDesDir = new File(tempDir+"/jsp/" + moduleName);
					tempJspDesDir.mkdirs();
					
					FileUtil.copyDir(sourceModuleJspFile,tempJspDesDir,true,new ArrayList<String>());	
				}	
				
				File sourceModulePagesFile = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot/pages/" + moduleName));
				if (sourceModulePagesFile.exists() && sourceModulePagesFile.listFiles().length > 0){
					File tempPagesDesDir = new File(tempDir+"/pages/" + moduleName);
					tempPagesDesDir.mkdirs();
					
					FileUtil.copyDir(sourceModulePagesFile,tempPagesDesDir,true,new ArrayList<String>());	
				}
			}
			
			File tempHandleClassSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/handler");
			if (tempHandleClassSrcDir.exists()){
				File tempHandlerClassesDesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/"+subDir+"/module/"+moduleName+"/handler");
				tempHandlerClassesDesDir.mkdirs();
				FileUtil.copyDir(tempHandleClassSrcDir,tempHandlerClassesDesDir,true,new ArrayList<String>());	
			}

			File tempServiceClassSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/service");
			if (tempServiceClassSrcDir.exists()){
				File tempServiceClassesDesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/"+subDir+"/module/"+moduleName+"/service");
				tempServiceClassesDesDir.mkdirs();
				FileUtil.copyDir(tempServiceClassSrcDir,tempServiceClassesDesDir,true,new ArrayList<String>());	
			}

			File tempPorltetClassSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/portlet");
			if (tempPorltetClassSrcDir.exists()){
				File tempPortletClassesDesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/"+subDir+"/module/"+moduleName+"/portlet");
				tempPortletClassesDesDir.mkdirs();
				FileUtil.copyDir(tempPorltetClassSrcDir,tempPortletClassesDesDir,true,new ArrayList<String>());	
			}
			
			File tempSqlmapSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/sqlmap");
			FileUtil.copyDir(tempSqlmapSrcDir,sqlmapDesDir,true,new ArrayList<String>());	
			
			if (tempHandleClassSrcDir.exists()){
				FileUtil.deleteWholeDirectory(tempHandleClassSrcDir);				
			}
			if (tempServiceClassSrcDir.exists()){
				FileUtil.deleteWholeDirectory(tempServiceClassSrcDir);	
			}
			if (tempSqlmapSrcDir.exists()){
				FileUtil.deleteWholeDirectory(tempSqlmapSrcDir);				
			}
			if (tempPorltetClassSrcDir.exists()){
				FileUtil.deleteWholeDirectory(tempPorltetClassSrcDir);				
			}
		}
	}
	
	
	private void copyServices(String appName,String subDir) throws IOException{
		File tempClassesDir = new File(tempDir+"/WEB-INF/services/"+subDir+"/service");
		tempClassesDir.mkdirs();
		
		File sourceModuleClassesFile = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot/WEB-INF/classes/"+subDir+"/service"));
		if (sourceModuleClassesFile.exists() && sourceModuleClassesFile.listFiles().length > 0){
			FileUtil.copyDir(sourceModuleClassesFile,tempClassesDir,true,new ArrayList<String>());	
		}
		
		File tempClassesSrcDir = new File(tempDir+"/WEB-INF/classes/"+subDir+"/service");
		if (tempClassesSrcDir.exists()){
			FileUtil.deleteWholeDirectory(tempClassesSrcDir);
		}
	}
	
	public DeployableZip buildDeployZip4Modules(IJavaProject javaProject,List<String> selectedModuleNames,boolean needReload) throws IOException{
		DeployableZip deployableZip = new DeployableZip();
		deployableZip.setType(DeployResource.FileTypes.Module);
		deployableZip.setNeedReload(needReload);
		
		ConsoleHandler.info("prepare zip resource ...");
		
		this.initTempDirs(javaProject);
		
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
		String subDir = projectConfig.getMainPkg().replaceAll("\\.", "/");
		
		copyModules(appName,subDir,this.selectedModuleNames,true);
		
		ConsoleHandler.info("do zip files ...");
		
		ZipHelper zipUtil = new ZipHelper();
		String tempZipFile = tempDeployDir + "/" + projectConfig.getProjectAlias() + ".zip";
		zipUtil.doZip(tempDirFile.getAbsolutePath(),tempZipFile);
		
		DataSource source = new FileDataSource(new File(tempZipFile));
		deployableZip.setDataHandler(new DataHandler(source));
		
		return deployableZip;
	}
	
	public DeployableZip buildDeployZip4Services(IJavaProject javaProject,boolean needReload) throws IOException{
		DeployableZip deployableZip = new DeployableZip();
		deployableZip.setType(DeployResource.FileTypes.Module);
		deployableZip.setNeedReload(needReload);
		
		ConsoleHandler.info("prepare zip resource ...");
		
		this.initTempDirs(javaProject);
		
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
		String subDir = projectConfig.getMainPkg().replaceAll("\\.", "/");
		
		copyServices(appName,subDir);
		
		ConsoleHandler.info("do zip files ...");
		
		ZipHelper zipUtil = new ZipHelper();
		String tempZipFile = tempDeployDir + "/" + projectConfig.getProjectAlias() + ".zip";
		zipUtil.doZip(tempDirFile.getAbsolutePath(),tempZipFile);
		
		DataSource source = new FileDataSource(new File(tempZipFile));
		deployableZip.setDataHandler(new DataHandler(source));
		
		return deployableZip;
	}
	
	private void checkParentFolder(String compilePath){
		Iterator<String> folderIter = folderPathList.iterator();
		while (folderIter.hasNext()){
			String folderPath = folderIter.next();
			if (!compilePath.equals(folderPath) && compilePath.startsWith(folderPath)){
				folderIter.remove();
				break;
			}
		}
		
		Iterator<String> packagePathIter = this.packagePathList.iterator();
		while (packagePathIter.hasNext()){
			String packagePath = packagePathIter.next();
			if (!compilePath.equals(packagePath) && compilePath.startsWith(packagePath)){
				packagePathIter.remove();
				break;
			}		
		}
	}
	
	private String parseXmlFileName(String path){
		String result = null;
		int lastSplashIndex = path.lastIndexOf("/");
		result = path.substring(lastSplashIndex+1,path.length());
		return result;
	}
	
	public DeployableZip buildDeployZip4AppFiles(IJavaProject javaProject,List<DeployResource> selectedDeployFilesList
			,boolean needReload) throws IOException{
		DeployableZip deployableZip = new DeployableZip();
		deployableZip.setType(DeployResource.FileTypes.Files);
		deployableZip.setNeedReload(needReload);
		
		this.initTempDirs(javaProject);
		
		ConsoleHandler.info("prepare zip resource ...");
		
		File projectFile = javaProject.getProject().getLocation().toFile().getAbsoluteFile();
		
		String subDir = projectConfig.getMainPkg().replaceAll("\\.", "/");
		
		boolean containService = false;
		
		for (int i=0;i < selectedDeployFilesList.size();i++){
			DeployResource deployResource = selectedDeployFilesList.get(i);
			String compilePath = deployResource.getCompilePath();
			checkParentFolder(compilePath);
			
			String currentPath = projectFile.getAbsolutePath()+ File.separator + "WebRoot" + compilePath;
			if (FileTypes.Folder.equals(deployResource.getFileType())
					|| FileTypes.Package.equals(deployResource.getFileType())){
				File tempResouceDesDir = null;
				if (deployResource.isSource()){
					if (compilePath.indexOf(subDir+"/module/") > -1){
//						String moduleName = parseModuleName(compilePath);
						if (compilePath.endsWith("sqlmap")){
							compilePath = "/WEB-INF/classes/sqlmap";
						}
//						else{
//							compilePath = compilePath.replaceAll("/classes/","/modules/"+moduleName+"/");
//						}
						tempResouceDesDir = new File(tempDir + compilePath);
					}
					else if (compilePath.endsWith("/module")){
						copyModules(appName,subDir,this.moduleNames,false);
						continue;
					}
					else if (compilePath.indexOf(subDir+"/service/") > -1){
						containService = true;
						continue;
					}
					else{
						tempResouceDesDir = new File(tempDir + compilePath);
					}
				}else{
					tempResouceDesDir = new File(tempDir + compilePath);
				}
				if (!tempResouceDesDir.exists()){
					tempResouceDesDir.mkdirs();
				}
				File tempResouceSrcDir = new File(currentPath);
				FileUtil.copyDir(tempResouceSrcDir, tempResouceDesDir);
			}
			else{
				File tempResouceDesFile = null;
				if (deployResource.isSource() && compilePath.indexOf(subDir+"/module/") > -1){
					String moduleName = parseModuleName(compilePath);
					if (compilePath.endsWith(".xml")){
						String fileName = this.parseXmlFileName(compilePath);
						if (compilePath.indexOf("/sqlmap/") > -1){
							compilePath = "/WEB-INF/classes/sqlmap/"+fileName;
						}else{
							compilePath = "/WEB-INF/modules/"+moduleName+"/"+fileName;							
						}
					}else{
						compilePath = compilePath.replaceAll("/classes/","/modules/"+moduleName+"/");						
					}
					tempResouceDesFile = new File(tempDir + compilePath);
				}
				if (deployResource.isSource() && compilePath.indexOf(subDir+"/service/") > -1){
					containService = true;
				}				
				else{
					tempResouceDesFile = new File(tempDir + compilePath);
				}
				if (tempResouceDesFile != null){
					File tempResouceSrcFile = new File(currentPath);
					FileUtil.copyFile(tempResouceSrcFile, tempResouceDesFile);					
				}
			}
		}
		if (containService){
			copyServices(appName, subDir);
		}
		
		List<String> containModules = new ArrayList<String>();
		parseCotainModules(containModules,subDir);
		if (containModules.size() > 0){
			copyModules(appName, subDir, containModules, false);
			deleteContainModules(containModules, subDir);
		}
		deleteContainServices(subDir);
		
		ConsoleHandler.info("do zip files ...");
		
		ZipHelper zipUtil = new ZipHelper();
		String tempZipFile = tempDeployDir + "/" + projectConfig.getProjectAlias() + ".zip";
		zipUtil.doZip(tempDirFile.getAbsolutePath(),tempZipFile);
		
		DataSource source = new FileDataSource(new File(tempZipFile));
		deployableZip.setDataHandler(new DataHandler(source));
		
		if (containService){
			deployableZip.setReloadService(true);
		}else{
			deployableZip.setReloadService(false);
		}
		
		return deployableZip;
	}
	
	private void parseCotainModules(List<String> containModules,String subDir){
		File moduleDir = new File(tempDir+"/WEB-INF/classes/"+subDir+"/module");
		if (moduleDir.exists()){
			File[] moduleFiles = moduleDir.listFiles();
			if (moduleDir != null && moduleFiles.length > 0){
				for (int i=0;i < moduleFiles.length;i++){
					File moduleFile = moduleFiles[i];
					containModules.add(moduleFile.getName());
				}
			}
		}
	}
	
	private void deleteContainModules(List<String> containModules,String subDir){
		File moduleDir = new File(tempDir+"/WEB-INF/classes/"+subDir+"/module");
		if (moduleDir.exists()){
			try {
				FileUtil.deleteWholeDirectory(moduleDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void deleteContainServices(String subDir){
		File serviceDir = new File(tempDir+"/WEB-INF/classes/"+subDir+"/service");
		if (serviceDir.exists()){
			try {
				FileUtil.deleteWholeDirectory(serviceDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<DeployResource> buildApplicaitonInputList(IJavaProject javaProject){
		List<DeployResource> result = new ArrayList<DeployResource>();
		String appName = javaProject.getProject().getName();
		DeployResource deployResource = new DeployResource();
		deployResource.setFileName(appName);
		deployResource.setFileType(FileTypes.Applicaiton);
		result.add(deployResource);
		
		this.selectedResourceList.add(deployResource);
		
		return result;
	}

	public List<DeployResource> buildModulesInputList(IJavaProject javaProject){
		List<DeployResource> result = new ArrayList<DeployResource>();
		for (int i=0;i < moduleNames.size();i++){
			String moduleName = moduleNames.get(i);
			DeployResource deployResource = new DeployResource();
			deployResource.setFileName(moduleName);
			deployResource.setFileType(FileTypes.Module);
			
			if (this.selectedModuleNames.contains(moduleName)){
				this.selectedResourceList.add(deployResource);
			}
			result.add(deployResource);
		}
		return result;
	}
	
	public List<DeployResource> buildServicesInputList(IJavaProject javaProject){
		List<DeployResource> result = new ArrayList<DeployResource>();
		
		DeployResource deployResource = new DeployResource();
		deployResource.setFileName("service");
		deployResource.setFileType(FileTypes.Package);
		deployResource.setFilePath(this.serviceJavaPath.toString());
		deployResource.setSource(true);
		
		String srcFilesPrefix = "/"+appName+"/src";
		String tempPath = this.serviceJavaPath.substring(srcFilesPrefix.length());
		String comilePath = "/WEB-INF/classes"+tempPath;
		deployResource.setCompilePath(comilePath);
		result.add(deployResource);
		
		this.selectedResourceList.add(deployResource);
		return result;
	}
	
	public String parseModuleName(String path){
		String result = null;
		int index = path.indexOf("/module/");
		int endIndex = path.indexOf("/", index+8);
		if (endIndex < 0){
			endIndex = path.length();
		}
		result = path.substring(index+8,endIndex);
		return result;
	}
	
	public List<DeployResource> buildAppFilesInputList(List<?> resources,IJavaProject javaProject){
		List<DeployResource> result = new ArrayList<DeployResource>();
		String appName = javaProject.getProject().getName();

		String srcFilesPrefix = "/"+appName+"/src";
		String webRootFilesPefix = "/"+appName+"/WebRoot";
		
		for (int i=0;i < resources.size();i++){
			Object object = resources.get(i);
			if (object instanceof IProject || object instanceof IJavaProject){
				continue;
			}
			if (object instanceof IResource){
				if (object instanceof IFolder){
					IResource folder = (IResource)object;	
					String folderName = folder.getName();
					if ("WebRoot".equals(folderName)){
						continue;
					}
					String folderPath = folder.getFullPath().toString();
					DeployResource deployResource = new DeployResource();
					deployResource.setFileName(folderName);
					deployResource.setFileType(FileTypes.Folder);
					deployResource.setFilePath(folderPath);
					deployResource.setCompilePath(folderPath.substring(webRootFilesPefix.length()));
					
					result.add(deployResource);
					this.selectedResourceList.add(deployResource);
				}else{
					IResource resource = (IResource)object;
					String resPath = resource.getFullPath().toString();
					
					DeployResource deployResource = new DeployResource();
					deployResource.setFileName(resource.getName());
					deployResource.setFileType(resource.getFileExtension());
					deployResource.setFilePath(resPath);

					if (resPath.startsWith(srcFilesPrefix)){
						String tempPath = resPath.substring(srcFilesPrefix.length());
						String compilePath = "/WEB-INF/classes"+tempPath;
						deployResource.setCompilePath(compilePath);						
						deployResource.setSource(true);
					}else{
						deployResource.setCompilePath(resPath.substring(webRootFilesPefix.length()));	
					}
					result.add(deployResource);
					this.selectedResourceList.add(deployResource);
				}
			}
			else if (object instanceof IJavaElement){
				if (object instanceof IPackageFragment){
					IPackageFragment javaPackage = (IPackageFragment)object;
					String packagePath = javaPackage.getPath().toString();
					
					DeployResource deployResource = new DeployResource();
					deployResource.setFileName(javaPackage.getElementName());
					deployResource.setFileType(FileTypes.Package);
					deployResource.setFilePath(packagePath);
					deployResource.setSource(true);
					
					String tempPath = packagePath.substring(srcFilesPrefix.length());
					String comilePath = "/WEB-INF/classes"+tempPath;
					deployResource.setCompilePath(comilePath);
					result.add(deployResource);
					this.selectedResourceList.add(deployResource);
				}else{
					IJavaElement javaElement = (IJavaElement)object;
					String elementName = javaElement.getElementName();
					if ("src".equals(elementName)){
						continue;
					}
					String resPath = javaElement.getPath().toString();
					DeployResource deployResource = new DeployResource();
					deployResource.setFileName(elementName);
					deployResource.setFileType(FileTypes.Java);
					deployResource.setFilePath(resPath);
					deployResource.setSource(true);
					
					String tempPath = resPath.substring(srcFilesPrefix.length());
					String comilePath = "/WEB-INF/classes"+tempPath;
					if (comilePath.endsWith(".java")){
						comilePath = comilePath.replaceAll(".java", ".class");						
					}
					deployResource.setCompilePath(comilePath);						
					result.add(deployResource);
					this.selectedResourceList.add(deployResource);
				}
			}
		}
		return result;
	}
	
	public static IJavaProject retriveJavaProject(Object object){
		IJavaProject result = null;
		if (object instanceof IResource){
			IResource resource = (IResource)object;
			IProject project = resource.getProject();
			result = JavaModelUtil.getJavaProjectFromProject(project);
		}
		else if (object instanceof IJavaElement){
			IJavaElement javaElement = (IJavaElement)object;
			result = javaElement.getJavaProject();
		}
		return result;
	}
	
	private boolean isValidModuleJspPath(String resPath){
		boolean result = false;
		Iterator<String> keys = moduleJspPathMap.keySet().iterator();
		while (keys.hasNext()){
			String moduleJspPath = keys.next();
			if (resPath.startsWith(moduleJspPath)){
				result = true;
				String moduleName = moduleJspPathMap.get(moduleJspPath);
				if (!selectedModuleNames.contains(moduleName)){
					this.selectedModuleNames.add(moduleName);					
				}
				break;
			}			
		}
		return result;
	}
	
	private boolean isValidServicePath(String resPath){
		boolean result = false;
		if (resPath.startsWith(this.serviceJavaPath)){
			result = true;
		}
		return result;
	}
	
	private boolean isValidModuleJavaPath(String resPath){
		boolean result = false;
		Iterator<String> keys = moduleJavaPathMap.keySet().iterator();
		while (keys.hasNext()){
			String moduleJavaPath = keys.next();
			if (resPath.startsWith(moduleJavaPath)){
				result = true;
				String moduleName = moduleJavaPathMap.get(moduleJavaPath);
				if (!selectedModuleNames.contains(moduleName)){
					this.selectedModuleNames.add(moduleName);					
				}
				break;
			}
		}
		return result;
	}

	public void parseModuleNames(IJavaProject javaProject){
		try {
			String appName = javaProject.getProject().getName();
			ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
			String mainPkg = projectConfig.getMainPkg();
			String mainPkgExt = mainPkg.replaceAll("\\.", "/");
			IPath path = javaProject.getProject().getLocation().append("/src/"+mainPkgExt+"/module");
			File file = path.toFile();
			if (file != null){
				IPath projectPath = javaProject.getProject().getFullPath();
				
				String[] childFiles = file.list();
				if (childFiles != null){
					for (int i=0;i < childFiles.length;i++){
						String moduleName = childFiles[i];
						this.moduleNames.add(moduleName);
						
						IPath tempModuleJavaPath = projectPath.append("/src/"+mainPkgExt+"/module/"+moduleName);
						this.moduleJavaPathMap.put(tempModuleJavaPath.toString(),moduleName);
						
						IPath tempModuleJspPath = projectPath.append("/WebRoot/jsp/"+moduleName);
						this.moduleJspPathMap.put(tempModuleJspPath.toString(),moduleName);
					}
				}
				
				IPath tempServiceJavaPath = projectPath.append("/src/"+mainPkgExt+"/service");
				this.serviceJavaPath = tempServiceJavaPath.toString();
			}

		} catch (Exception e) {
			ConsoleHandler.error(e.getLocalizedMessage());
		}
	}	
	
	public boolean isOnlyServiceResources(List<?> resources){
		boolean result = true;
		for (int i=0;i < resources.size();i++){
			Object object = resources.get(i);
			if (object instanceof IResource){
				IResource resource = (IResource)object;
				String resPath = resource.getFullPath().makeAbsolute().toString();
				if (!isValidServicePath(resPath)){
					result = false;
					break;
				}
			}
			else if (object instanceof IJavaElement){
				IJavaElement javaElement = (IJavaElement)object;
				String resPath = javaElement.getPath().makeAbsolute().toString();
				if (!isValidServicePath(resPath)){
					result = false;
					break;
				}
			}
		}
		
		return result;
	}
	
	public boolean isAllModuleResources(List<?> resources){
		boolean result = true;
		for (int i=0;i < resources.size();i++){
			Object object = resources.get(i);
			if (object instanceof IResource){
				IResource resource = (IResource)object;
				String resPath = resource.getFullPath().makeAbsolute().toString();
				if (!isValidModuleJspPath(resPath) && !isValidModuleJavaPath(resPath)){
					result = false;
					break;
				}
			}
			else if (object instanceof IJavaElement){
				IJavaElement javaElement = (IJavaElement)object;
				String resPath = javaElement.getPath().makeAbsolute().toString();
				if (!isValidModuleJavaPath(resPath)){
					result = false;
					break;
				}
			}
		}
		boolean isAllJsp = true;
		if (result){
			for (int i=0;i < resources.size();i++){
				Object object = resources.get(i);
				if (object instanceof IResource){
					IResource resource = (IResource)object;
					String resPath = resource.getFullPath().makeAbsolute().toString();
					if (!resPath.endsWith(".jsp")){
						isAllJsp = false;
						break;
					}
				}
				else if (object instanceof IJavaElement){
					isAllJsp = false;
					break;
				}
			}	
		}
		if (isAllJsp){
			result = false;
		}
		return result;
	}
	
    public List<DeployResource> getSelectedResourceList() {
		return selectedResourceList;
	}	
	
	public List<String> getFolderPathList(){
		return folderPathList;
	}

	public List<String> getDeleteDirs(){
		return deleteDirs;
	}
	
	public List<String> getPackagePathList(){
		return packagePathList;
	}   
	
	public List<String> getSelectedModuleNames(){
		return selectedModuleNames;
	}
}
