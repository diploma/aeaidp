package com.agileai.miscdp;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.SAXParserFactory;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 * 插件主类
 */
public class MiscdpPlugin extends AbstractUIPlugin {
	private static MiscdpPlugin plugin;
	public static final String PLUGIN_ID = DeveloperConst.PLUGIN_ID;
	
	@SuppressWarnings("rawtypes")
	private ServiceTracker parserTracker = null;
	
	public MiscdpPlugin() {
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public static MiscdpPlugin getDefault() {
		return plugin;
	}
	public static String getPluginId() {
		return PLUGIN_ID;
	}
	public static void log(IStatus status) {
		getDefault().getLog().log(status);
	}
	/**
	 * @generated
	 */
	public void logError(String error) {
		logError(error, null);
	}

	/**
	 * @generated
	 */
	public void logError(String error, Throwable throwable) {
		if (error == null && throwable != null) {
			error = throwable.getMessage();
		}
		getLog().log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.OK, error, throwable));
		debug(error, throwable);
	}

	/**
	 * @generated
	 */
	public void logInfo(String message) {
		logInfo(message, null);
	}

	/**
	 * @generated
	 */
	public void logInfo(String message, Throwable throwable) {
		if (message == null && throwable != null) {
			message = throwable.getMessage();
		}
		getLog().log(new Status(IStatus.INFO, PLUGIN_ID, IStatus.OK, message, throwable));
		debug(message, throwable);
	}

	/**
	 * @generated
	 */
	private void debug(String message, Throwable throwable) {
		if (!isDebugging()) {
			return;
		}
		if (message != null) {
			System.err.println(message);
		}
		if (throwable != null) {
			throwable.printStackTrace();
		}
	}
	public static void log(Throwable e) {
		log(new Status(4, "com.agileai.miscdp.studio", 100001, "Internal Error",e));
	}
	public static Shell getActiveWorkbenchShell() {
		IWorkbenchWindow window = getActiveWorkbenchWindow();
		if (window != null) {
			return window.getShell();
		}
		return null;
	}	
	public static IWorkbenchWindow getActiveWorkbenchWindow() {
		return getDefault().getWorkbench().getActiveWorkbenchWindow();
	}
	
	public Image getImage(String folderPath, String name) {
		if (name == null) {
			return null;
		}
		ImageRegistry images = getImageRegistry();
		Image image = images.get(name);
		if (image == null)
			try {
				URL _pluginBase = getBundle().getEntry("/");

				ImageDescriptor id = ImageDescriptor.createFromURL(new URL(
						_pluginBase, folderPath + "/" + name));
				images.put(name, id);

				image = images.get(name);
			} catch (MalformedURLException localMalformedURLException) {
			}
		return image;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SAXParserFactory getFactory()
	  {
	    if (this.parserTracker == null) {
	      this.parserTracker = new ServiceTracker(getBundle().getBundleContext(), SAXParserFactory.class.getName(), null);
	      this.parserTracker.open();
	    }
	    SAXParserFactory theFactory = (SAXParserFactory)this.parserTracker.getService();
	    if (theFactory != null)
	      theFactory.setNamespaceAware(true);
	    return theFactory;
	  }	
}
