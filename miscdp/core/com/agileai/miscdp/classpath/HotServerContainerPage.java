package com.agileai.miscdp.classpath;

import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.wizards.IClasspathContainerPage;
import org.eclipse.jdt.ui.wizards.IClasspathContainerPageExtension;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class HotServerContainerPage extends WizardPage 
               implements IClasspathContainerPage, IClasspathContainerPageExtension {
    private final static String DEFAULT_EXTS = "jar,zip";
    
    private IJavaProject proj;
    private Text extText;
    private Text dirText;
    private Composite composite;

    public HotServerContainerPage() {
        super(Messages.PageName, Messages.PageTitle, null);
        setDescription(Messages.PageDesc);
        setPageComplete(true);
    }
    public void initialize(IJavaProject project, IClasspathEntry[] currentEntries) {
        proj = project;
    }
    public void createControl(Composite parent) {
        composite = new Composite(parent, SWT.NULL);
        composite.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_FILL
                | GridData.HORIZONTAL_ALIGN_FILL));
        composite.setFont(parent.getFont());
        createDirGroup(composite);
        createExtGroup(composite);
        
        setControl(composite);
    }
    
    private void createDirGroup(Composite parent) {
        composite.setLayout(new GridLayout(2, false));
        
        Label label = new Label(composite, SWT.NONE);
        label.setText(Messages.DirLabel);
        
        dirText = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
        dirText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        dirText.setText(HotServerContainer.getSoakerLibraryDir());
    }
    
    private void createExtGroup(Composite parent) {
	    Label label = new Label(composite, SWT.NONE);
	    label.setText(Messages.ExtLabel);
        
        extText = new Text(composite,SWT.BORDER | SWT.READ_ONLY);
        extText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        extText.setText(getInitExts());
    }
    
    private String getInitExts() {
        return DEFAULT_EXTS;
    }
        
    protected String getExtValue() {
        return extText.getText().trim().toLowerCase();
    }
    
    protected String getDirValue() {
        return dirText.getText();
    }
    
    protected String getRelativeDirValue() {
        int projDirLen = proj.getProject().getLocation().toString().length();
        return getDirValue().substring( projDirLen );
    }

    public boolean finish() {
        return true;        
    }

    public IClasspathEntry getSelection() {
    	return JavaCore.newContainerEntry(HotServerContainer.ID);
    }

    public void setSelection(IClasspathEntry containerEntry) {
        if(containerEntry != null) {
//            IPath initPath = containerEntry.getPath();
//            System.out.println(initPath);
        }        
    }    
}
