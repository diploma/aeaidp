﻿package com.agileai.miscdp;
/**
 * Miscdp中涉及的静态变量
 */
public abstract class DeveloperConst {
	public static final String TREE_VIEW_ID = "com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView";
	public static final String HOTWEB_NATURE_ID = "com.agileai.miscdp.nature.Hotweb";
//	public static final String DEPLOYMENT_NATURE_ID = "com.genuitec.eclipse.ast.deploy.core.deploymentnature";
//	public static final String WEB_NATURE_ID = "com.genuitec.eclipse.j2eedt.core.webnature";
//	public static final String SPRING_NATURE_ID = "com.genuitec.eclipse.springframework.springnature";
//	public static final String JS_NATURE_ID = "org.eclipse.wst.jsdt.core.jsNature";
	public static final String JAVA_NATURE_ID = "org.eclipse.jdt.core.javanature";
	public static final String FACET_CORE_NATURE_ID = "org.eclipse.wst.common.project.facet.core.nature";
	public static final String MODULE_CORE_NATURE_ID = "org.eclipse.wst.common.modulecore.ModuleCoreNature";
	public static final String JAVA_EMF_NATURE_ID = "org.eclipse.jem.workbench.JavaEMFNature";
	
	public static final String JAVA_BUILDER_ID = "org.eclipse.jdt.core.javabuilder";
//	public static final String JAVASCRIPT_VALIDATOR_BUILDER_ID = "org.eclipse.wst.jsdt.core.javascriptValidator";
	public static final String FACET_CORE_BUILDER_ID = "org.eclipse.wst.common.project.facet.core.builder";
//	public static final String VALIDATION_BUILDER_ID = "org.eclipse.wst.validation.validationbuilder";
	
	public static final String WEB_CLASS_PATH_BUILDER_ID = "com.genuitec.eclipse.j2eedt.core.WebClasspathBuilder";
	public static final String J2EE_PROJECT_VALIDATOR_ID = "com.genuitec.eclipse.j2eedt.core.J2EEProjectValidator";
	public static final String DEPLOYMENT_DESCRIPTOR_VALIDATOR_ID = "com.genuitec.eclipse.j2eedt.core.DeploymentDescriptorValidator";
	public static final String SPRING_BUILDER_ID = "com.genuitec.eclipse.springframework.springbuilder";
	public static final String DEPLOYMENT_BUILDER_ID = "com.genuitec.eclipse.ast.deploy.core.DeploymentBuilder";
	
	public static final String J2EE14_CONTAINER_ID = "com.genuitec.eclipse.j2eedt.core.J2EE14_CONTAINER";
	public static final String WEB_CONTAINER_ID = "org.eclipse.jst.j2ee.internal.web.container";
	public static final String HOTSERVER_CONTAINER_ID = "com.agileai.HOTSERVER_CONTAINER";
	public static final String JRE_CONTAINER_ID = "org.eclipse.jdt.launching.JRE_CONTAINER";
	
	public static final String ABORT_ACTION_ID = "com.agileai.miscdp.hotweb.ui.actions.About";
	public static final String SHOW_PERSPECTIVE_ACTION_ID = "com.agileai.miscdp.hotweb.ui.actions.ShowMiscdpPerspectiveAction";
	
	public static final String PLUGIN_ID = "com.agileai.miscdp";
	
	public static final String PROJECT_CFG_NAME = ".hotwebdata";
	
	public static final String SU_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.standard.StandardModelEditor";
	public static final String CUSTOM_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.custom.CustomModelEditor";
	public static final String MODULE_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.module.FuncModuleEditor";
	public static final String PROJECT_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.project.FuncProjectEditor";
	
	public static final String TREECONTENT_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.treecontent.TreeContentModelEditor";
	public static final String QUERY_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.query.QueryModelEditor";
	public static final String PICKFILL_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.pickfill.PickFillModelEditor";
	public static final String TREEFILL_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.treefill.TreeFillModelEditor";
	public static final String TREEMANAGE_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.treemanage.TreeManageModelEditor";
	public static final String MASTERSUB_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.mastersub.MasterSubModelEditor";
	public static final String SIMPLE_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.simplest.SimplestModelEditor";
	public static final String PORTLET_EDITOR_ID = "com.agileai.miscdp.hotweb.ui.editors.portlet.PortletModelEditor";
	
	public static final String PROJECT_KEY = "project";
	public static final String ACTION_ID = "actionBars";
	
	public static class FuncTreeXml{
		public static final String FUNC_LIST_NAME = "FuncList";
		public static final String FUNC_NODE_NAME = "FuncNode";
		
		public static final String ID = "id";
		public static final String NAME = "name";
		public static final String CODE = "code";
		public static final String TYPE = "type";
		public static final String SUBPKG = "subPkg";
		public static final String TYPE_NAME = "typeName";
		
		public static final String DIR_NAME = ".fmodel";
	}
	
	public static final String PERSPECTIVE_ID = "com.agileai.miscdp.hotweb.HotwebPerspective";
	
	public static final String STANDARD_FUNCMODEL_ID = "standard";
	public static final String SIMPLE_FUNCMODEL_ID = "simplest";
	public static final String PORTLET_FUNCMODEL_ID = "portlet";
	public static final String QUERY_FUNCMODEL_ID = "querymodel";
	public static final String PICKFILL_FUNCMODEL_ID = "pickfillmodel";
	public static final String TREEFILL_FUNCMODEL_ID = "treefillmodel";
	public static final String TREEMANAGE_FUNCMODEL_ID = "treemanagemodel";
	public static final String CUSTOM_FUNCMODEL_ID = "customize";
	public static final String MASTERSUB_FUNCMODEL_ID = "mastersubmodel";
	public static final String TREECONTENT_FUNCMODEL_ID = "treecontentmodel";
	
	
	public static final int CHAR_GEN_LENGTH = 36;
	
	public static final String DATA_TYPE_NUMERIC = "numeric";
	
	public static final String INDEX_HANDLER_PATH =  "com/agileai/hotweb/controller";
	public static final String INDEX_CXMODULE_PATH =  "com/agileai/hotweb/cxmodule";
	public static final String INDEX_FILTER_PATH =  "com/agileai/hotweb/filter";
	public static final String DEMO_SERVICE_PATH = "com/agileai/hotweb/service";
	public static final String SYSTEM_HANDLER_PATH = "com/agileai/hotweb/module/system/handler";
	public static final String SYSTEM_SERVICE_PATH = "com/agileai/hotweb/module/system/service";
	
	public static final String IDX_HANDLER_PACKAGE = "com.agileai.hotweb.controller";
	public static final String IDX_CMODULE_PACKAGE = "com.agileai.hotweb.cxmodule";
	public static final String IDX_FILTER_PACKAGE = "com.agileai.hotweb.filter";
	public static final String IDX_SERVICE_PACKAGE = "com.agileai.hotweb.service";
	public static final String SYS_HANDLER_PACKAGE = "com.agileai.hotweb.module.system.handler";
	public static final String SYS_SERVICE_PACKAGE = "com.agileai.hotweb.module.system.service";
	
	public static final String CONTROLLER = "controller";
	public static final String CXMODULE = "cxmodule";
	public static final String MODULE = "module";
	public static final String FILTER = "filter";
	public static final String SERVICE = "service";
	
	public static final String JSDTSCOPE_CONFIG_FILE = ".jsdtscope";
	public static final String RESOURCES_PREFS_CONFIG_FILE = "org.eclipse.core.resources.prefs";
	public static final String JDT_CORE_PREFS_CONFIG_FILE = "org.eclipse.jdt.core.prefs";
	public static final String COMMON_COMPONENT_CONFIG_FILE = "org.eclipse.wst.common.component";
	public static final String SUPER_TYPE_CONTAINER_CONFIG_FILE = "org.eclipse.wst.jsdt.ui.superType.container";
	public static final String SUPERTYPE_NAME_CONFIG_FILE = "org.eclipse.wst.jsdt.ui.superType.name";
	public static final String FACET_CORE_XML_CONFIG_FILE = "org.eclipse.wst.common.project.facet.core.xml";
	
	public static final String DEFAULT_WEB_CONTEXT = "hotweb";
	
	public static final String HANDLER_CONFIG_FILE = "HandlerContext.xml";
	public static final String SERVICE_CONFIG_FILE = "ServiceContext.xml";
	public static final String HOTWEB_CONFIG_FILE = "hotweb.properties";
	public static final String LOG4J_CONFIG_FILE = "log4j.properties";
	
	public static final String DRIVER_CLASS_NAME = "dataSource.driverClassName";
	public static final String URL = "dataSource.url";
	public static final String USER_NAME = "dataSource.username";
	public static final String PASSWORD = "dataSource.password";
	
	public static final String BLANK_STRING = "空白项";
	
	public static final String PARAMETER_CLASS = "com.agileai.domain.DataParam";
	public static final String RESULT_CLASS = "com.agileai.domain.DataRow";
	
	public static final String DEFAULT_ROOT_ID = "00000000-0000-0000-00000000000000000";
	
	public static final int PACKAGE_LENGTH = 6;
	public static final int PACKAGE_STARTER = 4;
}
