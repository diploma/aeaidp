﻿package com.agileai.miscdp;

import java.io.ByteArrayInputStream;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
/**
 * 空的XML的dtd解析类
 */
public class NoOpEntityResolver implements EntityResolver {
	public InputSource resolveEntity(String publicId, String systemId) {
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream("".getBytes());
		return new InputSource(byteArrayInputStream);
//		return new InputSource("./workflow_2_8.dtd");
	}
}
