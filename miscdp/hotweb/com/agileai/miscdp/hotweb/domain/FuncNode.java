package com.agileai.miscdp.hotweb.domain;

public class FuncNode {
	private String id = null;
	private String name = null;
	private String subPkg = null;
	private String type = null;
	private String typeName = null;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubPkg() {
		return subPkg;
	}
	public void setSubPkg(String subPkg) {
		this.subPkg = subPkg;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
