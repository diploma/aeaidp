package com.agileai.miscdp.hotweb.domain;

public class ServerConfig {
	private String configAlias = "";
	private String projectAlias = "";
	private String serverUserId = "";
	private String serverUserPwd = "";
	private String serverAddress = "";
	private String serverPort = "";
	
	public String getConfigAlias() {
		return configAlias;
	}
	public void setConfigAlias(String configAlias) {
		this.configAlias = configAlias;
	}
	public String getProjectAlias() {
		return projectAlias;
	}
	public void setProjectAlias(String projectAlias) {
		this.projectAlias = projectAlias;
	}
	public String getServerUserId() {
		return serverUserId;
	}
	public void setServerUserId(String userId) {
		this.serverUserId = userId;
	}
	public String getServerUserPwd() {
		return serverUserPwd;
	}
	public void setServerUserPwd(String userPwd) {
		this.serverUserPwd = userPwd;
	}
	public String getServerAddress() {
		return serverAddress;
	}
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}
	public String getServerPort() {
		return serverPort;
	}
	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}	
}
