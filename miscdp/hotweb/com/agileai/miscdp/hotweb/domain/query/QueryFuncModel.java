package com.agileai.miscdp.hotweb.domain.query;

import java.io.Reader;
import java.io.StringReader;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.standard.StandardFuncModel;
import com.agileai.hotweb.model.FormObjectDocument.FormObject;
import com.agileai.hotweb.model.query.QueryFuncModelDocument;
/**
 * 查询功能模型
 */
public class QueryFuncModel extends StandardFuncModel{
	private String sqlMapFile = "";
	private String sqlNameSpace = "";
	private boolean showDetail = true;
	
	public QueryFuncModel(){
		this.editorId = DeveloperConst.QUERY_EDITOR_ID;
	}
	public void buildFuncModel(String funcDefine){
		QueryFuncModel queryFuncModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			QueryFuncModelDocument funcModelDocument = QueryFuncModelDocument.Factory.parse(reader);
			
			QueryFuncModelDocument.QueryFuncModel.BaseInfo baseInfo = funcModelDocument.getQueryFuncModel().getBaseInfo();
			queryFuncModel.setListHandlerId(baseInfo.getHandler().getListHandlerId());
			queryFuncModel.setListHandlerClass(baseInfo.getHandler().getListHandlerClass());
			queryFuncModel.setEditHandlerId(baseInfo.getHandler().getDetailHandlerId());
			queryFuncModel.setEditHandlerClass(baseInfo.getHandler().getDetailHandlerClass());
			
			queryFuncModel.setServiceId(baseInfo.getService().getServiceId());
			queryFuncModel.setImplClassName(baseInfo.getService().getImplClassName());
			queryFuncModel.setInterfaceName(baseInfo.getService().getInterfaceName());
			queryFuncModel.setListJspName(baseInfo.getListJspName());
			queryFuncModel.setDetailJspName(baseInfo.getDetailJspName());
			queryFuncModel.setListTitle(baseInfo.getListTitle());
			queryFuncModel.setDetailTitle(baseInfo.getDetailTitle());
			queryFuncModel.setTemplate(baseInfo.getTemplate());
			queryFuncModel.setShowDetail(baseInfo.getShowDetail());
			queryFuncModel.setListSql(baseInfo.getQueryListSql());
			String sqlMapPath = baseInfo.getSqlResource().getSqlMapPath();
			queryFuncModel.setSqlMapFile(sqlMapPath);
            
			QueryFuncModelDocument.QueryFuncModel.ListView listView = funcModelDocument.getQueryFuncModel().getListView();
			String rsIdColumnTemp  = listView.getListTableArea().getRow().getRsIdColumn();
			String[] rsIdColumnArray = rsIdColumnTemp.split(",");
			for (int i=0;i < rsIdColumnArray.length;i++){
				queryFuncModel.getRsIdColumns().add(rsIdColumnArray[i]);				
			}
			
			FormObject[] formObjects = listView.getParameterArea().getFormObjectArray();
			processPageParamters(formObjects, pageParameters);
			
			com.agileai.hotweb.model.ListTableType listTableType = listView.getListTableArea();
			queryFuncModel.setExportCsv(listTableType.getExportCsv());
			queryFuncModel.setExportXls(listTableType.getExportXls());
			queryFuncModel.setSortAble(listTableType.getSortAble());
			queryFuncModel.setPagination(listTableType.getPagination());

			com.agileai.hotweb.model.ListTableType.Row.Column columns[] = listTableType.getRow().getColumnArray();
			processListTableColumns(columns, listTableColumns);
			
			QueryFuncModelDocument.QueryFuncModel.DetailView detailView = funcModelDocument.getQueryFuncModel().getDetailView();
			FormObject[] editFormObjects =detailView.getDetailViewArea().getFormObjectArray();
			processPageFormFields(editFormObjects, pageFormFields);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getSqlMapFile() {
		return sqlMapFile;
	}
	public void setSqlMapFile(String sqlMapFile) {
		this.sqlMapFile = sqlMapFile;
	}
	public boolean isShowDetail() {
		return showDetail;
	}
	public void setShowDetail(boolean showDetail) {
		this.showDetail = showDetail;
	}
	public String getSqlNameSpace() {
		return sqlNameSpace;
	}
	public void setSqlNameSpace(String sqlNameSpace) {
		this.sqlNameSpace = sqlNameSpace;
	}
}
