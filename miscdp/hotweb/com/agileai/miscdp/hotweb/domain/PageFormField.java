﻿package com.agileai.miscdp.hotweb.domain;

import java.util.ArrayList;
import java.util.List;
/**
 * 页面表单域
 */
public class PageFormField {
	public static final String SELECT_TAG = "select";
	public static final String RES_FILE_TAG = "resfile";
	
	public static final String[] columnProperties = new String[] { "Label",
			"Code","TagType","DataType","Format","Validations" };
	public static final String[] tagTypes = new String[] {"text","select","radio","checkbox","textarea","hidden","popup","resfile"};
	public static final String[] simpleTagTypes = new String[] {"text","select","radio","textarea","hidden"};
	public static final String[] dataTypes = new String[] {"string","date","numeric", "other"};
	public static final String[] formats = new String[] {"","yyyy-MM-dd","yyyy-MM-dd HH:mm", "0.0","0.00","0.000"};
	
	public static final String LABEL = "Label";
	public static final String CODE = "Code";
	public static final String DATATYPE = "DataType";
	public static final String TAGTYPE = "TagType";
	public static final String FORMAT = "Format";
	public static final String VALUEPROVIDER = "ValueProvider";
	public static final String DEFAULTVALUE = "DefaultValue";
	public static final String VALIDATIONS = "Validations";

	private String label = "";
	private String isPK = "N";
	private String code = "";
	private String tagType = "";
	private String valueProvider = "";
	private String defaultValue = "";
	private boolean defValueIsVar = false;
	private List<Validation> validations = new ArrayList<Validation>();
	private String format = "";
	private String dataType = "";
	protected int size = 24; 
	
	public PageFormField(){
	}
	public PageFormField addValidation(Validation validation){
		this.validations.add(validation);
		return this;
	}
	public String getLabel() {
		return label;
	}

	public void setLabel(String labelName) {
		this.label = labelName;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String tagId) {
		this.code = tagId;
	}

	public String getTagType() {
		return tagType;
	}

	public void setTagType(String tagType) {
		this.tagType = tagType;
	}

	public String getValueProvider() {
		return valueProvider;
	}

	public void setValueProvider(String valueProvider) {
		this.valueProvider = valueProvider;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue,boolean isVarialbe) {
		this.defaultValue = defaultValue;
		this.defValueIsVar = isVarialbe;
	}
	public List<Validation> getValidations() {
		return validations;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getIsPK() {
		return isPK;
	}
	public void setIsPK(String isPK) {
		this.isPK = isPK;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size){
		this.size = size;
	}
	public void setDataLength(int dataLength) {
		if (dataLength > 36 && dataLength <= 64){
			this.size = 32;
		}
		else if (dataLength > 64){
			this.size = 65;
		}
	}
	public boolean isDefValueIsVar() {
		return defValueIsVar;
	}
}
