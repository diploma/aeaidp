package com.agileai.miscdp.hotweb.domain.mastersub;

import java.util.ArrayList;
import java.util.List;

/***
 * 主从表操作数据表模型
 */
public class MasterSubTableModel {
	private String namespace = null;
	private String tableName = null;
	private String schema = null;
	private String findMasterRecordsSql = null;
	private String getMasterRecordSql = null;
	private String insertMasterRecordSql = null;
	private String updateMasterRecordSql = null;
	private String deleteMasterRecordSql = null;
	
	private List<SubTableInfo> subTableInfoList = new ArrayList<SubTableInfo>();
	
	public String getDeleteMasterRecordSql() {
		return deleteMasterRecordSql;
	}
	public void setDeleteMasterRecordSql(String deleteMasterRecordSql) {
		this.deleteMasterRecordSql = deleteMasterRecordSql;
	}
	public String getFindMasterRecordsSql() {
		return findMasterRecordsSql;
	}
	public void setFindMasterRecordsSql(String findMasterRecordsSql) {
		this.findMasterRecordsSql = findMasterRecordsSql;
	}
	public String getGetMasterRecordSql() {
		return getMasterRecordSql;
	}
	public void setGetMasterRecordSql(String getMasterRecordSql) {
		this.getMasterRecordSql = getMasterRecordSql;
	}
	public String getInsertMasterRecordSql() {
		return insertMasterRecordSql;
	}
	public void setInsertMasterRecordSql(String insertMasterRecordSql) {
		this.insertMasterRecordSql = insertMasterRecordSql;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getUpdateMasterRecordSql() {
		return updateMasterRecordSql;
	}
	public void setUpdateMasterRecordSql(String updateMasterRecordSql) {
		this.updateMasterRecordSql = updateMasterRecordSql;
	}
	public List<SubTableInfo> getSubTableInfoList() {
		return subTableInfoList;
	}
}
