package com.agileai.miscdp.hotweb.ui.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.agileai.domain.KeyNamePair;
/**
 * 代码生成选项
 */
public class CodeGenConfigDialog extends Dialog {
	boolean cancelEdit = true;
	private List<KeyNamePair> selectedGenList = new ArrayList<KeyNamePair>();
	private List<KeyNamePair> inputGenList = new ArrayList<KeyNamePair>();
	private CheckboxTableViewer checkboxTableViewer;
	
	public CodeGenConfigDialog(Shell parentShell) {
		super(parentShell);
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) container.getLayout();
		gridLayout.marginHeight = 6;
		gridLayout.verticalSpacing = 4;
		gridLayout.marginWidth = 5;
		gridLayout.horizontalSpacing = 2;
		
		Composite composite = new Composite(container, SWT.NONE);
		GridLayout gl_composite = new GridLayout(2, false);
		gl_composite.marginHeight = 0;
		gl_composite.horizontalSpacing = 2;
		gl_composite.marginWidth = 2;
		composite.setLayout(gl_composite);
		
		Button btnNewButton = new Button(composite, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkboxTableViewer.setCheckedElements(new KeyNamePair[0]);
			}
		});
		btnNewButton.setBounds(0, 0, 80, 27);
		btnNewButton.setText("全 清");
		
		Button btnNewButton_1 = new Button(composite, SWT.NONE);
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkboxTableViewer.setCheckedElements(inputGenList.toArray(new KeyNamePair[0]));
			}
		});
		
		btnNewButton_1.setBounds(0, 0, 80, 27);
		btnNewButton_1.setText("全 选");
		Table table = new Table(container,SWT.MULTI | SWT.BORDER | SWT.CHECK |SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		checkboxTableViewer = new CheckboxTableViewer(table);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TableColumn newColumnTableColumn = new TableColumn(table, SWT.NONE);
		newColumnTableColumn.setWidth(272);
		newColumnTableColumn.setText("文件列表");
		checkboxTableViewer.setLabelProvider(new MSKeyNamePairLabelProvider());
		checkboxTableViewer.setContentProvider(new ArrayContentProvider());
		checkboxTableViewer.setInput(inputGenList);
		checkboxTableViewer.setCheckedElements(inputGenList.toArray(new KeyNamePair[0]));
		return container;
	}
	
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,true);
		createButton(parent, IDialogConstants.CANCEL_ID,IDialogConstants.CANCEL_LABEL, false);
	}
	
	protected Point getInitialSize() {
		return new Point(304, 399);
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("代码生成选项");
	}
	
	public HashMap<String,KeyNamePair> getSelectedKeyNamePairs() {
		HashMap<String,KeyNamePair> map = new HashMap<String,KeyNamePair>();
		for (int i=0;i < selectedGenList.size();i++){
			KeyNamePair keyNamePair = (KeyNamePair)selectedGenList.get(i);
			map.put(keyNamePair.getKey(),keyNamePair);
		}
		return map;
	}
	public void addSelectedKeyNamePair(KeyNamePair validation) {
		this.selectedGenList.add(validation);
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			cancelEdit = false;
			this.selectedGenList.clear();
			Object[] objects = (Object[])checkboxTableViewer.getCheckedElements();
			if (objects != null){
				for (int i=0;i < objects.length;i++){
					KeyNamePair object = (KeyNamePair)objects[i];	
					selectedGenList.add(object);
				}
			}
		}
		super.buttonPressed(buttonId);
	}
	public void setInputGenList(List<KeyNamePair> inputGenList) {
		this.inputGenList = inputGenList;
	}
	public boolean isCancelEdit() {
		return cancelEdit;
	}
}
class MSKeyNamePairLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof KeyNamePair) {
			KeyNamePair p = (KeyNamePair) element;
			if (columnIndex == 0) {
				return p.getValue();
			}
		}
		return null;
	}

	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
