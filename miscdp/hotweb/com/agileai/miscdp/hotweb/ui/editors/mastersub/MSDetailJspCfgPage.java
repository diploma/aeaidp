package com.agileai.miscdp.hotweb.ui.editors.mastersub;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.domain.mastersub.MasterSubFuncModel;
import com.agileai.miscdp.hotweb.domain.mastersub.SubTableConfig;
import com.agileai.miscdp.hotweb.ui.actions.mastersub.MSInitGridAction;
import com.agileai.miscdp.hotweb.ui.celleditors.CompositeProviderEditor;
import com.agileai.miscdp.hotweb.ui.celleditors.TableViewerKeyboardSupporter;
import com.agileai.miscdp.hotweb.ui.celleditors.ValidationEditor;
import com.agileai.miscdp.hotweb.ui.editors.AddOrDelOperation;
import com.agileai.miscdp.hotweb.ui.editors.Modifier;
import com.agileai.miscdp.hotweb.ui.editors.UpOrDownOperation;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;
/**
 * 明细Jsp配置页面
 */
public class MSDetailJspCfgPage extends Composite implements Modifier{
	private TabFolder tabFolder;
	private TableViewer tableViewer;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Table table;
	private Text detailTitleText;
	private MasterSubFuncModel funcModel= null;
	private MasterSubModelEditor modelEditor = null;
	private List<Composite> viewAreas = new ArrayList<Composite>();
	
	public MSDetailJspCfgPage(Composite parent,final MasterSubModelEditor modelEditor, int style) {
		super(parent, style);
		this.modelEditor = modelEditor;
		this.funcModel = modelEditor.getFuncModel();

		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
//		createContent();
	}
	public void createContent(){
		setLayout(new GridLayout(1, false));
		SashForm sashForm = new SashForm(this, SWT.VERTICAL);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite mainFormComposite = new Composite(sashForm, SWT.NONE);
		final GridLayout mainFormGridLayout = new GridLayout();
		mainFormGridLayout.verticalSpacing = 2;
		mainFormGridLayout.marginWidth = 2;
		mainFormGridLayout.marginHeight = 2;
		mainFormGridLayout.horizontalSpacing = 2;
		mainFormGridLayout.numColumns = 3;
		mainFormComposite.setLayout(mainFormGridLayout);
		toolkit.adapt(mainFormComposite);
		
		Composite detailListComposite = new Composite(sashForm, SWT.NONE);
		final GridLayout detailListGridLayout = new GridLayout();
		detailListGridLayout.verticalSpacing = 2;
		detailListGridLayout.marginWidth = 2;
		detailListGridLayout.marginHeight = 2;
		detailListGridLayout.horizontalSpacing = 2;
		detailListGridLayout.numColumns = 3;
		detailListComposite.setLayout(detailListGridLayout);
		detailListComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		toolkit.adapt(detailListComposite);
		
		final Label pageTitleLabel = new Label(mainFormComposite, SWT.NONE);
		pageTitleLabel.setText("页面标题");

		detailTitleText = new Text(mainFormComposite, SWT.BORDER);
		detailTitleText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(mainFormComposite, SWT.NONE);

		final Label pageTitleLabel_1 = new Label(mainFormComposite,SWT.NONE);
		pageTitleLabel_1.setText("主表元素");

		tableViewer = new TableViewer(mainFormComposite,SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL
				| SWT.H_SCROLL);
		table = tableViewer.getTable();
		final GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_table.heightHint = 124;
		gd_table.widthHint = 523;
		table.setLayoutData(gd_table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		final TableColumn labelColumn = new TableColumn(table, SWT.NONE);
		labelColumn.setWidth(120);
		labelColumn.setText("Label");

		final TableColumn codeColumn = new TableColumn(table, SWT.NONE);
		codeColumn.setWidth(120);
		codeColumn.setText("Code");

		final TableColumn tagTypeColumn = new TableColumn(table, SWT.NONE);
		tagTypeColumn.setWidth(70);
		tagTypeColumn.setText("TagType");

		final TableColumn dataTypeColumn = new TableColumn(table, SWT.NONE);
		dataTypeColumn.setWidth(70);
		dataTypeColumn.setText("DataType");

		final TableColumn formatColumn = new TableColumn(table, SWT.NONE);
		formatColumn.setWidth(120);
		formatColumn.setText("Format");

		final TableColumn validationColumn = new TableColumn(table, SWT.NONE);
		validationColumn.setWidth(180);
		validationColumn.setText("Validations");
		tableViewer.setLabelProvider(new FormFieldLabelProvider());
		tableViewer.setContentProvider(new ArrayContentProvider());
		final CellEditor[] pageParamEditors = new CellEditor[table.getColumnCount()]; 
		pageParamEditors[0] = new TextCellEditor(table,0);
		pageParamEditors[1] = new CompositeProviderEditor(modelEditor,tableViewer,table);
		pageParamEditors[2] = new ComboBoxCellEditor(table,PageFormField.simpleTagTypes, SWT.READ_ONLY); 
		pageParamEditors[3] = new ComboBoxCellEditor(table,PageFormField.dataTypes, SWT.READ_ONLY);
		pageParamEditors[4] = new ComboBoxCellEditor(table,PageFormField.formats, SWT.READ_ONLY);
		pageParamEditors[5] = new ValidationEditor(modelEditor,tableViewer,table);
		
		tableViewer.setCellEditors(pageParamEditors);
		tableViewer.setColumnProperties(PageFormField.columnProperties); 
		
		TableViewerKeyboardSupporter tableViewerSupporter = new TableViewerKeyboardSupporter(tableViewer); 
		tableViewerSupporter.startSupport();
		tableViewer.setCellModifier(new ICellModifier(){
			public boolean canModify(Object element, String property) {
				return true;
			}
			@SuppressWarnings({"rawtypes" })
			public Object getValue(Object element, String property) {
				PageFormField pageParameter = (PageFormField) element; 
				if (PageFormField.LABEL.equals(property)){
					return pageParameter.getLabel();
				}
				else if (PageFormField.CODE.equals(property)){
					return pageParameter.getCode();
				}
				else if (PageFormField.DATATYPE.equals(property)){
					return getDataTypeIndex(pageParameter.getDataType());
				}
				else if (PageFormField.FORMAT.equals(property)){
					return getFormatIndex(pageParameter.getFormat());
				}
				else if (PageFormField.TAGTYPE.equals(property)){
					return getTagTypeIndex(pageParameter.getTagType());
				}
				else if (PageFormField.VALIDATIONS.equals(property)){
					List validations = pageParameter.getValidations();
					return MiscdpUtil.getValidation(validations);					
				}
				return null;
			}
			private int getFormatIndex(String tagType){
				int result = 0;
				for (int i=0;i < PageFormField.formats.length;i++){
					if (PageFormField.formats[i].equals(tagType)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getTagTypeIndex(String tagType){
				int result = 0;
				for (int i=0;i < PageFormField.simpleTagTypes.length;i++){
					if (PageFormField.simpleTagTypes[i].equals(tagType)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getDataTypeIndex(String dataType){
				int result = 0;
				for (int i=0;i < PageFormField.dataTypes.length;i++){
					if (PageFormField.dataTypes[i].equals(dataType)){
						result = i;
						break;
					}
				}
				return result;
			}
			public void modify(Object element, String property, Object value) {
				TableItem item = (TableItem)element;
				PageFormField pageParameter = (PageFormField) item.getData();
				boolean changed = false;
				if (PageFormField.LABEL.equals(property)){
					if (!String.valueOf(value).equals(pageParameter.getLabel())){
						pageParameter.setLabel(String.valueOf(value));
						changed = true;
					}
				}
				else if (PageFormField.DATATYPE.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageFormField.dataTypes[index].equals(pageParameter.getDataType())){
						pageParameter.setDataType(PageFormField.dataTypes[index]);
						changed = true;
					}
				}
				else if (PageFormField.TAGTYPE.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageFormField.simpleTagTypes[index].equals(pageParameter.getTagType())){
						pageParameter.setTagType(PageFormField.simpleTagTypes[index]);
						changed = true;
					}
				}
				else if (PageFormField.FORMAT.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageFormField.formats[index].equals(pageParameter.getFormat())){
						pageParameter.setFormat(PageFormField.formats[index]);
						changed = true;
					}
				}
				if (changed){
					modelEditor.setModified(true);
					modelEditor.fireDirty();
					tableViewer.refresh();					
				}
			}
		});
		tableViewer.setInput(this.funcModel.getPageFormFields());
		
		final Composite composite = new Composite(mainFormComposite, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.verticalSpacing = 0;
		gridLayout_1.marginWidth = 0;
		gridLayout_1.marginHeight = 0;
		gridLayout_1.horizontalSpacing = 0;
		composite.setLayout(gridLayout_1);

		final Button addrowButton = new Button(composite, SWT.NONE);
		addrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().addRow(tableViewer,PageFormField.class);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		addrowButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addrowButton.setText("添加");

		final Button deleteButton = new Button(composite, SWT.NONE);
		deleteButton.addSelectionListener(new SelectionAdapter() {
			@SuppressWarnings("rawtypes")
			public void widgetSelected(SelectionEvent e) {
				Combo tableNameCombo = ((MasterSubModelEditor)modelEditor).getBasicConfigPage().getTableNameCombo();
				String tableName = tableNameCombo.getText();
				String projectName = funcModel.getProjectName();
				String [] primaryKeys = DBManager.getInstance(projectName).getPrimaryKeys(tableName);
				List pkList = ListUtil.arrayToList(primaryKeys);
				int selectIndex = tableViewer.getTable().getSelectionIndex();
				if (selectIndex != -1){
					List inputList = (List)tableViewer.getInput();
					PageFormField pageFormField = (PageFormField)inputList.get(selectIndex);
					String columnCode = pageFormField.getCode();
					if (pkList.contains(columnCode)){
						Shell shell = modelEditor.getSite().getShell();
						MessageDialog.openInformation(shell, "消息提示","该字段是主键不能删除!");
						return;
					}
				}
				
				new AddOrDelOperation().delRow(tableViewer);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		deleteButton.setText("删除");

		final Button upButton = new Button(composite, SWT.NONE);
		upButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = tableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveUp(tableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		upButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		upButton.setText("上移");

		final Button downButton = new Button(composite, SWT.NONE);
		downButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = tableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveDown(tableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		downButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		downButton.setText("下移");

		final Button syncButton = new Button(composite, SWT.NONE);
		syncButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				List<ListTabColumn> listTabColumns = funcModel.getListTableColumns();
				List<PageFormField> pageFormFields = funcModel.getPageFormFields();
				MiscdpUtil.syncListGrid2FormField(listTabColumns,pageFormFields);
				tableViewer.refresh();
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		syncButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		toolkit.adapt(syncButton, true, true);
		syncButton.setText("同步");

		final Composite composite_1 = new Composite(detailListComposite, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, true));
		final GridLayout gridLayout_11 = new GridLayout();
		gridLayout_11.verticalSpacing = 0;
		gridLayout_11.marginWidth = 0;
		gridLayout_11.marginHeight = 0;
		gridLayout_11.horizontalSpacing = 0;
		composite_1.setLayout(gridLayout_11);
		
		final Label subTableList = new Label(composite_1, SWT.NONE);
		final GridData gd_subTableList = new GridData();
		gd_subTableList.verticalIndent = 20;
		subTableList.setLayoutData(gd_subTableList);
		subTableList.setText("从表列表");
		toolkit.adapt(composite_1);

		final Button button = new Button(composite_1, SWT.NONE);
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				refreshSubTabConfig();
			}
		});
		final GridData gd_button = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd_button.verticalIndent = 5;
		button.setLayoutData(gd_button);
		button.setText("刷 新");

		final Composite subtableComposite = new Composite(detailListComposite, SWT.NONE);
		subtableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		final GridLayout subTableLayout = new GridLayout();
		subTableLayout.marginWidth = 0;
		subTableLayout.marginHeight = 0;
		subTableLayout.makeColumnsEqualWidth = true;
		subtableComposite.setLayout(subTableLayout);

		tabFolder = new TabFolder(subtableComposite, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		toolkit.adapt(tabFolder, true, true);

		List<SubTableConfig> subTableConfigList = funcModel.getSubTableConfigs();
		for (int i=0;i < subTableConfigList.size();i++){
			final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
			SubTableConfig subTableConfig = subTableConfigList.get(i);
			tabItem.setText(subTableConfig.getTableName().toUpperCase());
			if (SubTableConfig.ENTRY_EDIT_MODE.equals(subTableConfig.getEditMode())){
				SubTableEntryEditArea subTableArea = new SubTableEntryEditArea(tabFolder,SWT.NONE,modelEditor,subTableConfig);
				tabItem.setControl(subTableArea);
				subTableArea.initValues();
				subTableArea.registryModifiers();
				viewAreas.add(subTableArea);
			}else{
				SubTableListViewArea subTableArea = new SubTableListViewArea(tabFolder,SWT.NONE,modelEditor,subTableConfig);
				tabItem.setControl(subTableArea);	
				subTableArea.initValues();
				subTableArea.registryModifiers();
				viewAreas.add(subTableArea);
			}
		}
		
		sashForm.setWeights(new int[] {35, 65});
	}

	public void refreshSubTabConfig(){
		viewAreas.clear();
		if (tabFolder.getChildren() == null || tabFolder.getChildren().length == 0){
			List<SubTableConfig> subTableConfigList = funcModel.getSubTableConfigs();
			String projectName = funcModel.getProjectName();
			for (int i=0;i < subTableConfigList.size();i++){
				final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
				SubTableConfig subTableConfig = subTableConfigList.get(i);
				tabItem.setText(subTableConfig.getTableName().toUpperCase());
				String subTableId = subTableConfig.getSubTableId();
				String tableName = subTableConfig.getTableName();
				if (SubTableConfig.ENTRY_EDIT_MODE.equals(subTableConfig.getEditMode())){
					List<PageFormField> subEntryEditFormFields =this.funcModel.getSubEntryEditFormFields(subTableId);
					if (subEntryEditFormFields.isEmpty()){
						com.agileai.miscdp.hotweb.domain.Column[] columns = DBManager.getInstance(projectName).getColumns(tableName);
						List<String> hiddenFields = new ArrayList<String>();
						hiddenFields.add(subTableConfig.getForeignKey());
						if (StringUtil.isNotNullNotEmpty(subTableConfig.getSortField())){
							hiddenFields.add(subTableConfig.getSortField());						
						}
						MasterSubFuncModel.initPageFormFields(columns,subEntryEditFormFields,hiddenFields);
					}
					SubTableEntryEditArea subTableArea = new SubTableEntryEditArea(tabFolder,SWT.NONE,modelEditor,subTableConfig);
					tabItem.setControl(subTableArea);
					subTableArea.initValues();
					subTableArea.registryModifiers();
					viewAreas.add(subTableArea);
				}else{
					List<ListTabColumn> subListTableColumns = this.funcModel.getSubListTableColumns(subTableId);
					if (subListTableColumns.isEmpty()){
						com.agileai.miscdp.hotweb.domain.Column[] columns = DBManager.getInstance(projectName).getColumns(tableName);
						MSInitGridAction msInitGridAction = new MSInitGridAction();
						msInitGridAction.initListTableColumns(columns, subListTableColumns, null, subTableConfig);
					}
					SubTableListViewArea subTableArea = new SubTableListViewArea(tabFolder,SWT.NONE,modelEditor,subTableConfig);
					tabItem.setControl(subTableArea);
					subTableArea.initValues();
					subTableArea.registryModifiers();
					viewAreas.add(subTableArea);
				}
			}
		}
		else{
			TabItem[] items = tabFolder.getItems();
			for (TabItem item : items) {
				item.dispose();
			}
			String projectName = funcModel.getProjectName();
			List<SubTableConfig> subTableConfigList = funcModel.getSubTableConfigs();
			for (int i=0;i < subTableConfigList.size();i++){
				final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
				SubTableConfig subTableConfig = subTableConfigList.get(i);
				tabItem.setText(subTableConfig.getTableName().toUpperCase());
				String subTableId = subTableConfig.getSubTableId();
				String tableName = subTableConfig.getTableName();
				if (SubTableConfig.ENTRY_EDIT_MODE.equals(subTableConfig.getEditMode())){
					List<PageFormField> subEntryEditFormFields = this.funcModel.getSubEntryEditFormFields(subTableId);
					if (subEntryEditFormFields.isEmpty()){
						com.agileai.miscdp.hotweb.domain.Column[] columns = DBManager.getInstance(projectName).getColumns(tableName);
						List<String> hiddenFields = new ArrayList<String>();
						hiddenFields.add(subTableConfig.getForeignKey());
						if (StringUtil.isNotNullNotEmpty(subTableConfig.getSortField())){
							hiddenFields.add(subTableConfig.getSortField());						
						}
						MasterSubFuncModel.initPageFormFields(columns,subEntryEditFormFields,hiddenFields);
					}
					SubTableEntryEditArea subTableArea = new SubTableEntryEditArea(tabFolder,SWT.NONE,modelEditor,subTableConfig);
					tabItem.setControl(subTableArea);
					subTableArea.initValues();
					subTableArea.registryModifiers();
					viewAreas.add(subTableArea);
				}else{
					List<ListTabColumn> subListTableColumns = this.funcModel.getSubListTableColumns(subTableId);
					if (subListTableColumns.isEmpty()){
						com.agileai.miscdp.hotweb.domain.Column[] columns = DBManager.getInstance(projectName).getColumns(tableName);
						MSInitGridAction msInitGridAction = new MSInitGridAction();
						msInitGridAction.initListTableColumns(columns, subListTableColumns, null, subTableConfig);
					}
					SubTableListViewArea subTableArea = new SubTableListViewArea(tabFolder,SWT.NONE,modelEditor,subTableConfig);
					tabItem.setControl(subTableArea);	
					subTableArea.initValues();
					subTableArea.registryModifiers();
					viewAreas.add(subTableArea);
				}
			}
		}
		modelEditor.setModified(true);
		modelEditor.fireDirty();
	}
	
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}

	public void setFuncModel(MasterSubFuncModel standardFuncModel) {
		this.funcModel = standardFuncModel;
	}

	public MasterSubFuncModel buildConfig() {
		funcModel.setDetailTitle(detailTitleText.getText());
		int viewAreaCount = this.viewAreas.size();
		for (int i=0;i < viewAreaCount;i++){
			Composite composite = this.viewAreas.get(i);
			SubTableConfig subTableConfig = this.funcModel.getSubTableConfigs().get(i);
			if (composite instanceof SubTableEntryEditArea){
				SubTableEntryEditArea subTableEntryEditArea = (SubTableEntryEditArea)composite;
				subTableConfig.setTabTitile(subTableEntryEditArea.getTabNameText().getText());
				subTableConfig.setQueryListSql(subTableEntryEditArea.getFindSubRecordsSql().getText());
			}
			else if (composite instanceof SubTableListViewArea){
				SubTableListViewArea subTableListViewArea = (SubTableListViewArea)composite;
				subTableConfig.setTabTitile(subTableListViewArea.getTabNameText().getText());
				subTableConfig.setQueryListSql(subTableListViewArea.getFindRecordsSql().getText());	
			}
		}
		return this.funcModel;
	}
	public void initValues() {
		this.detailTitleText.setText(funcModel.getDetailTitle());
	}
	public TableViewer getTableViewer() {
		return tableViewer;
	}
	public void registryModifier() {
		detailTitleText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
}
class FormFieldLabelProvider extends LabelProvider implements ITableLabelProvider {
	@SuppressWarnings("rawtypes")
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof PageFormField) {
			PageFormField p = (PageFormField) element;
			if (columnIndex == 0) {
				return p.getLabel();
			} else if (columnIndex == 1) {
				return p.getCode();
			} else if (columnIndex == 2) {
				return p.getTagType();
			} else if (columnIndex == 3) {
				return p.getDataType();			
			} else if (columnIndex == 4) {
				return p.getFormat();				
			} else if (columnIndex == 5) {
				List validations = p.getValidations();
				return MiscdpUtil.getValidation(validations);
			}
		}
		return null;
	}
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
