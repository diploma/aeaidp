package com.agileai.miscdp.hotweb.ui.editors.project;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.ServerConfig;
import com.agileai.miscdp.server.HotServerManage;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;

public class ServerConfigEditDialog extends Dialog {
	public static class ActionTypes{
		public static final String Create = "Create";
		public static final String Copy = "Copy";
		public static final String Update = "Update";
	}
	
	private Text configAliasText;
	private Text projectAliasText;
	private Text serverAddressText;
	private Text serverPortText;
	private Text userIdText;
	private Text userPwdText;
	
	private ServerConfig serverConfig = null;
	private String actionType = null;
	
	public ServerConfigEditDialog(Shell parentShell,ServerConfig serverConfig,String actionType) {
		super(parentShell);
		if (serverConfig != null){
			this.serverConfig = serverConfig;
		}
		this.actionType = actionType;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(3, false));
		
		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("配置别名");
		
		configAliasText = new Text(container, SWT.BORDER);
		configAliasText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(container, SWT.NONE);
		
		Label lblNewLabel_1 = new Label(container, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_1.setText("应用别名");
		
		projectAliasText = new Text(container, SWT.BORDER);
		projectAliasText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(container, SWT.NONE);
		
		Label lblNewLabel_2 = new Label(container, SWT.NONE);
		lblNewLabel_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_2.setText("服务器地址");
		
		serverAddressText = new Text(container, SWT.BORDER);
		serverAddressText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(container, SWT.NONE);
		
		Label lblNewLabel_3 = new Label(container, SWT.NONE);
		lblNewLabel_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_3.setText("服务器端口");
		
		serverPortText = new Text(container, SWT.BORDER);
		serverPortText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(container, SWT.NONE);
		
		Label lblNewLabel_4 = new Label(container, SWT.NONE);
		lblNewLabel_4.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_4.setText("用户名");
		
		userIdText = new Text(container, SWT.BORDER);
		userIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(container, SWT.NONE);
		
		Label lblNewLabel_5 = new Label(container, SWT.NONE);
		lblNewLabel_5.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_5.setText("密   码");
		
		userPwdText = new Text(container, SWT.BORDER | SWT.PASSWORD);
		userPwdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Button testButton = new Button(container, SWT.NONE);
		testButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					String appName = projectAliasText.getText();
					String serverAdress = serverAddressText.getText();
					String serverPort = serverPortText.getText();
					String serverUserId = userIdText.getText();
					String serverUserPwd = userPwdText.getText();
					HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(appName,serverAdress,serverPort,serverUserId,serverUserPwd);
					hotServerManage.isExistContext(appName);
					DialogUtil.showMessage(getShell(),"OK","连接服务器成功！",DialogUtil.MESSAGE_TYPE.INFO);
				} catch (Exception ex) {
					DialogUtil.showMessage(getShell(),"消息提示","连接服务器失败！",DialogUtil.MESSAGE_TYPE.WARN);
					MiscdpPlugin.getDefault().logError(ex.getLocalizedMessage(), ex);
				}
			}
		});
		testButton.setText("测试连接");

		this.initValues();
		return container;
	}
	
	
	@Override
	protected void okPressed() {
		if (ActionTypes.Create.equals(this.actionType)){
			this.serverConfig = new ServerConfig();
		}
		this.serverConfig.setConfigAlias(this.configAliasText.getText().trim());
		this.serverConfig.setProjectAlias(this.projectAliasText.getText().trim());
		this.serverConfig.setServerAddress(this.serverAddressText.getText().trim());
		this.serverConfig.setServerPort(this.serverPortText.getText().trim());
		this.serverConfig.setServerUserId(this.userIdText.getText().trim());
		this.serverConfig.setServerUserPwd(this.userPwdText.getText().trim());
		
		super.okPressed();
	}

	private void initValues(){
		if (ActionTypes.Update.equals(this.actionType)){
			this.configAliasText.setText(serverConfig.getConfigAlias());
			this.projectAliasText.setText(serverConfig.getProjectAlias());
			this.serverAddressText.setText(serverConfig.getServerAddress());
			this.serverPortText.setText(serverConfig.getServerPort());
			this.userIdText.setText(serverConfig.getServerUserId());
			this.userPwdText.setText(serverConfig.getServerUserPwd());
		}else if (ActionTypes.Copy.equals(this.actionType)){
			this.configAliasText.setText(serverConfig.getConfigAlias()+"Copy");
			this.projectAliasText.setText(serverConfig.getProjectAlias());
			this.serverAddressText.setText(serverConfig.getServerAddress());
			this.serverPortText.setText(serverConfig.getServerPort());
			this.userIdText.setText(serverConfig.getServerUserId());
			this.userPwdText.setText(serverConfig.getServerUserPwd());
		}
	}
	
	public ServerConfig getServerConfig() {
		return serverConfig;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,true);
		createButton(parent, IDialogConstants.CANCEL_ID,IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(450, 271);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("配置编辑");
	}	
}
