package com.agileai.miscdp.hotweb.ui.editors.mastersub;

import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.PageParameter;
import com.agileai.miscdp.hotweb.domain.mastersub.MasterSubFuncModel;
import com.agileai.miscdp.hotweb.ui.celleditors.CompositeProviderEditor;
import com.agileai.miscdp.hotweb.ui.celleditors.ListColumnProviderEditor;
import com.agileai.miscdp.hotweb.ui.celleditors.TableViewerKeyboardSupporter;
import com.agileai.miscdp.hotweb.ui.celleditors.ValidationEditor;
import com.agileai.miscdp.hotweb.ui.editors.AddOrDelOperation;
import com.agileai.miscdp.hotweb.ui.editors.Modifier;
import com.agileai.miscdp.hotweb.ui.editors.UpOrDownOperation;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 列表JSP配置页面
 */
public class MSListJspCfgPage extends Composite implements Modifier{
	private TableViewer columnTableViewer;
	private TableViewer pageParamTableViewer;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Table pageParamTable;
	private Table columnTable;
	private Text listTitleText;
	private MasterSubFuncModel funcModel= null;
	private MasterSubModelEditor modelEditor = null;
	public MSListJspCfgPage(Composite parent,MasterSubModelEditor suModelEditor, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.modelEditor = suModelEditor;
		this.funcModel = modelEditor.getFuncModel();
	}
	public void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		gridLayout.numColumns = 3;
		setLayout(gridLayout);

		final Label titleLabel = new Label(this, SWT.NONE);
		titleLabel.setText("页面标题");

		listTitleText = new Text(this, SWT.BORDER);
		final GridData gd_listTitleText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_listTitleText.widthHint = 173;
		listTitleText.setLayoutData(gd_listTitleText);
		new Label(this, SWT.NONE);

		final Label tablenameLabel_1_1 = new Label(this, SWT.NONE);
		tablenameLabel_1_1.setLayoutData(new GridData());
		tablenameLabel_1_1.setText("查询参数");

		pageParamTableViewer = new TableViewer(this,SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL
				| SWT.H_SCROLL);
		pageParamTable = pageParamTableViewer.getTable();
		pageParamTable.setLinesVisible(true);
		pageParamTable.setHeaderVisible(true);
		final GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd_table.heightHint = 76;
		pageParamTable.setLayoutData(gd_table);

		final TableColumn labelColumn = new TableColumn(pageParamTable, SWT.NONE);
		labelColumn.setWidth(120);
		labelColumn.setText("Label");

		final TableColumn codeColumn = new TableColumn(pageParamTable, SWT.NONE);
		codeColumn.setWidth(120);
		codeColumn.setText("Code");

		final TableColumn tagTypeColumn = new TableColumn(pageParamTable, SWT.NONE);
		tagTypeColumn.setWidth(70);
		tagTypeColumn.setText("TagType");

		final TableColumn dataTypeColumn = new TableColumn(pageParamTable, SWT.NONE);
		dataTypeColumn.setWidth(70);
		dataTypeColumn.setText("DataType");

		final TableColumn paramFormatColumn = new TableColumn(pageParamTable, SWT.NONE);
		paramFormatColumn.setWidth(120);
		paramFormatColumn.setText("Format");

		final TableColumn validationColumn = new TableColumn(pageParamTable, SWT.NONE);
		validationColumn.setWidth(180);
		validationColumn.setText("Validations");
		pageParamTableViewer.setLabelProvider(new PageParamLabelProvider());
		pageParamTableViewer.setContentProvider(new ArrayContentProvider());
		
		final CellEditor[] pageParamEditors = new CellEditor[pageParamTable.getColumnCount()]; 
		pageParamEditors[0] = new TextCellEditor(pageParamTable);
		pageParamEditors[1] = new CompositeProviderEditor(modelEditor,pageParamTableViewer,pageParamTable);
		pageParamEditors[2] = new ComboBoxCellEditor(pageParamTable,PageParameter.tagTypes, SWT.READ_ONLY); 
		pageParamEditors[3] = new ComboBoxCellEditor(pageParamTable,PageParameter.dataTypes, SWT.READ_ONLY);
		pageParamEditors[4] = new ComboBoxCellEditor(pageParamTable,PageParameter.formats, SWT.READ_ONLY);
		pageParamEditors[5] = new ValidationEditor(modelEditor,pageParamTableViewer,pageParamTable);
		
		pageParamTableViewer.setCellEditors(pageParamEditors);
		pageParamTableViewer.setColumnProperties(PageParameter.columnProperties); 
		
		TableViewerKeyboardSupporter pageParamTableViewerSupporter = new TableViewerKeyboardSupporter(pageParamTableViewer); 
		pageParamTableViewerSupporter.startSupport();
		pageParamTableViewer.setCellModifier(new ICellModifier(){
			public boolean canModify(Object element, String property) {
				return true;
			}
			@SuppressWarnings({"rawtypes" })
			public Object getValue(Object element, String property) {
				PageParameter pageParameter = (PageParameter) element; 
				if (PageParameter.LABEL.equals(property)){
					return pageParameter.getLabel();
				}
				else if (PageParameter.CODE.equals(property)){
					return pageParameter.getCode();
				}
				else if (PageParameter.DATATYPE.equals(property)){
					return getDataTypeIndex(pageParameter.getDataType());
				}
				else if (PageParameter.FORMAT.equals(property)){
					return getFormatIndex(pageParameter.getFormat());
				}
				else if (PageParameter.TAGTYPE.equals(property)){
					return getTagTypeIndex(pageParameter.getTagType());
				}
				else if (PageParameter.VALIDATIONS.equals(property)){
					List validations = pageParameter.getValidations();
					return MiscdpUtil.getValidation(validations);
				}
				return null;
			}
			private int getFormatIndex(String tagType){
				int result = 0;
				for (int i=0;i < PageParameter.formats.length;i++){
					if (PageParameter.formats[i].equals(tagType)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getTagTypeIndex(String tagType){
				int result = 0;
				for (int i=0;i < PageParameter.tagTypes.length;i++){
					if (PageParameter.tagTypes[i].equals(tagType)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getDataTypeIndex(String dataType){
				int result = 0;
				for (int i=0;i < PageParameter.dataTypes.length;i++){
					if (PageParameter.dataTypes[i].equals(dataType)){
						result = i;
						break;
					}
				}
				return result;
			}
			public void modify(Object element, String property, Object value) {
				TableItem item = (TableItem)element;
				PageParameter pageParameter = (PageParameter) item.getData(); 
				boolean pageParamChanged = false;
				if (PageParameter.LABEL.equals(property)){
					if (!String.valueOf(value).equals(pageParameter.getLabel())){
						pageParameter.setLabel(String.valueOf(value));
						pageParamChanged = true;
					}
				}
//				else if (PageParameter.CODE.equals(property)){
//					if (!String.valueOf(value).equals(pageParameter.getCode())){
//						pageParameter.setCode(String.valueOf(value));
//						pageParamChanged = true;
//					}
//				}
				else if (PageParameter.DATATYPE.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageParameter.dataTypes[index].equals(pageParameter.getDataType())){
						pageParameter.setDataType(PageParameter.dataTypes[index]);
						pageParamChanged = true;
					}
				}
				else if (PageParameter.TAGTYPE.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageParameter.tagTypes[index].equals(pageParameter.getTagType())){
						pageParameter.setTagType(PageParameter.tagTypes[index]);
						pageParamChanged = true;
					}
				}
				else if (PageParameter.FORMAT.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageParameter.formats[index].equals(pageParameter.getFormat())){
						pageParameter.setFormat(PageParameter.formats[index]);
						pageParamChanged = true;
					}
				}
				if (pageParamChanged){
					modelEditor.setModified(true);
					modelEditor.fireDirty();
					pageParamTableViewer.refresh();
				}
			}
			
		});
		pageParamTableViewer.setInput(this.funcModel.getPageParameters());
		
		final Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.verticalSpacing = 0;
		gridLayout_1.marginHeight = 0;
		gridLayout_1.marginWidth = 0;
		gridLayout_1.horizontalSpacing = 0;
		composite.setLayout(gridLayout_1);

		final Button addButton = new Button(composite, SWT.NONE);
		addButton.addSelectionListener(new SelectionAdapter() {
			@SuppressWarnings("unchecked")
			public void widgetSelected(SelectionEvent e) {
				Text sqlText = modelEditor.getBasicConfigPage().getListSqlText();
				Shell shell = modelEditor.getSite().getShell();
				if (sqlText.getText() == null && sqlText.getText().trim().equals("")){
					DialogUtil.showMessage(shell,"消息提示","请先填写列表SQL!",DialogUtil.MESSAGE_TYPE.WARN);	
					return;
				}
				String listSql = sqlText.getText().trim();
				int lastWhereIndex = listSql.toLowerCase().indexOf("where");
				if (lastWhereIndex > 0){
					funcModel.getPageParameters().clear();
					String lashWhere = listSql.substring(lastWhereIndex,listSql.length());
					List<String> params = MiscdpUtil.getParams(lashWhere);
					MasterSubFuncModel.initPageParameters(params,funcModel.getPageParameters());
					
					TableViewer tableViewer = modelEditor.getListJspCfgPage().getPageParamTableViewer();
					tableViewer.setInput(funcModel.getPageParameters());
					tableViewer.refresh();
					
					modelEditor.setModified(true);
					modelEditor.fireDirty();
				}
			}
		});
		addButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addButton.setText("解析");

		final Button delButton = new Button(composite, SWT.NONE);
		delButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().delRow(pageParamTableViewer);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		delButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		delButton.setText("删除");

		final Button upButton = new Button(composite, SWT.NONE);
		upButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = pageParamTableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveUp(pageParamTableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		upButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		upButton.setText("上移");

		final Button downButton = new Button(composite, SWT.NONE);
		downButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = pageParamTableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveDown(pageParamTableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		downButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		downButton.setText("下移");

		final Label filteableLabel_1_1 = new Label(this, SWT.NONE);
		filteableLabel_1_1.setLayoutData(new GridData());
		filteableLabel_1_1.setText("列表字段");

		columnTableViewer = new TableViewer(this,SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL
				| SWT.H_SCROLL);
		columnTable = columnTableViewer.getTable();
		columnTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		columnTable.setHeaderVisible(true);
		columnTable.setLinesVisible(true);
		
		final TableColumn titleColumn = new TableColumn(columnTable, SWT.NONE);
		titleColumn.setWidth(120);
		titleColumn.setText("Title");
		
		final TableColumn propertyColumn = new TableColumn(columnTable, SWT.NONE);
		propertyColumn.setWidth(120);
		propertyColumn.setText("Property");

		final TableColumn widthColumn = new TableColumn(columnTable, SWT.NONE);
		widthColumn.setWidth(70);
		widthColumn.setText("Width");

		final TableColumn cellColumn = new TableColumn(columnTable, SWT.NONE);
		cellColumn.setWidth(70);
		cellColumn.setText("DataType");

		final TableColumn formatColumn = new TableColumn(columnTable, SWT.NONE);
		formatColumn.setWidth(120);
		formatColumn.setText("Format");

		final TableColumn mappingItemColumn = new TableColumn(columnTable, SWT.NONE);
		mappingItemColumn.setWidth(180);
		mappingItemColumn.setText("Provider");

		columnTableViewer.setLabelProvider(new TableColumnLabelProvider());
		columnTableViewer.setContentProvider(new ArrayContentProvider());
		
		
		final CellEditor[] tableColumnEditors = new CellEditor[columnTable.getColumnCount()]; 
		tableColumnEditors[0] = new TextCellEditor(columnTable);
		tableColumnEditors[1] = new TextCellEditor(columnTable);
		tableColumnEditors[2] = new ComboBoxCellEditor(columnTable,ListTabColumn.widths, SWT.READ_ONLY);
		tableColumnEditors[3] = new ComboBoxCellEditor(columnTable,ListTabColumn.dataTypes, SWT.READ_ONLY);
		tableColumnEditors[4] = new ComboBoxCellEditor(columnTable,ListTabColumn.formats, SWT.READ_ONLY);
		tableColumnEditors[5] = new ListColumnProviderEditor(modelEditor,columnTableViewer,columnTable);
		
		columnTableViewer.setCellEditors(tableColumnEditors);
		columnTableViewer.setColumnProperties(ListTabColumn.columnProperties); 
		
		TableViewerKeyboardSupporter columnTableViewerSupporter = new TableViewerKeyboardSupporter(columnTableViewer); 
		columnTableViewerSupporter.startSupport();
		
		columnTableViewer.setCellModifier(new ICellModifier(){
			public boolean canModify(Object element, String property) {
//				if (ListTabColumn.PROPERTY.equals(property)
//						|| ListTabColumn.CELL.equals(property)){
//					return false;
//				}
				return true;
			}
			public Object getValue(Object element, String property) {
				ListTabColumn listTabColumn = (ListTabColumn) element; 
				if (ListTabColumn.PROPERTY.equals(property)){
					return listTabColumn.getProperty();
				}
				else if (ListTabColumn.TITLE.equals(property)){
					return listTabColumn.getTitle();
				}
				else if (ListTabColumn.CELL.equals(property)){
					return getCellIndex(listTabColumn.getCell());
				}
				else if (ListTabColumn.FORMAT.equals(property)){
					return getFormatIndex(listTabColumn.getFormat());
				}
				else if (ListTabColumn.MAPPINGITEM.equals(property)){
					return listTabColumn.getMappingItem();
				}
				else if (ListTabColumn.WIDTH.equals(property)){
					return getWidthIndex(listTabColumn.getWidth());
				}
				return null;
			}
			private int getWidthIndex(String cell){
				int result = 0;
				for (int i=0;i < ListTabColumn.widths.length;i++){
					if (ListTabColumn.widths[i].equals(cell)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getCellIndex(String cell){
				int result = 0;
				for (int i=0;i < ListTabColumn.dataTypes.length;i++){
					if (ListTabColumn.dataTypes[i].equals(cell)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getFormatIndex(String cell){
				int result = 0;
				for (int i=0;i < ListTabColumn.formats.length;i++){
					if (ListTabColumn.formats[i].equals(cell)){
						result = i;
						break;
					}
				}
				return result;
			}
			public void modify(Object element, String property, Object value) {
				TableItem item = (TableItem)element;
				ListTabColumn listTabColumn = (ListTabColumn)item.getData();
				boolean changed = false;
				if (ListTabColumn.PROPERTY.equals(property)){
					if (!String.valueOf(value).equals(listTabColumn.getProperty())){
						listTabColumn.setProperty(String.valueOf(value));
						changed = true;
					}
				}
				else if (ListTabColumn.TITLE.equals(property)){
					if (!String.valueOf(value).equals(listTabColumn.getTitle())){
						listTabColumn.setTitle(String.valueOf(value));
						changed = true;
					}
				}
				else if (ListTabColumn.CELL.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !ListTabColumn.dataTypes[index].equals(listTabColumn.getCell())){
						listTabColumn.setCell(ListTabColumn.dataTypes[index]);
						changed = true;
					}
				}
				else if (ListTabColumn.FORMAT.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !ListTabColumn.formats[index].equals(listTabColumn.getFormat())){
						listTabColumn.setFormat(ListTabColumn.formats[index]);
						changed = true;
					}
				}
				else if (ListTabColumn.WIDTH.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !ListTabColumn.widths[index].equals(listTabColumn.getWidth())){
						listTabColumn.setWidth(ListTabColumn.widths[index]);
						changed = true;
					}
				}
				if (changed){
					modelEditor.setModified(true);
					modelEditor.fireDirty();
					columnTableViewer.refresh();
				}
			}
		});
		columnTableViewer.setInput(this.funcModel.getListTableColumns());
		
		final Composite composite_1 = new Composite(this, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.verticalSpacing = 0;
		gridLayout_2.marginWidth = 0;
		gridLayout_2.marginHeight = 0;
		gridLayout_2.horizontalSpacing = 0;
		composite_1.setLayout(gridLayout_2);

		final Button addrowButton = new Button(composite_1, SWT.NONE);
		addrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().addRow(columnTableViewer,ListTabColumn.class);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		addrowButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addrowButton.setText("添加");

		final Button deleteButton = new Button(composite_1, SWT.NONE);
		deleteButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().delRow(columnTableViewer);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		deleteButton.setText("删除");

		final Button upButton_1 = new Button(composite_1, SWT.NONE);
		upButton_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = columnTableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveUp(columnTableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		upButton_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		upButton_1.setText("上移");

		final Button downButton_1 = new Button(composite_1, SWT.NONE);
		downButton_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = columnTableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveDown(columnTableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		downButton_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		downButton_1.setText("下移");
	}
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	protected void checkSubclass() {

	}

	public void setFuncModel(MasterSubFuncModel standardFuncModel) {
		this.funcModel = standardFuncModel;
	}

	public MasterSubFuncModel buildConfig() {
		funcModel.setListTitle(listTitleText.getText());
		return funcModel;
	}
	public void initValues() {
		this.listTitleText.setText(funcModel.getListTitle());
	}
	public TableViewer getPageParamTableViewer() {
		return pageParamTableViewer;
	}
	public TableViewer getColumnTableViewer() {
		return columnTableViewer;
	}
	public void registryModifier() {
		listTitleText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
}
class PageParamLabelProvider extends LabelProvider implements ITableLabelProvider {
	@SuppressWarnings("rawtypes")
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof PageParameter) {
			PageParameter p = (PageParameter) element;
			if (columnIndex == 0) {
				return p.getLabel();
			} else if (columnIndex == 1) {
				return p.getCode();
			} else if (columnIndex == 2) {
				return p.getTagType();
			} else if (columnIndex == 3) {
				return p.getDataType();				
			} else if (columnIndex == 4) {
				return p.getFormat();
			} else if (columnIndex == 5) {
				List validations = p.getValidations();
				return MiscdpUtil.getValidation(validations);
			}
		}
		return null;
	}

	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}

class TableColumnLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof ListTabColumn) {
			ListTabColumn p = (ListTabColumn) element;
			if (columnIndex == 0) {
				return p.getTitle();
			} else if (columnIndex == 1) {
				return p.getProperty();
			} else if (columnIndex == 2) {
				return p.getWidth();
			} else if (columnIndex == 3) {
				return p.getCell();
			} else if (columnIndex == 4) {
				return p.getFormat();
			} else if (columnIndex == 5) {
				return p.getMappingItem();
			}
		}
		return null;
	}

	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
