package com.agileai.miscdp.hotweb.ui.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.agileai.common.IniReader;
import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.server.HotServerManage;
import com.agileai.miscdp.ui.TextCheckPolicy;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * Hotweb工程向导基础配置页
 */
public class HotwebProjectWizardBasicPage extends WizardPage {
	private Text projectLocationText;
	private Text projectNameText;
	private Text mainPackageText;
	
	private String defProjectPath;
	private Text serverAddressText;
	private Text serverPortText;
	private Button javaWebRadioButton;
	private Button integrateWebRadioButton;
	private HotwebProjectWizard wizard = null;
	private Button createWSCheckButton;
	private Composite layoutContainer = null;
	private Text serverUserIdText;
	private Text serverUserPwdText;
	
	public HotwebProjectWizardBasicPage(HotwebProjectWizard wizard) {
		super("");
		IniReader reader = MiscdpUtil.getIniReader(); 
		String title = reader.getValue("ProjectWizard","BasicWizardTitle");
		setTitle(title);
		String desc = reader.getValue("ProjectWizard","BasicWizardDesc");
		setDescription(desc);
		this.wizard = wizard;
		this.defProjectPath = Platform.getLocation().toString();
	}

	public void createControl(Composite parent) {
		this.layoutContainer = new Composite(parent, SWT.NULL);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 3;
		gridLayout.marginWidth = 3;
		gridLayout.numColumns = 1;
		layoutContainer.setLayout(gridLayout);
		setControl(layoutContainer);

		Group basicContainer = new Group(layoutContainer, SWT.NULL);
		basicContainer.setText(Messages.getString("BasicInfo")); 
		basicContainer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		final GridLayout gl_basicContainer = new GridLayout();
		gl_basicContainer.verticalSpacing = 3;
		gl_basicContainer.marginWidth = 3;
		gl_basicContainer.numColumns = 2;
		basicContainer.setLayout(gl_basicContainer);
		
		final Label typeLabel = new Label(basicContainer, SWT.NONE);
		typeLabel.setText("应用类型");
		
		Composite composite = new Composite(basicContainer, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		javaWebRadioButton = new Button(composite, SWT.RADIO);
		javaWebRadioButton.setSelection(true);
		javaWebRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				serverPortText.setText("6060");
				relayout(true);
			}
		});
		javaWebRadioButton.setText(Messages.getString("JavaWebProject")); 
		
		integrateWebRadioButton = new Button(composite, SWT.RADIO);
		integrateWebRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				serverPortText.setText("8080");
				relayout(true);
			}
		});
		integrateWebRadioButton.setText(Messages.getString("IntegrateWebProject"));
		
		final Label label = new Label(basicContainer, SWT.NONE);
		label.setText("应用名称");

		projectNameText = new Text(basicContainer, SWT.BORDER);
		projectNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				basicValidatation();
			}
		});
		projectNameText.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String tempProjectName = projectNameText.getText();
				projectLocationText.setText(defProjectPath+"/"+tempProjectName);
			}
		});
		projectNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new TextCheckPolicy(projectNameText, TextCheckPolicy.POLICY_FREE_NAME);
		projectNameText.forceFocus();
		
		final Label mainPkgLabel = new Label(basicContainer, SWT.NONE);
		mainPkgLabel.setText("主目录");

		mainPackageText = new Text(basicContainer, SWT.BORDER);
		mainPackageText.setText("com.companyname.projectname");
		mainPackageText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				basicValidatation();
			}
		});
		mainPackageText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		

		final Label label_1 = new Label(basicContainer, SWT.NONE);
		label_1.setText("本地路径");

		projectLocationText = new Text(basicContainer, SWT.BORDER);
		projectLocationText.setEditable(false);
		projectLocationText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		projectLocationText.setText(defProjectPath);
		new Label(basicContainer, SWT.NONE);
		
		createWSCheckButton = new Button(basicContainer, SWT.CHECK);
		GridData gd_btnCheckButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnCheckButton.verticalIndent = 2;
		createWSCheckButton.setLayoutData(gd_btnCheckButton);
		createWSCheckButton.setText(Messages.getString("CreateWebService"));
		
		Group serverContainer = new Group(layoutContainer, SWT.NULL);
		serverContainer.setText(Messages.getString("ServerInfo")); 
		serverContainer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		final GridLayout gl_serverContainer = new GridLayout();
		gl_serverContainer.verticalSpacing = 3;
		gl_serverContainer.marginWidth = 3;
		gl_serverContainer.numColumns = 3;
		serverContainer.setLayout(gl_serverContainer);
		
		Label serverAddressLabel = new Label(serverContainer, SWT.NONE);
		serverAddressLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		serverAddressLabel.setText(Messages.getString("ServerAddress"));
		
		serverAddressText = new Text(serverContainer, SWT.BORDER);
		serverAddressText.setText("localhost"); 
		serverAddressText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		serverAddressText.setEditable(false);
		serverAddressText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(serverContainer, SWT.NONE);
		
		Label serverPortLabel = new Label(serverContainer, SWT.NONE);
		serverPortLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		serverPortLabel.setText(Messages.getString("ServerPort"));
		
		serverPortText = new Text(serverContainer, SWT.BORDER);
		serverPortText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		serverPortText.setText("6060");
		new Label(serverContainer, SWT.NONE);
		
		Label lblNewLabel = new Label(serverContainer, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText(Messages.getString("ServerUserId"));
		
		serverUserIdText = new Text(serverContainer, SWT.BORDER);
		serverUserIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		serverUserIdText.setText("admin");
		new Label(serverContainer, SWT.NONE);
		
		Label label_2 = new Label(serverContainer, SWT.NONE);
		label_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_2.setText(Messages.getString("ServerUserPwd"));
		
		serverUserPwdText = new Text(serverContainer, SWT.BORDER | SWT.PASSWORD);
		serverUserPwdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		serverUserPwdText.setText("admin");
		
		Button btnNewButton = new Button(serverContainer, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean validateResout = basicValidatation();
				if (validateResout){
					try{
						String serverPort = serverPortText.getText();
						String serverAddress = serverAddressText.getText();
						String appName = projectNameText.getText();
						String serverUserId = serverUserIdText.getText();
						String serveruserPwd = serverUserPwdText.getText();
						
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(serverAddress, serverPort,serverUserId,serveruserPwd);
						boolean isExist = hotServerManage.isExistContext(appName);
						if (!isExist){
							setPageComplete(true);
							wizard.configPage.defaultPageComplete = false;
							wizard.configPage.setPageComplete(false);
						}else{
							setErrorMessage("application named with "+ appName+" is already exist !");
							setPageComplete(false);
						}
					} catch (Exception ex) {
						setErrorMessage("connect to server failed , please chect it !");
						MiscdpPlugin.getDefault().logError(ex.getLocalizedMessage(), ex);
					}			
				}
			}
		});
		btnNewButton.setText(Messages.getString("TestConnection"));
		this.setPageComplete(false);
	}
	
	public Text getProjectLocationText() {
		return projectLocationText;
	}
	public Text getProjectNameText() {
		return projectNameText;
	}
	public boolean isJavaWebProject(){
		return javaWebRadioButton.getSelection();
	}
	private void relayout(boolean show){
		createWSCheckButton.setEnabled(show);
//		GridData tempGridData0 = (GridData)createWSCheckButton.getLayoutData();
//		if (show){
//			tempGridData0.heightHint = -1;
//		}else{
//			tempGridData0.heightHint = 0;
//		}
//		layoutContainer.layout();
	}
	
	private boolean basicValidatation() {
		String projectName = this.projectNameText.getText();
		if (projectName.length() == 0) {
			updateStatus("project name must be specified !");
			return false;
		}
		if (projectName.length() != projectName.getBytes().length){
			updateStatus("project name must be character !");
			return false;
		}
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		if (project.exists()){
			updateStatus("project is already existed !");
			return false;
		}
		String mainPkgName = this.mainPackageText.getText();
		if (mainPkgName.length() == 0) {
			updateStatus("project main package must be specified !");
			return false;
		}
		if (mainPkgName.length() != mainPkgName.getBytes().length){
			updateStatus("project main package must be character !");
			return false;
		}
		String serverAddress = this.serverAddressText.getText();
		if (serverAddress.length() == 0) {
			updateStatus("server address must be specified !");
			return false;
		}
		String serverPort = this.serverPortText.getText();
		if (serverPort.length() == 0) {
			updateStatus("server port must be specified !");
			return false;
		}
		
		String serverUserId = this.serverUserIdText.getText();
		if (serverUserId.length() == 0) {
			updateStatus("server user name must be specified !");
			return false;
		}
		
		String serverUserPwd = this.serverUserPwdText.getText();
		if (serverUserPwd.length() == 0) {
			updateStatus("server user password must be specified !");
			return false;
		}
		updateStatus(null);
		return true;
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
	}
	public Text getMainPackageText() {
		return mainPackageText;
	}
	public Text getServerAddressText() {
		return serverAddressText;
	}
	public Text getServerPortText() {
		return serverPortText;
	}
	public Button getCreateWSCheckButton() {
		return createWSCheckButton;
	}
	public Text getServerUserIdText() {
		return serverUserIdText;
	}
	public Text getServerUserPwdText() {
		return serverUserPwdText;
	}
}