package com.agileai.miscdp.hotweb.ui.celleditors.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;
import com.agileai.util.StringUtil;
/**
 * 组合编辑器提示框
 */
public class PopupProviderDialog extends Dialog {
	private String code = null;
	
	private Text codeText;
	private Text handlerText;
//	private Combo handlerCombo;
	private PageFormField pageParameter = null;
	
//	private String projectName = null;
	protected BaseModelEditor modelEditor;
	
	public PopupProviderDialog(BaseModelEditor modelEditor,Shell parentShell,String projectName) {
		super(parentShell);
//		this.projectName = projectName;
		this.modelEditor = modelEditor;
	}
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);

		final Label label_1 = new Label(container, SWT.NONE);
		label_1.setText("字段编码");

		codeText = new Text(container, SWT.BORDER);
		codeText.setEditable(false);
		codeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		final Label label = new Label(container, SWT.NONE);
		label.setText("HandlerId");

		handlerText = new Text(container, SWT.BORDER);
		handlerText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		this.initValues();
		return container;
	}
	
	private void initValues(){
		codeText.setText(pageParameter.getCode());
		String valueProvider = pageParameter.getValueProvider();
		if (!StringUtil.isNullOrEmpty(valueProvider)){
			handlerText.setText(valueProvider);
		}
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	
	@Override
	protected void okPressed() {
		if (!StringUtil.isNullOrEmpty(handlerText.getText())){
			pageParameter.setValueProvider(handlerText.getText());
		}
		pageParameter.setDataType("other");
		super.okPressed();
	}
	
	@Override
	protected Point getInitialSize() {
		return new Point(417, 261);
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("内容选择框");
	}
	
	public String getCode() {
		return code;
	}
	
	public void setPageParameter(PageFormField pageParameter) {
		this.pageParameter = pageParameter;
		this.code = pageParameter.getCode();
	}
}