package com.agileai.miscdp.hotweb.ui.celleditors.dialog;

import java.util.ArrayList;

import net.sf.swtaddons.autocomplete.combo.AutocompleteComboInput;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.hotweb.domain.Column;
import com.agileai.miscdp.hotweb.domain.mastersub.SubTableConfig;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.util.StringUtil;
/**
 * 子表信息编辑器提示框
 */
public class SubTableConfigProviderDialog extends Dialog {
	private Combo editModeCombo;
	private Combo tableNameCombo;
	private List foreignKeyList;
	private List sortFieldList;
	private String[] tables = null;
	private SubTableConfig subTableConfig = null;
	boolean cancelEdit = true;
	
	private String currentTableName = null;
	private String projectName = null;
	
	public SubTableConfigProviderDialog(Shell parentShell,String projectName) {
		super(parentShell);
		this.projectName = projectName;
	}
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 3;
		gridLayout.numColumns = 3;
		container.setLayout(gridLayout);

		final Label tableNameLabel = new Label(container, SWT.NONE);
		tableNameLabel.setText("表 名");

		tableNameCombo = new Combo(container, SWT.NONE);
		tableNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		tableNameCombo.setItems(this.getTables());
		tableNameCombo.setText(subTableConfig.getTableName());
		new AutocompleteComboInput(tableNameCombo);
		
		final Button button = new Button(container, SWT.NONE);
		button.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent arg0) {
				if (StringUtil.isNullOrEmpty(tableNameCombo.getText())){
					DialogUtil.showInfoMessage("请先选择表名！");
					return;
				}
				String tableName = tableNameCombo.getText();
				Column[] columns = DBManager.getInstance(projectName).getColumns(tableName);
				String[] items = getFields(columns);
				foreignKeyList.setItems(items);
				sortFieldList.setItems(items);
			}
		});
		button.setText("刷新");
		
		final Label editModelLabel = new Label(container, SWT.NONE);
		editModelLabel.setText("编辑模式");
		
		
		editModeCombo = new Combo(container, SWT.NONE);
		editModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		editModeCombo.setItems(SubTableConfig.editModes);
		editModeCombo.setText(subTableConfig.getEditMode());
		new Label(container, SWT.NONE);

		final Label foreignKeyLabel = new Label(container, SWT.NONE);
		foreignKeyLabel.setText("外键字段");

		foreignKeyList = new List(container, SWT.BORDER);
		foreignKeyList.addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent arg0) {
				buttonPressed(IDialogConstants.OK_ID);
			}
		});
		GridData gd_foreignKeyList = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_foreignKeyList.heightHint = 190;
		foreignKeyList.setLayoutData(gd_foreignKeyList);
		new Label(container, SWT.NONE);
		
		Label label = new Label(container, SWT.NONE);
		label.setText("排序字段");
		sortFieldList = new List(container, SWT.BORDER);
		GridData gd_sortFieldList = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_sortFieldList.heightHint = 190;
		sortFieldList.setLayoutData(gd_sortFieldList);
		new Label(container, SWT.NONE);

		if (!StringUtil.isNullOrEmpty(subTableConfig.getTableName())){
			String tableName = subTableConfig.getTableName();
			Column[] columns = DBManager.getInstance(projectName).getColumns(tableName);
			String[] fields = getFields(columns);
			
			foreignKeyList.setItems(fields);
			String curForeignKey = subTableConfig.getForeignKey();
			for (int i=0;i < columns.length;i++){
				Column column = columns[i];
				if (column.getName().equals(curForeignKey)){
					foreignKeyList.setSelection(i);
					break;
				}
			}
			
			sortFieldList.setItems(fields);
			String curSortField = subTableConfig.getSortField();
			for (int i=0;i < columns.length;i++){
				Column column = columns[i];
				if (column.getName().equals(curSortField)){
					sortFieldList.setSelection(i);
					break;
				}
			}
		}
		
		return container;
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			if (StringUtil.isNullOrEmpty(tableNameCombo.getText())){
				DialogUtil.showInfoMessage("请先选择表名！");
				return;
			}
			if (StringUtil.isNullOrEmpty(editModeCombo.getText())){
				DialogUtil.showInfoMessage("请先选择编辑模式！");
				return;
			}
			if (foreignKeyList.getSelectionIndex() == -1){
				DialogUtil.showInfoMessage("请先选择外键字段！");
				return;
			}
		}
		if (buttonId == IDialogConstants.OK_ID) {
			cancelEdit = false;
			currentTableName = tableNameCombo.getText();
			subTableConfig.setTableName(currentTableName);
			subTableConfig.setEditMode(editModeCombo.getText());
			
			String foreignKey = foreignKeyList.getItem(foreignKeyList.getSelectionIndex());
			subTableConfig.setForeignKey(foreignKey);
			if (sortFieldList.getSelectionIndex() > -1){
				String sortField = sortFieldList.getItem(sortFieldList.getSelectionIndex());
				subTableConfig.setSortField(sortField);				
			}
		}
		super.buttonPressed(buttonId);
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String[] getTables(){
		if (this.tables == null){
			java.util.List tableList = DBManager.getInstance(projectName).getTables(null);
			this.tables = (String[]) tableList.toArray(new String[0]);	
		}
		return this.tables;
	}
	private String[] getFields(Column[] columns){
		java.util.List<String> result = new ArrayList<String>();
		for (int i=0;i < columns.length;i++){
			Column column = columns[i];
			result.add(column.getName());
		}
		return result.toArray(new String[]{});
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	@Override
	protected Point getInitialSize() {
		return new Point(363, 539);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("子表信息配置");
	}
	public boolean isCancelEdit() {
		return cancelEdit;
	}
	public SubTableConfig getSubTableConfig() {
		return subTableConfig;
	}
	public void setSubTableConfig(SubTableConfig subTableConfig) {
		this.subTableConfig = subTableConfig;
	}
	public String getCurrentTableName() {
		return currentTableName;
	}
}
