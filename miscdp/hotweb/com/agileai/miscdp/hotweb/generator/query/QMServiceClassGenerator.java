﻿package com.agileai.miscdp.hotweb.generator.query;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;

import com.agileai.miscdp.MiscdpPlugin;

import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
/**
 * 服务实现类代码生成器
 */
public class QMServiceClassGenerator {
	private String charencoding = "UTF-8";
	private String templateDir = null;
	private String javaFile = null;
	private File xmlFile = null;
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void generate() {
		String fileName = "template";
		try {
			templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
        	Configuration cfg = new Configuration();
        	cfg.setEncoding(Locale.getDefault(), charencoding);
        	cfg.setDirectoryForTemplateLoading(new File(templateDir));
            cfg.setObjectWrapper(new DefaultObjectWrapper());
        	String templateFile = "/query/ServiceImpl.java.ftl";
        	Template temp = cfg.getTemplate(templateFile,charencoding);
        	temp.setEncoding(charencoding);
            Map root = new HashMap();
            NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
            root.put("doc",nodeModel);
            File file = new File(javaFile);
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),charencoding));
//            Writer out = new OutputStreamWriter(System.out);
            temp.process(root, out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }		
	}
	public void setJavaFile(String javaFile) {
		this.javaFile = javaFile;
	}
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
	public String getCharencoding() {
		return charencoding;
	}
}
