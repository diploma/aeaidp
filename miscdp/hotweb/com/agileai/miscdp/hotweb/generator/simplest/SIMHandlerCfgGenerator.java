﻿package com.agileai.miscdp.hotweb.generator.simplest;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.simplest.SimplestFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * 最简功能模型Handler配置代码生成器
 */
public class SIMHandlerCfgGenerator implements Generator{
	private SimplestFuncModel funcModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        String handlerId = funcModel.getHandlerId();
	        String defaultJspName = funcModel.getDefaultJspName();
	        String handlerClass = funcModel.getHandlerClass();
	        MiscdpUtil.newHandlerConfigElement(document,handlerId,handlerClass,defaultJspName);
	        
	        XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setFuncModel(SimplestFuncModel suFuncModel) {
		this.funcModel = suFuncModel;
	}
}
