/*
Navicat Oracle Data Transfer
Oracle Client Version : 11.2.0.1.0

Source Server         : AESB_CONSOLE
Source Server Version : 110200
Source Host           : localhost:1521
Source Schema         : HOTWEB

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2015-02-25 19:14:26
*/


-- ----------------------------
-- Table structure for SECURITY_GROUP
-- ----------------------------
DROP TABLE "SECURITY_GROUP";
CREATE TABLE "SECURITY_GROUP" (
"GRP_ID" NVARCHAR2(36) NOT NULL ,
"GRP_CODE" NVARCHAR2(32) NULL ,
"GRP_NAME" NVARCHAR2(32) NULL ,
"GRP_PID" NVARCHAR2(36) NULL ,
"GRP_DESC" NVARCHAR2(128) NULL ,
"GRP_STATE" CHAR(1) NULL ,
"GRP_SORT" NUMBER(11) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_GROUP
-- ----------------------------
INSERT INTO "SECURITY_GROUP" VALUES ('00000000-0000-0000-00000000000000000', 'Root', '公司集团', null, null, '1', '0');
INSERT INTO "SECURITY_GROUP" VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'IT', '信息部', '00000000-0000-0000-00000000000000000', null, '1', '2');

-- ----------------------------
-- Table structure for SECURITY_GROUP_AUTH
-- ----------------------------
DROP TABLE "SECURITY_GROUP_AUTH";
CREATE TABLE "SECURITY_GROUP_AUTH" (
"GRP_AUTH_ID" NVARCHAR2(36) NOT NULL ,
"GRP_ID" NVARCHAR2(36) NULL ,
"RES_TYPE" NVARCHAR2(32) NULL ,
"RES_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_GROUP_AUTH
-- ----------------------------
INSERT INTO "SECURITY_GROUP_AUTH" VALUES ('4C20EDAC-0A0B-45BB-8B71-B557A94E7429', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'Navigater', '02');

-- ----------------------------
-- Table structure for SECURITY_ROLE
-- ----------------------------
DROP TABLE "SECURITY_ROLE";
CREATE TABLE "SECURITY_ROLE" (
"ROLE_ID" NVARCHAR2(36) NOT NULL ,
"ROLE_CODE" NVARCHAR2(32) NULL ,
"ROLE_NAME" NVARCHAR2(32) NULL ,
"ROLE_PID" NVARCHAR2(36) NULL ,
"ROLE_DESC" NVARCHAR2(128) NULL ,
"ROLE_STATE" NVARCHAR2(32) NULL ,
"ROLE_SORT" NUMBER(11) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_ROLE
-- ----------------------------
INSERT INTO "SECURITY_ROLE" VALUES ('00000000-0000-0000-00000000000000000', 'System', '系统角色', null, null, '1', null);
INSERT INTO "SECURITY_ROLE" VALUES ('8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'ITManager', 'IT主管', '00000000-0000-0000-00000000000000000', null, '1', '3');

-- ----------------------------
-- Table structure for SECURITY_ROLE_AUTH
-- ----------------------------
DROP TABLE "SECURITY_ROLE_AUTH";
CREATE TABLE "SECURITY_ROLE_AUTH" (
"ROLE_AUTH_ID" NVARCHAR2(36) NOT NULL ,
"ROLE_ID" NVARCHAR2(36) NULL ,
"RES_TYPE" NVARCHAR2(32) NULL ,
"RES_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_ROLE_AUTH
-- ----------------------------
INSERT INTO "SECURITY_ROLE_AUTH" VALUES ('586BF6FA-4066-4C65-9352-C0A17B6E2170', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO "SECURITY_ROLE_AUTH" VALUES ('702F8D06-7AF5-4931-8F96-5B4A69D9D2E5', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');

-- ----------------------------
-- Table structure for SECURITY_ROLE_GROUP_REL
-- ----------------------------
DROP TABLE "SECURITY_ROLE_GROUP_REL";
CREATE TABLE "SECURITY_ROLE_GROUP_REL" (
"GRP_ID" NVARCHAR2(36) NOT NULL ,
"ROLE_ID" NVARCHAR2(36) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_ROLE_GROUP_REL
-- ----------------------------
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('00000000-0000-0000-00000000000000000', '00000000-0000-0000-00000000000000000');
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('21E50836-912E-4DB8-82CB-9B32C8A44C9F', '00000000-0000-0000-00000000000000000');
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('21E50836-912E-4DB8-82CB-9B32C8A44C9F', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('23F5915B-C8F1-4BA5-AED9-7CE50B11D5F4', '00000000-0000-0000-00000000000000000');
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('23F5915B-C8F1-4BA5-AED9-7CE50B11D5F4', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('315F898C-A008-4F77-BAB0-4FDF935F7B1F', '00000000-0000-0000-00000000000000000');
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('315F898C-A008-4F77-BAB0-4FDF935F7B1F', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('BBD420A2-68AE-49C2-B3D8-78DC166F511F', '00000000-0000-0000-00000000000000000');

-- ----------------------------
-- Table structure for SECURITY_USER
-- ----------------------------
DROP TABLE "SECURITY_USER";
CREATE TABLE "SECURITY_USER" (
"USER_ID" NVARCHAR2(36) NOT NULL ,
"USER_CODE" NVARCHAR2(32) NULL ,
"USER_NAME" NVARCHAR2(32) NULL ,
"USER_PWD" NVARCHAR2(32) NULL ,
"USER_SEX" CHAR(1) NULL ,
"USER_DESC" NVARCHAR2(128) NULL ,
"USER_STATE" NVARCHAR2(32) NULL ,
"USER_SORT" NUMBER(11) NULL ,
"USER_MAIL" NVARCHAR2(64) NULL ,
"USER_PHONE" NVARCHAR2(64) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_USER
-- ----------------------------
INSERT INTO "SECURITY_USER" VALUES ('7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'admin', '管理员', '21232F297A57A5A743894A0E4A801FC3', 'M', '内置账户，勿删！！', '1', '1', null, null);
INSERT INTO "SECURITY_USER" VALUES ('B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'user', '默认用户', 'EE11CBB19052E40B07AAC0CA060C23EE', 'M', '默认内置用户，勿删！！', '1', '2', null, null);

-- ----------------------------
-- Table structure for SECURITY_USER_AUTH
-- ----------------------------
DROP TABLE "SECURITY_USER_AUTH";
CREATE TABLE "SECURITY_USER_AUTH" (
"USER_AUTH_ID" NVARCHAR2(36) NOT NULL ,
"USER_ID" NVARCHAR2(36) NULL ,
"RES_TYPE" NVARCHAR2(32) NULL ,
"RES_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_USER_AUTH
-- ----------------------------
INSERT INTO "SECURITY_USER_AUTH" VALUES ('1279D0FE-B366-4D7E-9C99-B6B3424387F3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('5194F17D-B001-44EE-9FAB-21F836CB49A8', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Handler', '494DF09B-7573-4CCA-85C1-97F4DC58C86B');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('63253005-BECA-4C51-A764-C36342DA009F', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('97F07CAD-F046-48FB-B03B-5A0AA049FBD5', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('D76FA80F-51AB-4A5B-9594-6DAE53A3C1E3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Operation', '3EC41FB8-ED98-41F3-B86D-F7473AEDD002');

-- ----------------------------
-- Table structure for SECURITY_USER_GROUP_REL
-- ----------------------------
DROP TABLE "SECURITY_USER_GROUP_REL";
CREATE TABLE "SECURITY_USER_GROUP_REL" (
"GRP_ID" NVARCHAR2(36) NOT NULL ,
"USER_ID" NVARCHAR2(36) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_USER_GROUP_REL
-- ----------------------------
INSERT INTO "SECURITY_USER_GROUP_REL" VALUES ('00000000-0000-0000-00000000000000000', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');
INSERT INTO "SECURITY_USER_GROUP_REL" VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '9A0A9DE7-608A-4B2B-B1C7-F6C11FD1A94E');
INSERT INTO "SECURITY_USER_GROUP_REL" VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC');

-- ----------------------------
-- Table structure for SECURITY_USER_ROLE_REL
-- ----------------------------
DROP TABLE "SECURITY_USER_ROLE_REL";
CREATE TABLE "SECURITY_USER_ROLE_REL" (
"ROLE_ID" NVARCHAR2(36) NOT NULL ,
"USER_ID" NVARCHAR2(36) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_USER_ROLE_REL
-- ----------------------------
INSERT INTO "SECURITY_USER_ROLE_REL" VALUES ('00000000-0000-0000-00000000000000000', '01355EDD-C556-4003-AFCE-0E777D4C2776');
INSERT INTO "SECURITY_USER_ROLE_REL" VALUES ('00000000-0000-0000-00000000000000000', '3C927795-572A-4478-871E-5AC72C165BCB');
INSERT INTO "SECURITY_USER_ROLE_REL" VALUES ('00000000-0000-0000-00000000000000000', '6A4BD4CD-5A31-4510-A4D0-A227C0CECA93');
INSERT INTO "SECURITY_USER_ROLE_REL" VALUES ('00000000-0000-0000-00000000000000000', 'F1580191-F60D-438E-9B59-3C1E491E9E78');
INSERT INTO "SECURITY_USER_ROLE_REL" VALUES ('8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', '0098122C-C06E-4316-85B7-54C2646329AF');
INSERT INTO "SECURITY_USER_ROLE_REL" VALUES ('8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');
INSERT INTO "SECURITY_USER_ROLE_REL" VALUES ('8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC');
INSERT INTO "SECURITY_USER_ROLE_REL" VALUES ('D4094F0E-8087-4AAD-A957-01797000BB42', 'CDADE387-273C-45D0-8858-8AC0050DC78E');

-- ----------------------------
-- Table structure for SYS_CODELIST
-- ----------------------------
DROP TABLE "SYS_CODELIST";
CREATE TABLE "SYS_CODELIST" (
"TYPE_ID" NVARCHAR2(32) NOT NULL ,
"CODE_ID" NVARCHAR2(64) NOT NULL ,
"CODE_NAME" NVARCHAR2(64) NULL ,
"CODE_DESC" NVARCHAR2(1024) NULL ,
"CODE_SORT" NUMBER(11) NULL ,
"CODE_FLAG" NVARCHAR2(32) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_CODELIST
-- ----------------------------
INSERT INTO "SYS_CODELIST" VALUES ('BOOL_DEFINE', 'N', '否', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('BOOL_DEFINE', 'Y', '是', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('CODE_TYPE_GROUP', 'app_code_define', '应用编码', 'null', '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('CODE_TYPE_GROUP', 'sys_code_define', '系统编码', '系统编码123a1', '3', '1');
INSERT INTO "SYS_CODELIST" VALUES ('FUNCTION_TYPE', 'funcmenu', '功能菜单', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('FUNCTION_TYPE', 'funcnode', '功能节点', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('HANDLER_TYPE', 'MAIN', '主控制器', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('HANDLER_TYPE', 'OTHER', '其他控制器', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('MENUTREE_CASCADE', '0', '关闭', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('MENUTREE_CASCADE', '1', '展开', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('OPER_CTR_TYPE', 'disableMode', '不能操作', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('OPER_CTR_TYPE', 'hiddenMode', '隐藏按钮', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('POSITION_TYPE', 'dummy_postion', '虚拟岗位', 'null', '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('POSITION_TYPE', 'real_postion', '实际岗位', 'null', '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('RES_TYPE', 'IMAGE', '图片文件', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('RES_TYPE', 'ISO', '镜像文件', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('RES_TYPE', 'VIDEO', '视频文件', null, '3', '1');
INSERT INTO "SYS_CODELIST" VALUES ('SYS_VALID_TYPE', '0', '无效', 'null', '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('SYS_VALID_TYPE', '1', '有效', 'null', '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('UNIT_TYPE', 'dept', '部门', null, '10', '1');
INSERT INTO "SYS_CODELIST" VALUES ('UNIT_TYPE', 'org', '机构', null, '20', '1');
INSERT INTO "SYS_CODELIST" VALUES ('UNIT_TYPE', 'post', '岗位', null, '30', '1');
INSERT INTO "SYS_CODELIST" VALUES ('USER_SEX', 'F', '女', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('USER_SEX', 'M', '男', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Bottom', 'BottomHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Building', 'BuildingHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Homepage', 'HomepageHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Logo', 'LogoHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'MainWin', 'MainWinHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'MenuTree', 'MenuTreeHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Navigater', 'NavigaterHandler', null, '1', '1');
-- ----------------------------
-- Table structure for SYS_CODETYPE
-- ----------------------------
DROP TABLE "SYS_CODETYPE";
CREATE TABLE "SYS_CODETYPE" (
"TYPE_ID" NVARCHAR2(32) NOT NULL ,
"TYPE_NAME" NVARCHAR2(32) NULL ,
"TYPE_GROUP" NVARCHAR2(32) NULL ,
"TYPE_DESC" NVARCHAR2(128) NULL ,
"IS_CACHED" CHAR(1) NULL ,
"IS_UNITEADMIN" CHAR(1) NULL ,
"IS_EDITABLE" CHAR(1) NULL ,
"LEGNTT_LIMIT" NVARCHAR2(6) NULL ,
"CHARACTER_LIMIT" CHAR(1) NULL ,
"EXTEND_SQL" CHAR(1) NULL ,
"SQL_BODY" NVARCHAR2(512) NULL ,
"SQL_COND" NVARCHAR2(256) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_CODETYPE
-- ----------------------------
INSERT INTO "SYS_CODETYPE" VALUES ('BOOL_DEFINE', '布尔定义', 'sys_code_define', null, 'Y', 'Y', 'Y', '1', 'C', null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('CODE_TYPE_GROUP', '编码类型分组', 'app_code_define', '编码类型分组', null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('FUNCTION_TYPE', '功能类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('HANDLER_TYPE', '控制器类型', 'sys_code_define', null, 'N', 'Y', 'Y', '32', 'C', null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('MENUTREE_CASCADE', '是否展开', 'sys_code_define', null, 'Y', 'Y', 'Y', '1', 'N', 'N', null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('OPER_CTR_TYPE', '操作控制类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('POSITION_TYPE', '岗位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('RES_TYPE', '资源类型', 'sys_code_define', null, 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('SYS_VALID_TYPE', '有效标识符', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('UNIT_TYPE', '单位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('USER_SEX', '性别类型', 'sys_code_define', null, 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('AuthedHandlerId', '认证Handler定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '64', 'B', 'N', null, null);

-- ----------------------------
-- Table structure for SYS_FUNCTION
-- ----------------------------
DROP TABLE "SYS_FUNCTION";
CREATE TABLE "SYS_FUNCTION" (
"FUNC_ID" NVARCHAR2(36) NOT NULL ,
"FUNC_NAME" NVARCHAR2(64) NULL ,
"FUNC_TYPE" NVARCHAR2(32) NULL ,
"MAIN_HANDLER" NVARCHAR2(36) NULL ,
"FUNC_PID" NVARCHAR2(36) NULL ,
"FUNC_STATE" CHAR(1) NULL ,
"FUNC_SORT" NUMBER(11) NULL ,
"FUNC_DESC" NVARCHAR2(256) NULL,
"FUNC_ICON" NVARCHAR2(32) NULL
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_FUNCTION
-- ----------------------------
INSERT INTO "SYS_FUNCTION" VALUES ('00000000-0000-0000-00000000000000000', 'Miscdp样例系统', 'funcmenu', null, null, '1', null, null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('00000000-0000-0000-00000000000000001', '系统管理', 'funcmenu', null, '00000000-0000-0000-00000000000000000', '1', '99', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('0855A94F-F8E0-42D6-A0DF-72F1D11C6D0B', '投票管理', 'funcnode', '87D4B614-DF7E-48D2-BA04-3A31728740D9', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '3', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('2F0F133F-1A6F-4E01-AADD-5B11C97BE6A0', '资源管理', 'funcnode', '6479AF01-D00B-4A40-B502-98FAF8418E92', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '5', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('36954650-E79A-4682-8AC2-D3A78461A3D4', '资源分组', 'funcnode', '0543DD98-8AE6-4DF1-990E-24CD51E73564', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '4', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6', '附件管理', 'funcnode', '1F617665-FC8B-4E8C-ABE4-540C363A17A8', '00000000-0000-0000-00000000000000001', '1', '7', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('67BA273A-DD31-48D0-B78C-1D60D5316074', '系统日志', 'funcnode', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', '00000000-0000-0000-00000000000000001', '1', '6', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('692B0D37-2E66-4E82-92B4-E59BCF76EE76', '编码管理', 'funcnode', 'B4FE5722-9EA6-47D8-8770-D999A3F6A354', '00000000-0000-0000-00000000000000001', '1', '5', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('72D2FDBA-6400-4C48-A462-BA5E5B996631', '账户查询', 'funcnode', '6BA56996-4F31-464F-B230-A7E764CCC547', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '2', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('8C84B439-2788-4608-89C4-8F5AA076D124', '组织机构', 'funcnode', '439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', '00000000-0000-0000-00000000000000001', '1', '1', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('A0334956-426E-4E49-831B-EB00E37285FD', '编码类型', 'funcnode', '9A16D554-F989-438A-B92D-C8C8AC6BF9B8', '00000000-0000-0000-00000000000000001', '1', '4', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '样例功能', 'funcmenu', null, '00000000-0000-0000-00000000000000000', '1', '100', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('C843A0B6-2837-48A6-9C16-DCB6FFB37790', '账户管理', 'funcnode', '048E2C48-1BD4-4EB2-94E4-208835D302EB', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '1', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('C977BC31-C78F-4B16-B0C6-769783E46A06', '功能管理', 'funcnode', '46C52D33-8797-4251-951F-F7CA23C76BD7', '00000000-0000-0000-00000000000000001', '1', '3', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('D3582A2A-3173-4F92-B1AD-2F999A2CBE18', '修改密码', 'funcnode', '88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', '00000000-0000-0000-00000000000000001', '1', '8', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('DFE8BE4C-4024-4A7B-8DF2-630003832AE9', '角色管理', 'funcnode', '0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', '00000000-0000-0000-00000000000000001', '1', '2', null, null);

-- ----------------------------
-- Table structure for SYS_HANDLER
-- ----------------------------
DROP TABLE "SYS_HANDLER";
CREATE TABLE "SYS_HANDLER" (
"HANLER_ID" NVARCHAR2(36) NOT NULL ,
"HANLER_CODE" NVARCHAR2(64) NULL ,
"HANLER_TYPE" NVARCHAR2(32) NULL ,
"HANLER_URL" NVARCHAR2(128) NULL ,
"FUNC_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_HANDLER
-- ----------------------------
INSERT INTO "SYS_HANDLER" VALUES ('048E2C48-1BD4-4EB2-94E4-208835D302EB', 'AccountManageList', 'MAIN', null, 'C843A0B6-2837-48A6-9C16-DCB6FFB37790');
INSERT INTO "SYS_HANDLER" VALUES ('0543DD98-8AE6-4DF1-990E-24CD51E73564', 'ResGroupTreeManage', 'MAIN', null, '36954650-E79A-4682-8AC2-D3A78461A3D4');
INSERT INTO "SYS_HANDLER" VALUES ('0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', 'SecurityRoleTreeManage', 'MAIN', null, 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO "SYS_HANDLER" VALUES ('1F617665-FC8B-4E8C-ABE4-540C363A17A8', 'WcmGeneralGroup8ContentList', 'MAIN', null, '5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6');
INSERT INTO "SYS_HANDLER" VALUES ('439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', 'SecurityGroupList', 'MAIN', null, '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO "SYS_HANDLER" VALUES ('46C52D33-8797-4251-951F-F7CA23C76BD7', 'FunctionTreeManage', 'MAIN', null, 'C977BC31-C78F-4B16-B0C6-769783E46A06');
INSERT INTO "SYS_HANDLER" VALUES ('494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'SysLogQueryList', 'MAIN', null, '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO "SYS_HANDLER" VALUES ('6479AF01-D00B-4A40-B502-98FAF8418E92', 'ResGroup8ContentList', 'MAIN', null, '2F0F133F-1A6F-4E01-AADD-5B11C97BE6A0');
INSERT INTO "SYS_HANDLER" VALUES ('6BA56996-4F31-464F-B230-A7E764CCC547', 'AccountQueryList', 'MAIN', null, '72D2FDBA-6400-4C48-A462-BA5E5B996631');
INSERT INTO "SYS_HANDLER" VALUES ('87D4B614-DF7E-48D2-BA04-3A31728740D9', 'VoteInfoManageList', 'MAIN', null, '0855A94F-F8E0-42D6-A0DF-72F1D11C6D0B');
INSERT INTO "SYS_HANDLER" VALUES ('88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', 'ModifyPassword', 'MAIN', null, 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO "SYS_HANDLER" VALUES ('9A16D554-F989-438A-B92D-C8C8AC6BF9B8', 'CodeTypeManageList', 'MAIN', null, 'A0334956-426E-4E49-831B-EB00E37285FD');
INSERT INTO "SYS_HANDLER" VALUES ('B4FE5722-9EA6-47D8-8770-D999A3F6A354', 'CodeListManageList', 'MAIN', null, '692B0D37-2E66-4E82-92B4-E59BCF76EE76');

-- ----------------------------
-- Table structure for SYS_LOG
-- ----------------------------
DROP TABLE "SYS_LOG";
CREATE TABLE "SYS_LOG" (
"ID" NCHAR(36) NULL ,
"OPER_TIME" DATE NOT NULL ,
"IP_ADDTRESS" NVARCHAR2(32) NULL ,
"USER_ID" NVARCHAR2(32) NULL ,
"USER_NAME" NVARCHAR2(32) NULL ,
"FUNC_NAME" NVARCHAR2(64) NULL ,
"ACTION_TYPE" NVARCHAR2(32) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_LOG
-- ----------------------------
INSERT INTO "SYS_LOG" VALUES ('50158899-1D9A-4F02-B3ED-588A5983FEB5', TO_DATE('2015-02-25 16:53:14', 'YYYY-MM-DD HH24:MI:SS'), '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO "SYS_LOG" VALUES ('9C0E8965-2355-4224-92EC-8C0F432FC8B9', TO_DATE('2015-02-25 17:43:35', 'YYYY-MM-DD HH24:MI:SS'), '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO "SYS_LOG" VALUES ('5A098FEE-3FA0-4595-8839-E8844B130259', TO_DATE('2015-02-25 17:44:27', 'YYYY-MM-DD HH24:MI:SS'), '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');

-- ----------------------------
-- Table structure for SYS_ONLINECOUNT
-- ----------------------------
DROP TABLE "SYS_ONLINECOUNT";
CREATE TABLE "SYS_ONLINECOUNT" (
"IPADDRRESS" NVARCHAR2(64) NOT NULL ,
"ONLINECOUNT" NUMBER(11) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_ONLINECOUNT
-- ----------------------------

-- ----------------------------
-- Table structure for SYS_OPERATION
-- ----------------------------
DROP TABLE "SYS_OPERATION";
CREATE TABLE "SYS_OPERATION" (
"OPER_ID" NCHAR(36) NOT NULL ,
"HANLER_ID" NVARCHAR2(36) NULL ,
"OPER_CODE" NVARCHAR2(64) NULL ,
"OPER_NAME" NVARCHAR2(64) NULL ,
"OPER_ACTIONTPYE" NVARCHAR2(64) NULL ,
"OPER_SORT" NUMBER(11) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_OPERATION
-- ----------------------------
INSERT INTO "SYS_OPERATION" VALUES ('3EC41FB8-ED98-41F3-B86D-F7473AEDD002', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'viewDetail', '查看', 'viewDetail', '1');
INSERT INTO "SYS_OPERATION" VALUES ('6BF7A157-D333-4F40-8612-F61D3FD4D258', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'refreshImgBtn', '刷新', 'refresh', '2');

-- ----------------------------
-- Table structure for WCM_GENERAL_GROUP
-- ----------------------------
DROP TABLE "WCM_GENERAL_GROUP";
CREATE TABLE "WCM_GENERAL_GROUP" (
"GRP_ID" NVARCHAR2(36) NOT NULL ,
"GRP_NAME" NVARCHAR2(64) NULL ,
"GRP_PID" NVARCHAR2(36) NULL ,
"GRP_ORDERNO" NUMBER(11) NULL ,
"GRP_IS_SYSTEM" NVARCHAR2(32) NULL ,
"GRP_RES_TYPE_DESC" NVARCHAR2(32) NULL ,
"GRP_RES_TYPE_EXTS" NVARCHAR2(128) NULL ,
"GRP_RES_SIZE_LIMIT" NVARCHAR2(32) NULL ,
"GRP_DESC" NVARCHAR2(128) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WCM_GENERAL_GROUP
-- ----------------------------
INSERT INTO "WCM_GENERAL_GROUP" VALUES ('77777777-7777-7777-7777-777777777777', '附件目录', null, null, null, null, null, null, null);
INSERT INTO "WCM_GENERAL_GROUP" VALUES ('A6018D88-8345-46EE-A452-CE362FAC72E2', '视频文件', '77777777-7777-7777-7777-777777777777', '1', 'Y', '视频', '*.mp4;*.3gp;*.wmv;*.avi;*.rm;*.rmvb;*.flv', '100MB', null);
INSERT INTO "WCM_GENERAL_GROUP" VALUES ('CF35D1E6-102E-428A-B39C-0072D491D5B1', '业务附件', '77777777-7777-7777-7777-777777777777', '2', 'Y', '所有资源', '*.*', '2MB', null);

-- ----------------------------
-- Table structure for WCM_GENERAL_RESOURCE
-- ----------------------------
DROP TABLE "WCM_GENERAL_RESOURCE";
CREATE TABLE "WCM_GENERAL_RESOURCE" (
"RES_ID" NVARCHAR2(36) NOT NULL ,
"GRP_ID" NVARCHAR2(36) NULL ,
"RES_NAME" NVARCHAR2(64) NULL ,
"RES_SHAREABLE" NVARCHAR2(32) NULL ,
"RES_LOCATION" NVARCHAR2(256) NULL ,
"RES_SIZE" NVARCHAR2(64) NULL ,
"RES_SUFFIX" NVARCHAR2(32) NULL ,
"RES_DESCRIPTION" NVARCHAR2(256) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WCM_GENERAL_RESOURCE
-- ----------------------------

-- ----------------------------
-- Indexes structure for table SECURITY_GROUP
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_GROUP
-- ----------------------------
ALTER TABLE "SECURITY_GROUP" ADD CHECK ("GRP_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_GROUP
-- ----------------------------
ALTER TABLE "SECURITY_GROUP" ADD PRIMARY KEY ("GRP_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_GROUP_AUTH
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_GROUP_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_GROUP_AUTH" ADD CHECK ("GRP_AUTH_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_GROUP_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_GROUP_AUTH" ADD PRIMARY KEY ("GRP_AUTH_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_ROLE
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_ROLE
-- ----------------------------
ALTER TABLE "SECURITY_ROLE" ADD CHECK ("ROLE_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_ROLE
-- ----------------------------
ALTER TABLE "SECURITY_ROLE" ADD PRIMARY KEY ("ROLE_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_ROLE_AUTH
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_ROLE_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_ROLE_AUTH" ADD CHECK ("ROLE_AUTH_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_ROLE_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_ROLE_AUTH" ADD PRIMARY KEY ("ROLE_AUTH_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_ROLE_GROUP_REL
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_ROLE_GROUP_REL
-- ----------------------------
ALTER TABLE "SECURITY_ROLE_GROUP_REL" ADD CHECK ("GRP_ID" IS NOT NULL);
ALTER TABLE "SECURITY_ROLE_GROUP_REL" ADD CHECK ("ROLE_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_ROLE_GROUP_REL
-- ----------------------------
ALTER TABLE "SECURITY_ROLE_GROUP_REL" ADD PRIMARY KEY ("GRP_ID", "ROLE_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_USER
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_USER
-- ----------------------------
ALTER TABLE "SECURITY_USER" ADD CHECK ("USER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_USER
-- ----------------------------
ALTER TABLE "SECURITY_USER" ADD PRIMARY KEY ("USER_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_USER_AUTH
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_USER_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_USER_AUTH" ADD CHECK ("USER_AUTH_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_USER_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_USER_AUTH" ADD PRIMARY KEY ("USER_AUTH_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_USER_GROUP_REL
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_USER_GROUP_REL
-- ----------------------------
ALTER TABLE "SECURITY_USER_GROUP_REL" ADD CHECK ("GRP_ID" IS NOT NULL);
ALTER TABLE "SECURITY_USER_GROUP_REL" ADD CHECK ("USER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_USER_GROUP_REL
-- ----------------------------
ALTER TABLE "SECURITY_USER_GROUP_REL" ADD PRIMARY KEY ("GRP_ID", "USER_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_USER_ROLE_REL
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_USER_ROLE_REL
-- ----------------------------
ALTER TABLE "SECURITY_USER_ROLE_REL" ADD CHECK ("ROLE_ID" IS NOT NULL);
ALTER TABLE "SECURITY_USER_ROLE_REL" ADD CHECK ("USER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_USER_ROLE_REL
-- ----------------------------
ALTER TABLE "SECURITY_USER_ROLE_REL" ADD PRIMARY KEY ("ROLE_ID", "USER_ID");

-- ----------------------------
-- Indexes structure for table SYS_CODELIST
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_CODELIST
-- ----------------------------
ALTER TABLE "SYS_CODELIST" ADD CHECK ("TYPE_ID" IS NOT NULL);
ALTER TABLE "SYS_CODELIST" ADD CHECK ("CODE_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_CODELIST
-- ----------------------------
ALTER TABLE "SYS_CODELIST" ADD PRIMARY KEY ("TYPE_ID", "CODE_ID");

-- ----------------------------
-- Indexes structure for table SYS_CODETYPE
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_CODETYPE
-- ----------------------------
ALTER TABLE "SYS_CODETYPE" ADD CHECK ("TYPE_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_CODETYPE
-- ----------------------------
ALTER TABLE "SYS_CODETYPE" ADD PRIMARY KEY ("TYPE_ID");

-- ----------------------------
-- Indexes structure for table SYS_FUNCTION
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_FUNCTION
-- ----------------------------
ALTER TABLE "SYS_FUNCTION" ADD CHECK ("FUNC_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_FUNCTION
-- ----------------------------
ALTER TABLE "SYS_FUNCTION" ADD PRIMARY KEY ("FUNC_ID");

-- ----------------------------
-- Indexes structure for table SYS_HANDLER
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_HANDLER
-- ----------------------------
ALTER TABLE "SYS_HANDLER" ADD CHECK ("HANLER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_HANDLER
-- ----------------------------
ALTER TABLE "SYS_HANDLER" ADD PRIMARY KEY ("HANLER_ID");

-- ----------------------------
-- Checks structure for table SYS_LOG
-- ----------------------------
ALTER TABLE "SYS_LOG" ADD CHECK ("OPER_TIME" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table SYS_ONLINECOUNT
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_ONLINECOUNT
-- ----------------------------
ALTER TABLE "SYS_ONLINECOUNT" ADD CHECK ("IPADDRRESS" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_ONLINECOUNT
-- ----------------------------
ALTER TABLE "SYS_ONLINECOUNT" ADD PRIMARY KEY ("IPADDRRESS");

-- ----------------------------
-- Indexes structure for table SYS_OPERATION
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_OPERATION
-- ----------------------------
ALTER TABLE "SYS_OPERATION" ADD CHECK ("OPER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_OPERATION
-- ----------------------------
ALTER TABLE "SYS_OPERATION" ADD PRIMARY KEY ("OPER_ID");

-- ----------------------------
-- Indexes structure for table WCM_GENERAL_GROUP
-- ----------------------------

-- ----------------------------
-- Checks structure for table WCM_GENERAL_GROUP
-- ----------------------------
ALTER TABLE "WCM_GENERAL_GROUP" ADD CHECK ("GRP_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WCM_GENERAL_GROUP
-- ----------------------------
ALTER TABLE "WCM_GENERAL_GROUP" ADD PRIMARY KEY ("GRP_ID");

-- ----------------------------
-- Indexes structure for table WCM_GENERAL_RESOURCE
-- ----------------------------

-- ----------------------------
-- Checks structure for table WCM_GENERAL_RESOURCE
-- ----------------------------
ALTER TABLE "WCM_GENERAL_RESOURCE" ADD CHECK ("RES_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WCM_GENERAL_RESOURCE
-- ----------------------------
ALTER TABLE "WCM_GENERAL_RESOURCE" ADD PRIMARY KEY ("RES_ID");
